<?php
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS|JSON_HEX_QUOT);

	$classBase = 'gecko-slideshow__slide';

	$classes = [
		$classBase,
		'swiper-slide',
	];

	if (isset($attributes['addContrast']) && $attributes['addContrast']) {
		$classes[] = $classBase . '--add-contrast';
	}

	if (isset($attributes['verticalAlign']) && $attributes['verticalAlign'] != "") {
		$classes[] = $classBase . '--vertical-align-' . $attributes['verticalAlign'];
	} else {
		$classes[] = $classBase . '--vertical-align-top';
	}

	if (isset($attributes['rowWidth']) && $attributes['rowWidth'] != "") {
		$classes[] = $classBase . '--row-width-' . $attributes['rowWidth'];
	} else {
		$classes[] = $classBase . '--row-width-full';
	}


	// Background style classes
	if (isset($attributes['bgImageUrl']) && $attributes['bgImageUrl'] != "") {
		if (isset($attributes['bgOverlayOpacity']) && $attributes['bgOverlayOpacity'] != "") {
			$classes[] = $classBase . '--opacity-' . $attributes['bgOverlayOpacity'];
		} else {
			$classes[] = $classBase . '--opacity-0';
		}
	}

	// Background styles
	$style = '';
	if (isset($attributes['bgColorValue']) && $attributes['bgColorValue'] != "") {
		$style .= "background-color: " . $attributes['bgColorValue'] . ";";
	}
	if (isset($attributes['bgImageUrl']) && $attributes['bgImageUrl'] != "") {
		$style .= "background-image: url('" . $attributes['bgImageUrl'] . "');";
	}
	if (isset($attributes['bgImageFocalPoint']) && $attributes['bgImageFocalPoint'] != "") {
		$x = intval($attributes['bgImageFocalPoint']['x'] * 100);
		$y = intval($attributes['bgImageFocalPoint']['y'] * 100);
		$style .= "background-position: ".$x. "% " . $y . "%;";
	}

	$markup = '<div class="' . implode(' ', $classes) . '" style="%s" data-props="%s">';

		if (isset($attributes['bgOverlayColorValue']) && $attributes['bgOverlayColorValue'] != "") {
			$overlayStyle = 'background-color: ' . $attributes['bgOverlayColorValue'] . ';';
			$markup .= '<div class="' . $classBase . '-overlay" style="' . $overlayStyle . '"></div>';
		}

		$markup .= '%s';

	$markup .= '</div>';
	// $markup = '<div class="'.implode(' ', $classes). '" style="%s" data-props="%s">%s</div>';

	return sprintf(
		$markup,
		$style,
		htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'),
		$content
	);
};