<?php
/**
	* If the block is dynamic you would render the template here.
	*/
$render = function ( $attributes, $content ) {
	// Defaults and attributes
	// Setting to false unless needed because all styles do not need to be inlined
	$defaults = array(
		'size' => false,
		'background' => false,
		'minHeight' => false,
		'className' => false,
		'align' => false,
		'contrast' => false,
	);
	// Add a filter to hook into the default args
	$defaults = apply_filters( 'gecko/section/defaults', $defaults, $attributes );
	$atts = wp_parse_args( $attributes, $defaults );

	$classNames = array('gecko-section');
	if($atts['className']) $classNames[] = $atts['className'];
	if($atts['size']) $classNames[] = 'is-size-'.$atts['size'];
	if($atts['align']) $classNames[] = 'is-align-'.$atts['align'];
	if($atts['contrast']) $classNames[] = 'add-contrast';
	// Add a filter to hook into classNames
	$classNames = apply_filters( 'gecko/section/class', $classNames, $attributes );

	$styles = array();
	if($atts['background']) $styles['background'] = $atts['background'];
	if($atts['minHeight']) $styles['min-height'] = $atts['minHeight'];
	// Add a filter to hook into the inine styles $args = ($styles, $atts)
	$styles = apply_filters( 'gecko/section/style', $styles, $attributes );

	$styleString = '';
	foreach ($styles as $key => $value) {
		if($value) $styleString .= $key.':'.$value.';';
	}


	return sprintf('<div class="%s" style="%s"><div class="gecko-section__inner">%s</div></div>', implode(' ', $classNames), $styleString, $content);
};