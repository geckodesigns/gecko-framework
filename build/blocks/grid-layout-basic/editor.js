/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/plugins/gecko-framework/build/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/blocks/grid-layout-basic/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/blocks/grid-layout-basic/index.js":
/*!***********************************************!*\
  !*** ./src/blocks/grid-layout-basic/index.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_editor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/editor */ "@wordpress/editor");
/* harmony import */ var _wordpress_editor__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_editor__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_6__);



/**
 * WordPress dependencies
 */






var name = 'gecko/grid-layout-basic';
var settings = {
  title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Basic'),
  parent: ['gecko/grid-layout'],
  icon: react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["SVG"], {
    viewBox: "0 0 24 24",
    xmlns: "http://www.w3.org/2000/svg"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Path"], {
    d: "M0,0h24v24H0V0z M0,0h24v24H0V0z",
    fill: "none"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Path"], {
    d: "m20 7v10h-16v-10h16m0-2h-16c-1.1 0-1.99 0.9-1.99 2l-0.01 10c0 1.1 0.9 2 2 2h16c1.1 0 2-0.9 2-2v-10c0-1.1-0.9-2-2-2z"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "11",
    y: "8",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "11",
    y: "11",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "8",
    y: "8",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "8",
    y: "11",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "5",
    y: "11",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "5",
    y: "8",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "8",
    y: "14",
    width: "8",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "14",
    y: "11",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "14",
    y: "8",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "17",
    y: "11",
    width: "2",
    height: "2"
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Rect"], {
    x: "17",
    y: "8",
    width: "2",
    height: "2"
  })),
  description: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('An image grid block.'),
  category: 'common',
  supports: {
    className: true,
    inserter: true,
    reusable: false,
    html: false
  },
  styles: [{
    name: 'default',
    label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Default'),
    isDefault: true
  }],
  attributes: {
    h: {
      type: 'number',
      default: 1
    },
    w: {
      type: 'number',
      default: 1
    },
    alignContent: {
      type: 'string',
      default: 'top'
    }
  },
  transforms: {
    from: [{
      type: 'block',
      blocks: ['gecko/grid-layout-image', 'gecko/grid-layout-item', 'gecko/grid-layout-card'],
      transform: function transform(attributes, innerBlocks) {
        var selected = Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_6__["select"])('core/editor').getSelectedBlock(); // because innerBlocks does not work.
        // It appears that innerBlocks will be added in the future.

        var h = attributes.h,
            w = attributes.w;
        return Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_4__["createBlock"])('gecko/grid-layout-basic', {
          h: h,
          w: w
        }, selected.innerBlocks);
      }
    }]
  },
  deprecated: [{
    migrate: function migrate() {},
    save: function save() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_editor__WEBPACK_IMPORTED_MODULE_5__["InnerBlocks"].Content, null);
    }
  }],
  edit: function edit(_ref) {
    var attributes = _ref.attributes,
        setAttributes = _ref.setAttributes,
        className = _ref.className,
        insertBlocksAfter = _ref.insertBlocksAfter;
    var h = attributes.h,
        w = attributes.w,
        alignContent = attributes.alignContent;
    var styles = {
      gridColumnEnd: 'span ' + w,
      gridRowEnd: 'span ' + h,
      alignSelf: alignContent === 'center' ? 'center' : 'start'
    };
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_editor__WEBPACK_IMPORTED_MODULE_5__["InspectorControls"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["PanelBody"], {
      title: "Grid Item"
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["RangeControl"], {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Width'),
      value: w,
      onChange: function onChange(next) {
        setAttributes({
          w: next
        });
      },
      min: 1,
      max: 12
    }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["RangeControl"], {
      label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Span Rows'),
      value: h,
      onChange: function onChange(next) {
        setAttributes({
          h: next
        });
      },
      min: 1,
      max: 12
    }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_editor__WEBPACK_IMPORTED_MODULE_5__["BlockControls"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["Toolbar"], {
      controls: [{
        icon: 'arrow-up-alt',
        title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Align Top'),
        isActive: alignContent === 'top',
        onClick: function onClick() {
          return setAttributes({
            alignContent: 'top'
          });
        }
      }, {
        icon: 'align-center',
        title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Align Center'),
        isActive: alignContent === 'center',
        onClick: function onClick() {
          return setAttributes({
            alignContent: 'center'
          });
        }
      }, {
        icon: 'arrow-down-alt',
        title: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__["__"])('Align Bottom'),
        isActive: alignContent === 'bottom',
        onClick: function onClick() {
          return setAttributes({
            alignContent: 'bottom'
          });
        }
      }]
    })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "gecko-grid-layout-editor-styles",
      style: styles
    }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "wp-block-gecko-grid-layout-editor__wrap gecko-grid-layout-basic ".concat(attributes.className, " align-content-").concat(attributes.alignContent)
    }, typeof insertBlocksAfter === 'function' //This line makes sure styles do not break
    ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_editor__WEBPACK_IMPORTED_MODULE_5__["InnerBlocks"], {
      templateLock: false
    }) : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Lorem Ipsum") // This is what shows as the preview content.
    ));
  },
  save: function save() {
    // Honestly I don't want to do this but conventions are conventions.
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_wordpress_editor__WEBPACK_IMPORTED_MODULE_5__["InnerBlocks"].Content, null));
  }
};
Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_4__["registerBlockType"])(name, settings);

/***/ }),

/***/ "@wordpress/blocks":
/*!*****************************************!*\
  !*** external {"this":["wp","blocks"]} ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["blocks"]; }());

/***/ }),

/***/ "@wordpress/components":
/*!*********************************************!*\
  !*** external {"this":["wp","components"]} ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["components"]; }());

/***/ }),

/***/ "@wordpress/data":
/*!***************************************!*\
  !*** external {"this":["wp","data"]} ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["data"]; }());

/***/ }),

/***/ "@wordpress/editor":
/*!*****************************************!*\
  !*** external {"this":["wp","editor"]} ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["editor"]; }());

/***/ }),

/***/ "@wordpress/element":
/*!******************************************!*\
  !*** external {"this":["wp","element"]} ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["element"]; }());

/***/ }),

/***/ "@wordpress/i18n":
/*!***************************************!*\
  !*** external {"this":["wp","i18n"]} ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["i18n"]; }());

/***/ }),

/***/ "react":
/*!*********************************!*\
  !*** external {"this":"React"} ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["React"]; }());

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Jsb2Nrcy9ncmlkLWxheW91dC1iYXNpYy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwge1widGhpc1wiOltcIndwXCIsXCJibG9ja3NcIl19Iiwid2VicGFjazovLy9leHRlcm5hbCB7XCJ0aGlzXCI6W1wid3BcIixcImNvbXBvbmVudHNcIl19Iiwid2VicGFjazovLy9leHRlcm5hbCB7XCJ0aGlzXCI6W1wid3BcIixcImRhdGFcIl19Iiwid2VicGFjazovLy9leHRlcm5hbCB7XCJ0aGlzXCI6W1wid3BcIixcImVkaXRvclwiXX0iLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIHtcInRoaXNcIjpbXCJ3cFwiLFwiZWxlbWVudFwiXX0iLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIHtcInRoaXNcIjpbXCJ3cFwiLFwiaTE4blwiXX0iLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIHtcInRoaXNcIjpcIlJlYWN0XCJ9Il0sIm5hbWVzIjpbIm5hbWUiLCJzZXR0aW5ncyIsInRpdGxlIiwiX18iLCJwYXJlbnQiLCJpY29uIiwiZGVzY3JpcHRpb24iLCJjYXRlZ29yeSIsInN1cHBvcnRzIiwiY2xhc3NOYW1lIiwiaW5zZXJ0ZXIiLCJyZXVzYWJsZSIsImh0bWwiLCJzdHlsZXMiLCJsYWJlbCIsImlzRGVmYXVsdCIsImF0dHJpYnV0ZXMiLCJoIiwidHlwZSIsImRlZmF1bHQiLCJ3IiwiYWxpZ25Db250ZW50IiwidHJhbnNmb3JtcyIsImZyb20iLCJibG9ja3MiLCJ0cmFuc2Zvcm0iLCJpbm5lckJsb2NrcyIsInNlbGVjdGVkIiwic2VsZWN0IiwiZ2V0U2VsZWN0ZWRCbG9jayIsImNyZWF0ZUJsb2NrIiwiZGVwcmVjYXRlZCIsIm1pZ3JhdGUiLCJzYXZlIiwiZWRpdCIsInNldEF0dHJpYnV0ZXMiLCJpbnNlcnRCbG9ja3NBZnRlciIsImdyaWRDb2x1bW5FbmQiLCJncmlkUm93RW5kIiwiYWxpZ25TZWxmIiwibmV4dCIsImlzQWN0aXZlIiwib25DbGljayIsInJlZ2lzdGVyQmxvY2tUeXBlIl0sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEZBOzs7QUFHQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxJQUFNQSxJQUFJLEdBQUcseUJBQWI7QUFFQSxJQUFNQyxRQUFRLEdBQUc7QUFDaEJDLE9BQUssRUFBRUMsMERBQUUsQ0FBRSxPQUFGLENBRE87QUFHaEJDLFFBQU0sRUFBRSxDQUFDLG1CQUFELENBSFE7QUFLaEJDLE1BQUksRUFBRSwyREFBQyx5REFBRDtBQUFLLFdBQU8sRUFBQyxXQUFiO0FBQXlCLFNBQUssRUFBQztBQUEvQixLQUE0RCwyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxpQ0FBUjtBQUEwQyxRQUFJLEVBQUM7QUFBL0MsSUFBNUQsRUFBb0gsMkRBQUMsMERBQUQ7QUFBTSxLQUFDLEVBQUM7QUFBUixJQUFwSCxFQUFvUCwyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxJQUFSO0FBQWEsS0FBQyxFQUFDLEdBQWY7QUFBbUIsU0FBSyxFQUFDLEdBQXpCO0FBQTZCLFVBQU0sRUFBQztBQUFwQyxJQUFwUCxFQUE4UiwyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxJQUFSO0FBQWEsS0FBQyxFQUFDLElBQWY7QUFBb0IsU0FBSyxFQUFDLEdBQTFCO0FBQThCLFVBQU0sRUFBQztBQUFyQyxJQUE5UixFQUF5VSwyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxHQUFSO0FBQVksS0FBQyxFQUFDLEdBQWQ7QUFBa0IsU0FBSyxFQUFDLEdBQXhCO0FBQTRCLFVBQU0sRUFBQztBQUFuQyxJQUF6VSxFQUFrWCwyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxHQUFSO0FBQVksS0FBQyxFQUFDLElBQWQ7QUFBbUIsU0FBSyxFQUFDLEdBQXpCO0FBQTZCLFVBQU0sRUFBQztBQUFwQyxJQUFsWCxFQUE0WiwyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxHQUFSO0FBQVksS0FBQyxFQUFDLElBQWQ7QUFBbUIsU0FBSyxFQUFDLEdBQXpCO0FBQTZCLFVBQU0sRUFBQztBQUFwQyxJQUE1WixFQUFzYywyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxHQUFSO0FBQVksS0FBQyxFQUFDLEdBQWQ7QUFBa0IsU0FBSyxFQUFDLEdBQXhCO0FBQTRCLFVBQU0sRUFBQztBQUFuQyxJQUF0YyxFQUErZSwyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxHQUFSO0FBQVksS0FBQyxFQUFDLElBQWQ7QUFBbUIsU0FBSyxFQUFDLEdBQXpCO0FBQTZCLFVBQU0sRUFBQztBQUFwQyxJQUEvZSxFQUF5aEIsMkRBQUMsMERBQUQ7QUFBTSxLQUFDLEVBQUMsSUFBUjtBQUFhLEtBQUMsRUFBQyxJQUFmO0FBQW9CLFNBQUssRUFBQyxHQUExQjtBQUE4QixVQUFNLEVBQUM7QUFBckMsSUFBemhCLEVBQW9rQiwyREFBQywwREFBRDtBQUFNLEtBQUMsRUFBQyxJQUFSO0FBQWEsS0FBQyxFQUFDLEdBQWY7QUFBbUIsU0FBSyxFQUFDLEdBQXpCO0FBQTZCLFVBQU0sRUFBQztBQUFwQyxJQUFwa0IsRUFBOG1CLDJEQUFDLDBEQUFEO0FBQU0sS0FBQyxFQUFDLElBQVI7QUFBYSxLQUFDLEVBQUMsSUFBZjtBQUFvQixTQUFLLEVBQUMsR0FBMUI7QUFBOEIsVUFBTSxFQUFDO0FBQXJDLElBQTltQixFQUF5cEIsMkRBQUMsMERBQUQ7QUFBTSxLQUFDLEVBQUMsSUFBUjtBQUFhLEtBQUMsRUFBQyxHQUFmO0FBQW1CLFNBQUssRUFBQyxHQUF6QjtBQUE2QixVQUFNLEVBQUM7QUFBcEMsSUFBenBCLENBTFU7QUFPaEJDLGFBQVcsRUFBRUgsMERBQUUsQ0FBRSxzQkFBRixDQVBDO0FBU2hCSSxVQUFRLEVBQUUsUUFUTTtBQVdoQkMsVUFBUSxFQUFFO0FBQ1RDLGFBQVMsRUFBRSxJQURGO0FBRVRDLFlBQVEsRUFBRSxJQUZEO0FBR1RDLFlBQVEsRUFBRSxLQUhEO0FBSVRDLFFBQUksRUFBRTtBQUpHLEdBWE07QUFrQmhCQyxRQUFNLEVBQUUsQ0FDUDtBQUFFYixRQUFJLEVBQUUsU0FBUjtBQUFtQmMsU0FBSyxFQUFFWCwwREFBRSxDQUFFLFNBQUYsQ0FBNUI7QUFBMENZLGFBQVMsRUFBRTtBQUFyRCxHQURPLENBbEJRO0FBc0JoQkMsWUFBVSxFQUFFO0FBQ1hDLEtBQUMsRUFBRTtBQUFFQyxVQUFJLEVBQUUsUUFBUjtBQUFrQkMsYUFBTyxFQUFFO0FBQTNCLEtBRFE7QUFFWEMsS0FBQyxFQUFFO0FBQUVGLFVBQUksRUFBRSxRQUFSO0FBQWtCQyxhQUFPLEVBQUU7QUFBM0IsS0FGUTtBQUdYRSxnQkFBWSxFQUFFO0FBQUVILFVBQUksRUFBRSxRQUFSO0FBQWtCQyxhQUFPLEVBQUU7QUFBM0I7QUFISCxHQXRCSTtBQTRCaEJHLFlBQVUsRUFBRTtBQUNYQyxRQUFJLEVBQUUsQ0FDTDtBQUNDTCxVQUFJLEVBQUUsT0FEUDtBQUVDTSxZQUFNLEVBQUUsQ0FBQyx5QkFBRCxFQUE0Qix3QkFBNUIsRUFBc0Qsd0JBQXRELENBRlQ7QUFHQ0MsZUFBUyxFQUFFLG1CQUFDVCxVQUFELEVBQWFVLFdBQWIsRUFBNkI7QUFDdkMsWUFBTUMsUUFBUSxHQUFHQyw4REFBTSxDQUFDLGFBQUQsQ0FBTixDQUFzQkMsZ0JBQXRCLEVBQWpCLENBRHVDLENBQ29CO0FBQzNEOztBQUZ1QyxZQUdoQ1osQ0FIZ0MsR0FHeEJELFVBSHdCLENBR2hDQyxDQUhnQztBQUFBLFlBRzdCRyxDQUg2QixHQUd4QkosVUFId0IsQ0FHN0JJLENBSDZCO0FBSXZDLGVBQU9VLHFFQUFXLENBQUMseUJBQUQsRUFBNEI7QUFDN0NiLFdBQUMsRUFBRUEsQ0FEMEM7QUFFN0NHLFdBQUMsRUFBRUE7QUFGMEMsU0FBNUIsRUFHZk8sUUFBUSxDQUFDRCxXQUhNLENBQWxCO0FBSUE7QUFYRixLQURLO0FBREssR0E1Qkk7QUE4Q2hCSyxZQUFVLEVBQUUsQ0FBQztBQUNaQyxXQURZLHFCQUNGLENBQUUsQ0FEQTtBQUVaQyxRQUZZLGtCQUVMO0FBQ04sYUFBTywyREFBQyw2REFBRCxDQUFhLE9BQWIsT0FBUDtBQUNBO0FBSlcsR0FBRCxDQTlDSTtBQXFEaEJDLE1BckRnQixzQkFxRGtEO0FBQUEsUUFBM0RsQixVQUEyRCxRQUEzREEsVUFBMkQ7QUFBQSxRQUEvQ21CLGFBQStDLFFBQS9DQSxhQUErQztBQUFBLFFBQWhDMUIsU0FBZ0MsUUFBaENBLFNBQWdDO0FBQUEsUUFBckIyQixpQkFBcUIsUUFBckJBLGlCQUFxQjtBQUFBLFFBRWhFbkIsQ0FGZ0UsR0FLN0RELFVBTDZELENBRWhFQyxDQUZnRTtBQUFBLFFBR2hFRyxDQUhnRSxHQUs3REosVUFMNkQsQ0FHaEVJLENBSGdFO0FBQUEsUUFJaEVDLFlBSmdFLEdBSzdETCxVQUw2RCxDQUloRUssWUFKZ0U7QUFPakUsUUFBTVIsTUFBTSxHQUFHO0FBQ2R3QixtQkFBYSxFQUFFLFVBQVNqQixDQURWO0FBRWRrQixnQkFBVSxFQUFFLFVBQVVyQixDQUZSO0FBR2RzQixlQUFTLEVBQUdsQixZQUFZLEtBQUssUUFBbEIsR0FBNkIsUUFBN0IsR0FBdUM7QUFIcEMsS0FBZjtBQUtBLFdBQ0MsMkRBQUMsMkRBQUQsUUFDQywyREFBQyxtRUFBRCxRQUNDLDJEQUFDLCtEQUFEO0FBQVcsV0FBSyxFQUFDO0FBQWpCLE9BQ0MsMkRBQUMsa0VBQUQ7QUFDQyxXQUFLLEVBQUdsQiwwREFBRSxDQUFFLE9BQUYsQ0FEWDtBQUVDLFdBQUssRUFBR2lCLENBRlQ7QUFHQyxjQUFRLEVBQUcsa0JBQUVvQixJQUFGLEVBQVk7QUFDdEJMLHFCQUFhLENBQUU7QUFDZGYsV0FBQyxFQUFFb0I7QUFEVyxTQUFGLENBQWI7QUFHQSxPQVBGO0FBUUMsU0FBRyxFQUFHLENBUlA7QUFTQyxTQUFHLEVBQUc7QUFUUCxNQURELEVBWUMsMkRBQUMsa0VBQUQ7QUFDQyxXQUFLLEVBQUdyQywwREFBRSxDQUFFLFdBQUYsQ0FEWDtBQUVDLFdBQUssRUFBR2MsQ0FGVDtBQUdDLGNBQVEsRUFBRyxrQkFBRXVCLElBQUYsRUFBWTtBQUN0QkwscUJBQWEsQ0FBRTtBQUNkbEIsV0FBQyxFQUFFdUI7QUFEVyxTQUFGLENBQWI7QUFHQSxPQVBGO0FBUUMsU0FBRyxFQUFHLENBUlA7QUFTQyxTQUFHLEVBQUc7QUFUUCxNQVpELENBREQsQ0FERCxFQTJCQywyREFBQywrREFBRCxRQUNDLDJEQUFDLDZEQUFEO0FBQVMsY0FBUSxFQUFFLENBQ2xCO0FBQ0NuQyxZQUFJLEVBQUUsY0FEUDtBQUVDSCxhQUFLLEVBQUVDLDBEQUFFLENBQUMsV0FBRCxDQUZWO0FBR0NzQyxnQkFBUSxFQUFFcEIsWUFBWSxLQUFLLEtBSDVCO0FBSUNxQixlQUFPLEVBQUU7QUFBQSxpQkFBTVAsYUFBYSxDQUFDO0FBQzVCZCx3QkFBWSxFQUFFO0FBRGMsV0FBRCxDQUFuQjtBQUFBO0FBSlYsT0FEa0IsRUFTbEI7QUFDQ2hCLFlBQUksRUFBRSxjQURQO0FBRUNILGFBQUssRUFBRUMsMERBQUUsQ0FBQyxjQUFELENBRlY7QUFHQ3NDLGdCQUFRLEVBQUVwQixZQUFZLEtBQUssUUFINUI7QUFJQ3FCLGVBQU8sRUFBRTtBQUFBLGlCQUFNUCxhQUFhLENBQUM7QUFBQ2Qsd0JBQVksRUFBRTtBQUFmLFdBQUQsQ0FBbkI7QUFBQTtBQUpWLE9BVGtCLEVBZWxCO0FBQ0NoQixZQUFJLEVBQUUsZ0JBRFA7QUFFQ0gsYUFBSyxFQUFFQywwREFBRSxDQUFDLGNBQUQsQ0FGVjtBQUdDc0MsZ0JBQVEsRUFBRXBCLFlBQVksS0FBSyxRQUg1QjtBQUlDcUIsZUFBTyxFQUFFO0FBQUEsaUJBQU1QLGFBQWEsQ0FBQztBQUFDZCx3QkFBWSxFQUFFO0FBQWYsV0FBRCxDQUFuQjtBQUFBO0FBSlYsT0Fma0I7QUFBbkIsTUFERCxDQTNCRCxFQW9EQztBQUFLLGVBQVMsbUNBQWQ7QUFBbUQsV0FBSyxFQUFFUjtBQUExRCxNQXBERCxFQXFEQztBQUFLLGVBQVMsNEVBQ3NERyxVQUFVLENBQUNQLFNBRGpFLDRCQUM0Rk8sVUFBVSxDQUFDSyxZQUR2RztBQUFkLE9BR0csT0FBT2UsaUJBQVAsS0FBNkIsVUFBN0IsQ0FBd0M7QUFBeEMsTUFDQywyREFBQyw2REFBRDtBQUFhLGtCQUFZLEVBQUc7QUFBNUIsTUFERCxHQUVDLG9GQUxKLENBS3VCO0FBTHZCLEtBckRELENBREQ7QUFnRUEsR0FqSWU7QUFtSWhCSCxNQW5JZ0Isa0JBbUlUO0FBQ047QUFDQSxXQUFPLHdFQUFLLDJEQUFDLDZEQUFELENBQWEsT0FBYixPQUFMLENBQVA7QUFDQTtBQXRJZSxDQUFqQjtBQXdJQVUsMkVBQWlCLENBQUMzQyxJQUFELEVBQU9DLFFBQVAsQ0FBakIsQzs7Ozs7Ozs7Ozs7QUMzSkEsYUFBYSx1Q0FBdUMsRUFBRSxJOzs7Ozs7Ozs7OztBQ0F0RCxhQUFhLDJDQUEyQyxFQUFFLEk7Ozs7Ozs7Ozs7O0FDQTFELGFBQWEscUNBQXFDLEVBQUUsSTs7Ozs7Ozs7Ozs7QUNBcEQsYUFBYSx1Q0FBdUMsRUFBRSxJOzs7Ozs7Ozs7OztBQ0F0RCxhQUFhLHdDQUF3QyxFQUFFLEk7Ozs7Ozs7Ozs7O0FDQXZELGFBQWEscUNBQXFDLEVBQUUsSTs7Ozs7Ozs7Ozs7QUNBcEQsYUFBYSxnQ0FBZ0MsRUFBRSxJIiwiZmlsZSI6ImJsb2Nrcy9ncmlkLWxheW91dC1iYXNpYy9lZGl0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIi93cC1jb250ZW50L3BsdWdpbnMvZ2Vja28tZnJhbWV3b3JrL2J1aWxkL1wiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9ibG9ja3MvZ3JpZC1sYXlvdXQtYmFzaWMvaW5kZXguanNcIik7XG4iLCIvKipcbiAqIFdvcmRQcmVzcyBkZXBlbmRlbmNpZXNcbiAqL1xuaW1wb3J0IHtcblx0UGFuZWxCb2R5LFxuXHRSYW5nZUNvbnRyb2wsXG5cdFBhdGgsXG5cdFNWRyxcblx0UmVjdCxcblx0VG9vbGJhcixcbn0gZnJvbSAnQHdvcmRwcmVzcy9jb21wb25lbnRzJztcbmltcG9ydCB7IF9fIH0gZnJvbSAnQHdvcmRwcmVzcy9pMThuJztcbmltcG9ydCB7IEZyYWdtZW50IH0gZnJvbSAnQHdvcmRwcmVzcy9lbGVtZW50JztcbmltcG9ydCB7Y3JlYXRlQmxvY2ssIHJlZ2lzdGVyQmxvY2tUeXBlfSBmcm9tICdAd29yZHByZXNzL2Jsb2Nrcyc7XG5pbXBvcnQge0luc3BlY3RvckNvbnRyb2xzLElubmVyQmxvY2tzLEJsb2NrQ29udHJvbHN9IGZyb20gJ0B3b3JkcHJlc3MvZWRpdG9yJztcbmltcG9ydCB7c2VsZWN0fSBmcm9tICdAd29yZHByZXNzL2RhdGEnO1xuXG5jb25zdCBuYW1lID0gJ2dlY2tvL2dyaWQtbGF5b3V0LWJhc2ljJztcblxuY29uc3Qgc2V0dGluZ3MgPSB7XG5cdHRpdGxlOiBfXyggJ0Jhc2ljJyApLFxuXG5cdHBhcmVudDogWydnZWNrby9ncmlkLWxheW91dCddLFxuXG5cdGljb246IDxTVkcgdmlld0JveD1cIjAgMCAyNCAyNFwiIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIj48UGF0aCBkPVwiTTAsMGgyNHYyNEgwVjB6IE0wLDBoMjR2MjRIMFYwelwiIGZpbGw9XCJub25lXCIgLz48UGF0aCBkPVwibTIwIDd2MTBoLTE2di0xMGgxNm0wLTJoLTE2Yy0xLjEgMC0xLjk5IDAuOS0xLjk5IDJsLTAuMDEgMTBjMCAxLjEgMC45IDIgMiAyaDE2YzEuMSAwIDItMC45IDItMnYtMTBjMC0xLjEtMC45LTItMi0yelwiIC8+PFJlY3QgeD1cIjExXCIgeT1cIjhcIiB3aWR0aD1cIjJcIiBoZWlnaHQ9XCIyXCIgLz48UmVjdCB4PVwiMTFcIiB5PVwiMTFcIiB3aWR0aD1cIjJcIiBoZWlnaHQ9XCIyXCIgLz48UmVjdCB4PVwiOFwiIHk9XCI4XCIgd2lkdGg9XCIyXCIgaGVpZ2h0PVwiMlwiIC8+PFJlY3QgeD1cIjhcIiB5PVwiMTFcIiB3aWR0aD1cIjJcIiBoZWlnaHQ9XCIyXCIgLz48UmVjdCB4PVwiNVwiIHk9XCIxMVwiIHdpZHRoPVwiMlwiIGhlaWdodD1cIjJcIiAvPjxSZWN0IHg9XCI1XCIgeT1cIjhcIiB3aWR0aD1cIjJcIiBoZWlnaHQ9XCIyXCIgLz48UmVjdCB4PVwiOFwiIHk9XCIxNFwiIHdpZHRoPVwiOFwiIGhlaWdodD1cIjJcIiAvPjxSZWN0IHg9XCIxNFwiIHk9XCIxMVwiIHdpZHRoPVwiMlwiIGhlaWdodD1cIjJcIiAvPjxSZWN0IHg9XCIxNFwiIHk9XCI4XCIgd2lkdGg9XCIyXCIgaGVpZ2h0PVwiMlwiIC8+PFJlY3QgeD1cIjE3XCIgeT1cIjExXCIgd2lkdGg9XCIyXCIgaGVpZ2h0PVwiMlwiIC8+PFJlY3QgeD1cIjE3XCIgeT1cIjhcIiB3aWR0aD1cIjJcIiBoZWlnaHQ9XCIyXCIgLz48L1NWRz4sXG5cblx0ZGVzY3JpcHRpb246IF9fKCAnQW4gaW1hZ2UgZ3JpZCBibG9jay4nICksXG5cblx0Y2F0ZWdvcnk6ICdjb21tb24nLFxuXG5cdHN1cHBvcnRzOiB7XG5cdFx0Y2xhc3NOYW1lOiB0cnVlLFxuXHRcdGluc2VydGVyOiB0cnVlLFxuXHRcdHJldXNhYmxlOiBmYWxzZSxcblx0XHRodG1sOiBmYWxzZSxcblx0fSxcblxuXHRzdHlsZXM6IFtcblx0XHR7IG5hbWU6ICdkZWZhdWx0JywgbGFiZWw6IF9fKCAnRGVmYXVsdCcpLCBpc0RlZmF1bHQ6IHRydWUgfSxcblx0XSxcblxuXHRhdHRyaWJ1dGVzOiB7XG5cdFx0aDogeyB0eXBlOiAnbnVtYmVyJywgZGVmYXVsdDogMX0sXG5cdFx0dzogeyB0eXBlOiAnbnVtYmVyJywgZGVmYXVsdDogMX0sXG5cdFx0YWxpZ25Db250ZW50OiB7IHR5cGU6ICdzdHJpbmcnLCBkZWZhdWx0OiAndG9wJ30sXG5cdH0sXG5cblx0dHJhbnNmb3Jtczoge1xuXHRcdGZyb206IFtcblx0XHRcdHtcblx0XHRcdFx0dHlwZTogJ2Jsb2NrJyxcblx0XHRcdFx0YmxvY2tzOiBbJ2dlY2tvL2dyaWQtbGF5b3V0LWltYWdlJywgJ2dlY2tvL2dyaWQtbGF5b3V0LWl0ZW0nLCAnZ2Vja28vZ3JpZC1sYXlvdXQtY2FyZCddLFxuXHRcdFx0XHR0cmFuc2Zvcm06IChhdHRyaWJ1dGVzLCBpbm5lckJsb2NrcykgPT4ge1xuXHRcdFx0XHRcdGNvbnN0IHNlbGVjdGVkID0gc2VsZWN0KCdjb3JlL2VkaXRvcicpLmdldFNlbGVjdGVkQmxvY2soKTsgLy8gYmVjYXVzZSBpbm5lckJsb2NrcyBkb2VzIG5vdCB3b3JrLlxuXHRcdFx0XHRcdC8vIEl0IGFwcGVhcnMgdGhhdCBpbm5lckJsb2NrcyB3aWxsIGJlIGFkZGVkIGluIHRoZSBmdXR1cmUuXG5cdFx0XHRcdFx0Y29uc3Qge2gsIHd9ID0gYXR0cmlidXRlcztcblx0XHRcdFx0XHRyZXR1cm4gY3JlYXRlQmxvY2soJ2dlY2tvL2dyaWQtbGF5b3V0LWJhc2ljJywge1xuXHRcdFx0XHRcdFx0aDogaCxcblx0XHRcdFx0XHRcdHc6IHcsXG5cdFx0XHRcdFx0fSwgc2VsZWN0ZWQuaW5uZXJCbG9ja3MpO1xuXHRcdFx0XHR9LFxuXHRcdFx0fSxcdFxuXHRcdF0sXG5cdH0sXG5cblx0ZGVwcmVjYXRlZDogW3tcblx0XHRtaWdyYXRlKCkge30sXG5cdFx0c2F2ZSgpIHtcblx0XHRcdHJldHVybiA8SW5uZXJCbG9ja3MuQ29udGVudCAvID4gO1xuXHRcdH0sXG5cdH1dLFxuXG5cdGVkaXQoeyBhdHRyaWJ1dGVzLCBzZXRBdHRyaWJ1dGVzLCBjbGFzc05hbWUsIGluc2VydEJsb2Nrc0FmdGVyIH0pIHtcblx0XHRjb25zdCB7XG5cdFx0XHRoLFxuXHRcdFx0dyxcblx0XHRcdGFsaWduQ29udGVudCxcblx0XHR9ID0gYXR0cmlidXRlcztcblx0XHRcblx0XHRjb25zdCBzdHlsZXMgPSB7XG5cdFx0XHRncmlkQ29sdW1uRW5kOiAnc3BhbiAnKyB3LFxuXHRcdFx0Z3JpZFJvd0VuZDogJ3NwYW4gJyArIGgsXG5cdFx0XHRhbGlnblNlbGY6IChhbGlnbkNvbnRlbnQgPT09ICdjZW50ZXInKT8gJ2NlbnRlcic6ICdzdGFydCcsXG5cdFx0fTtcblx0XHRyZXR1cm4gKFxuXHRcdFx0PEZyYWdtZW50PlxuXHRcdFx0XHQ8SW5zcGVjdG9yQ29udHJvbHM+XG5cdFx0XHRcdFx0PFBhbmVsQm9keSB0aXRsZT1cIkdyaWQgSXRlbVwiPlxuXHRcdFx0XHRcdFx0PFJhbmdlQ29udHJvbFxuXHRcdFx0XHRcdFx0XHRsYWJlbD17IF9fKCAnV2lkdGgnICkgfVxuXHRcdFx0XHRcdFx0XHR2YWx1ZT17IHcgfVxuXHRcdFx0XHRcdFx0XHRvbkNoYW5nZT17ICggbmV4dCApID0+IHtcblx0XHRcdFx0XHRcdFx0XHRzZXRBdHRyaWJ1dGVzKCB7XG5cdFx0XHRcdFx0XHRcdFx0XHR3OiBuZXh0LFxuXHRcdFx0XHRcdFx0XHRcdH0gKTtcblx0XHRcdFx0XHRcdFx0fSB9XG5cdFx0XHRcdFx0XHRcdG1pbj17IDEgfVxuXHRcdFx0XHRcdFx0XHRtYXg9eyAxMiB9XG5cdFx0XHRcdFx0XHQvPlxuXHRcdFx0XHRcdFx0PFJhbmdlQ29udHJvbFxuXHRcdFx0XHRcdFx0XHRsYWJlbD17IF9fKCAnU3BhbiBSb3dzJyApIH1cblx0XHRcdFx0XHRcdFx0dmFsdWU9eyBoIH1cblx0XHRcdFx0XHRcdFx0b25DaGFuZ2U9eyAoIG5leHQgKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0c2V0QXR0cmlidXRlcygge1xuXHRcdFx0XHRcdFx0XHRcdFx0aDogbmV4dCxcblx0XHRcdFx0XHRcdFx0XHR9ICk7XG5cdFx0XHRcdFx0XHRcdH0gfVxuXHRcdFx0XHRcdFx0XHRtaW49eyAxIH1cblx0XHRcdFx0XHRcdFx0bWF4PXsgMTIgfVxuXHRcdFx0XHRcdFx0Lz5cblx0XHRcdFx0XHQ8L1BhbmVsQm9keT5cblx0XHRcdFx0PC9JbnNwZWN0b3JDb250cm9scz5cblx0XHRcdFx0PEJsb2NrQ29udHJvbHM+XG5cdFx0XHRcdFx0PFRvb2xiYXIgY29udHJvbHM9e1tcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0aWNvbjogJ2Fycm93LXVwLWFsdCcsXG5cdFx0XHRcdFx0XHRcdHRpdGxlOiBfXygnQWxpZ24gVG9wJyksXG5cdFx0XHRcdFx0XHRcdGlzQWN0aXZlOiBhbGlnbkNvbnRlbnQgPT09ICd0b3AnLFxuXHRcdFx0XHRcdFx0XHRvbkNsaWNrOiAoKSA9PiBzZXRBdHRyaWJ1dGVzKHtcblx0XHRcdFx0XHRcdFx0XHRhbGlnbkNvbnRlbnQ6ICd0b3AnXG5cdFx0XHRcdFx0XHRcdH0pLFxuXHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0aWNvbjogJ2FsaWduLWNlbnRlcicsXG5cdFx0XHRcdFx0XHRcdHRpdGxlOiBfXygnQWxpZ24gQ2VudGVyJyksXG5cdFx0XHRcdFx0XHRcdGlzQWN0aXZlOiBhbGlnbkNvbnRlbnQgPT09ICdjZW50ZXInLFxuXHRcdFx0XHRcdFx0XHRvbkNsaWNrOiAoKSA9PiBzZXRBdHRyaWJ1dGVzKHthbGlnbkNvbnRlbnQ6ICdjZW50ZXInfSksXG5cdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRpY29uOiAnYXJyb3ctZG93bi1hbHQnLFxuXHRcdFx0XHRcdFx0XHR0aXRsZTogX18oJ0FsaWduIEJvdHRvbScpLFxuXHRcdFx0XHRcdFx0XHRpc0FjdGl2ZTogYWxpZ25Db250ZW50ID09PSAnYm90dG9tJyxcblx0XHRcdFx0XHRcdFx0b25DbGljazogKCkgPT4gc2V0QXR0cmlidXRlcyh7YWxpZ25Db250ZW50OiAnYm90dG9tJ30pLFxuXHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRdfT5cblx0XHRcdFx0XHQ8L1Rvb2xiYXI+XG5cdFx0XHRcdDwvQmxvY2tDb250cm9scz5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9e2BnZWNrby1ncmlkLWxheW91dC1lZGl0b3Itc3R5bGVzYH0gc3R5bGU9e3N0eWxlc30+PC9kaXY+XG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0ge1xuXHRcdFx0XHRcdGB3cC1ibG9jay1nZWNrby1ncmlkLWxheW91dC1lZGl0b3JfX3dyYXAgZ2Vja28tZ3JpZC1sYXlvdXQtYmFzaWMgJHthdHRyaWJ1dGVzLmNsYXNzTmFtZX0gYWxpZ24tY29udGVudC0ke2F0dHJpYnV0ZXMuYWxpZ25Db250ZW50fWBcblx0XHRcdFx0fSA+XG5cdFx0XHRcdFx0eyB0eXBlb2YgaW5zZXJ0QmxvY2tzQWZ0ZXIgPT09ICdmdW5jdGlvbicgLy9UaGlzIGxpbmUgbWFrZXMgc3VyZSBzdHlsZXMgZG8gbm90IGJyZWFrXG5cdFx0XHRcdFx0XHQ/IDxJbm5lckJsb2NrcyB0ZW1wbGF0ZUxvY2s9eyBmYWxzZSB9Lz5cblx0XHRcdFx0XHRcdDogPHA+TG9yZW0gSXBzdW08L3A+IC8vIFRoaXMgaXMgd2hhdCBzaG93cyBhcyB0aGUgcHJldmlldyBjb250ZW50LlxuXHRcdFx0XHRcdH1cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHQ8L0ZyYWdtZW50PlxuXHRcdCk7XG5cdH0sXG5cblx0c2F2ZSgpIHtcblx0XHQvLyBIb25lc3RseSBJIGRvbid0IHdhbnQgdG8gZG8gdGhpcyBidXQgY29udmVudGlvbnMgYXJlIGNvbnZlbnRpb25zLlxuXHRcdHJldHVybig8ZGl2PjxJbm5lckJsb2Nrcy5Db250ZW50IC8+PC9kaXY+KTsgXG5cdH0sXG59O1xucmVnaXN0ZXJCbG9ja1R5cGUobmFtZSwgc2V0dGluZ3MpO1xuIiwiKGZ1bmN0aW9uKCkgeyBtb2R1bGUuZXhwb3J0cyA9IHRoaXNbXCJ3cFwiXVtcImJsb2Nrc1wiXTsgfSgpKTsiLCIoZnVuY3Rpb24oKSB7IG1vZHVsZS5leHBvcnRzID0gdGhpc1tcIndwXCJdW1wiY29tcG9uZW50c1wiXTsgfSgpKTsiLCIoZnVuY3Rpb24oKSB7IG1vZHVsZS5leHBvcnRzID0gdGhpc1tcIndwXCJdW1wiZGF0YVwiXTsgfSgpKTsiLCIoZnVuY3Rpb24oKSB7IG1vZHVsZS5leHBvcnRzID0gdGhpc1tcIndwXCJdW1wiZWRpdG9yXCJdOyB9KCkpOyIsIihmdW5jdGlvbigpIHsgbW9kdWxlLmV4cG9ydHMgPSB0aGlzW1wid3BcIl1bXCJlbGVtZW50XCJdOyB9KCkpOyIsIihmdW5jdGlvbigpIHsgbW9kdWxlLmV4cG9ydHMgPSB0aGlzW1wid3BcIl1bXCJpMThuXCJdOyB9KCkpOyIsIihmdW5jdGlvbigpIHsgbW9kdWxlLmV4cG9ydHMgPSB0aGlzW1wiUmVhY3RcIl07IH0oKSk7Il0sInNvdXJjZVJvb3QiOiIifQ==