<?php
/**
 * Grid Basic Callback
 */
$render = function ( $attributes, $content ) {
	// Defaults and attributes
	// Setting to false unless needed because all styles do not need to be inlined
	$defaults = array(
		'w' => false,
		'h' => false,
		'alignContent' => false,
		'className' => false,
	);
	// Add a filter to hook into the default args
	$defaults = apply_filters( 'gecko/grid-layout-basic/defaults', $defaults, $attributes );
	$atts = wp_parse_args( $attributes, $defaults );

	$classNames = array('wp-block-gecko-grid-layout__item');
	$classNames[] = 'gecko-grid-layout-item';
	if($atts['className']) $classNames[] = $atts['className'];
	if($atts['alignContent']) $classNames[] = 'align-content-'.$atts['alignContent'];
	// Add a filter to hook into classNames
	$classNames = apply_filters( 'gecko/grid-layout-basic/class', $classNames, $attributes );

	$styles = array();
	$styles['grid-column-end'] = ($atts['w'])?'span '.$atts['w']:false;
	$styles['grid-row-end'] = ($atts['h'])?'span '.$atts['h']:false;
	$styles['-ms-grid-column-span'] = ($atts['w'])? $atts['w']:false;
	// $styles['-ms-grid-row-span'] = ($atts['h'])? $atts['h']:false;
	// Add a filter to hook into the inine styles $args = ($styles, $atts)
	$styles = apply_filters( 'gecko/grid-layout-basic/style', $styles, $attributes );

	$style = '';
	foreach ($styles as $key => $value) {
		if($value) $style .= $key.':'.$value.';';
	}
	$class = implode(' ', $classNames);
	$template = '<div class="%s" style="%s">%s</div>';
	$output = sprintf($template, $class, $style, $content);
	return apply_filters( 'gecko/grid-layout-basic/output', $output, $class, $style, $content, $attributes );
};