<?php
/**
	* If the block is dynamic you would render the template here.
	*/
$render = function ( $attributes, $content ) {
	if(!is_user_logged_in()) return;
	// Defaults and attributes
	// Setting to false unless needed because all styles do not need to be inlined
	$defaults = array(
		'size' => false,
	);
	// Add a filter to hook into the default args
	$defaults = apply_filters( 'gecko/permissions/defaults', $defaults, $attributes );
	$atts = wp_parse_args( $attributes, $defaults );
	return $content;
};