<?php
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS|JSON_HEX_QUOT);

	$classBase = 'gecko-tabs__panel';

	$classes = [
		$classBase
	];

	if (isset($attributes['className']) && $attributes['className'] != "") {
		$classes[] = $attributes['className'];
	}

	$markup = '<section class="' . implode(' ', $classes) . '" data-props="%s">%s</section>';

	return sprintf(
		$markup,
		htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'),
		$content
	);
};