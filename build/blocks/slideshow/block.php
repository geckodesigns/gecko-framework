<?php
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS | JSON_HEX_QUOT);

	$container_classes = [
		'gecko-slideshow',
		'swiper-container',
	];
	// var_dump($attributes);
	if (isset($attributes['fullHeight']) && $attributes['fullHeight']) {
		$container_classes[] = 'gecko-slideshow--full-height';
	}

	if (isset($attributes['enablePagination']) && $attributes['enablePagination']) {
		$container_classes[] = 'gecko-slideshow--has-pagination';
	}
	if (isset($attributes['enableNavArrows']) && $attributes['enableNavArrows']) {
		$container_classes[] = 'gecko-slideshow--has-nav-arrows';
	}

	$markup = '<div class="'.implode(' ', $container_classes).'" data-props="%s">';
		$markup .= '<div class="gecko-slideshow__wrapper swiper-wrapper">%s</div>';

		if (isset($attributes['enablePagination']) && $attributes['enablePagination']) {
			$markup .= '<div class="swiper-pagination"></div>';
		}

		if (isset($attributes['enableNavArrows']) && $attributes['enableNavArrows']) {
			$markup .= '<div class="swiper-button-next"></div>';
			$markup .= '<div class="swiper-button-prev"></div>';
		}

	$markup .= '</div>';

	return sprintf(
		$markup,
		htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'),
		$content
	);
};