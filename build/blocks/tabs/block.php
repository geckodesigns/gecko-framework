<?php
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS | JSON_HEX_QUOT);

	$container_classes = [
		'gecko-tabs'
	];

	if ($attributes['className'] && $attributes['className'] != '') {
		$container_classes[] = $attributes['className'];
	}

	$markup = '<section class="'.implode(' ', $container_classes).'" data-props="%s">';
		$markup .= '<section class="gecko-tabs__panel-wrapper">%s</section>';

	$markup .= '</section>';

	return sprintf(
		$markup,
		htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'),
		$content
	);
};