/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/plugins/gecko-framework/build/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/blocks/tabs/public.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

module.exports = _arrayWithoutHoles;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/iterableToArray.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArray.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

module.exports = _iterableToArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/nonIterableSpread.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableSpread.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

module.exports = _nonIterableSpread;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/toConsumableArray.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toConsumableArray.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithoutHoles = __webpack_require__(/*! ./arrayWithoutHoles */ "./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js");

var iterableToArray = __webpack_require__(/*! ./iterableToArray */ "./node_modules/@babel/runtime/helpers/iterableToArray.js");

var nonIterableSpread = __webpack_require__(/*! ./nonIterableSpread */ "./node_modules/@babel/runtime/helpers/nonIterableSpread.js");

function _toConsumableArray(arr) {
  return arrayWithoutHoles(arr) || iterableToArray(arr) || nonIterableSpread();
}

module.exports = _toConsumableArray;

/***/ }),

/***/ "./src/blocks/tabs/public.js":
/*!***********************************!*\
  !*** ./src/blocks/tabs/public.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ "./node_modules/@babel/runtime/helpers/toConsumableArray.js");
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__);

document.addEventListener("DOMContentLoaded", function (event) {
  var tabBlocks = document.getElementsByClassName('gecko-tabs');

  _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(tabBlocks).forEach(function (block, index) {
    // Create nav element and insert into DOM in gecko-tabs element
    var tabNav = document.createElement("nav");
    tabNav.className = "gecko-tabs__nav";
    block.insertBefore(tabNav, block.childNodes[0]); // Get block index

    var blockIndex = index;
    var styleActive = 'active';
    var styleInactive = 'inactive';
    var statusActive = 'visible';
    var statusInactive = 'hidden'; // Get child panels

    var panels = block.querySelectorAll('.gecko-tabs__panel');

    _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(panels).forEach(function (panel, index) {
      // Get panel data
      var attributes = JSON.parse(panel.getAttribute('data-props')); // Create unique panelId to use as data-target

      var panelId = "panel_" + blockIndex + "_" + index; // Set panelId as data-target on panel

      panel.setAttribute('data-target', panelId);
      panel.setAttribute('data-status', 'hidden'); // create Nav Button; add classes, text and data-target. Insert the nav button into the nav DOM element.

      var navButton = document.createElement("button");
      navButton.classList = "gecko-tabs__nav_button gecko-tabs--button";
      navButton.setAttribute('data-target', panelId);
      navButton.setAttribute('data-style', styleInactive);
      var navButtonText = document.createTextNode(attributes.panelTitle);
      navButton.appendChild(navButtonText);
      tabNav.appendChild(navButton);

      if (index === 0) {
        panel.classList.add('active');
        panel.setAttribute('data-status', statusActive);
        navButton.classList.add('active');
        navButton.setAttribute('data-style', styleActive);
      }

      navButton.addEventListener("click", function (e) {
        var clickedButton = e.target;
        var buttonNodeList = tabNav.childNodes;
        buttonNodeList.forEach(function (button) {
          button.classList.remove('active');
          button.setAttribute('data-style', styleInactive);
        });
        clickedButton.classList.add('active');
        clickedButton.setAttribute('data-style', styleActive);
        var dataTarget = clickedButton.getAttribute('data-target');
        panels.forEach(function (panel) {
          if (panel.getAttribute('data-status') === statusActive) {
            panel.setAttribute('data-status', statusInactive);
            panel.classList.remove(styleActive);
          }

          if (panel.getAttribute('data-target') === dataTarget) {
            panel.setAttribute('data-status', statusActive);
            panel.classList.add(styleActive);
          }
        });
      });
    });
  });
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvYXJyYXlXaXRob3V0SG9sZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvaXRlcmFibGVUb0FycmF5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL25vbkl0ZXJhYmxlU3ByZWFkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL3RvQ29uc3VtYWJsZUFycmF5LmpzIiwid2VicGFjazovLy8uL3NyYy9ibG9ja3MvdGFicy9wdWJsaWMuanMiXSwibmFtZXMiOlsiZG9jdW1lbnQiLCJhZGRFdmVudExpc3RlbmVyIiwiZXZlbnQiLCJ0YWJCbG9ja3MiLCJnZXRFbGVtZW50c0J5Q2xhc3NOYW1lIiwiZm9yRWFjaCIsImJsb2NrIiwiaW5kZXgiLCJ0YWJOYXYiLCJjcmVhdGVFbGVtZW50IiwiY2xhc3NOYW1lIiwiaW5zZXJ0QmVmb3JlIiwiY2hpbGROb2RlcyIsImJsb2NrSW5kZXgiLCJzdHlsZUFjdGl2ZSIsInN0eWxlSW5hY3RpdmUiLCJzdGF0dXNBY3RpdmUiLCJzdGF0dXNJbmFjdGl2ZSIsInBhbmVscyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJwYW5lbCIsImF0dHJpYnV0ZXMiLCJKU09OIiwicGFyc2UiLCJnZXRBdHRyaWJ1dGUiLCJwYW5lbElkIiwic2V0QXR0cmlidXRlIiwibmF2QnV0dG9uIiwiY2xhc3NMaXN0IiwibmF2QnV0dG9uVGV4dCIsImNyZWF0ZVRleHROb2RlIiwicGFuZWxUaXRsZSIsImFwcGVuZENoaWxkIiwiYWRkIiwiZSIsImNsaWNrZWRCdXR0b24iLCJ0YXJnZXQiLCJidXR0b25Ob2RlTGlzdCIsImJ1dHRvbiIsInJlbW92ZSIsImRhdGFUYXJnZXQiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0EsaURBQWlELGdCQUFnQjtBQUNqRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxvQzs7Ozs7Ozs7Ozs7QUNWQTtBQUNBO0FBQ0E7O0FBRUEsa0M7Ozs7Ozs7Ozs7O0FDSkE7QUFDQTtBQUNBOztBQUVBLG9DOzs7Ozs7Ozs7OztBQ0pBLHdCQUF3QixtQkFBTyxDQUFDLHVGQUFxQjs7QUFFckQsc0JBQXNCLG1CQUFPLENBQUMsbUZBQW1COztBQUVqRCx3QkFBd0IsbUJBQU8sQ0FBQyx1RkFBcUI7O0FBRXJEO0FBQ0E7QUFDQTs7QUFFQSxvQzs7Ozs7Ozs7Ozs7Ozs7OztBQ1ZBQSxRQUFRLENBQUNDLGdCQUFULENBQTBCLGtCQUExQixFQUE4QyxVQUFDQyxLQUFELEVBQVc7QUFDckQsTUFBTUMsU0FBUyxHQUFHSCxRQUFRLENBQUNJLHNCQUFULENBQWdDLFlBQWhDLENBQWxCOztBQUNBLGtGQUFJRCxTQUFKLEVBQWVFLE9BQWYsQ0FBdUIsVUFBQ0MsS0FBRCxFQUFRQyxLQUFSLEVBQWtCO0FBQ3JDO0FBQ0EsUUFBTUMsTUFBTSxHQUFHUixRQUFRLENBQUNTLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZjtBQUNBRCxVQUFNLENBQUNFLFNBQVAsR0FBbUIsaUJBQW5CO0FBQ0FKLFNBQUssQ0FBQ0ssWUFBTixDQUFtQkgsTUFBbkIsRUFBMkJGLEtBQUssQ0FBQ00sVUFBTixDQUFpQixDQUFqQixDQUEzQixFQUpxQyxDQU1yQzs7QUFDQSxRQUFNQyxVQUFVLEdBQUdOLEtBQW5CO0FBQ0EsUUFBTU8sV0FBVyxHQUFHLFFBQXBCO0FBQ0EsUUFBTUMsYUFBYSxHQUFHLFVBQXRCO0FBQ0EsUUFBTUMsWUFBWSxHQUFHLFNBQXJCO0FBQ0EsUUFBTUMsY0FBYyxHQUFHLFFBQXZCLENBWHFDLENBYXJDOztBQUNBLFFBQU1DLE1BQU0sR0FBR1osS0FBSyxDQUFDYSxnQkFBTixDQUF1QixvQkFBdkIsQ0FBZjs7QUFDQSxvRkFBSUQsTUFBSixFQUFZYixPQUFaLENBQW9CLFVBQUNlLEtBQUQsRUFBUWIsS0FBUixFQUFrQjtBQUNsQztBQUNBLFVBQU1jLFVBQVUsR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdILEtBQUssQ0FBQ0ksWUFBTixDQUFtQixZQUFuQixDQUFYLENBQW5CLENBRmtDLENBR2xDOztBQUNBLFVBQU1DLE9BQU8sR0FBRyxXQUFXWixVQUFYLEdBQXdCLEdBQXhCLEdBQThCTixLQUE5QyxDQUprQyxDQUtsQzs7QUFDQWEsV0FBSyxDQUFDTSxZQUFOLENBQW1CLGFBQW5CLEVBQWtDRCxPQUFsQztBQUNBTCxXQUFLLENBQUNNLFlBQU4sQ0FBbUIsYUFBbkIsRUFBa0MsUUFBbEMsRUFQa0MsQ0FTbEM7O0FBQ0EsVUFBTUMsU0FBUyxHQUFHM0IsUUFBUSxDQUFDUyxhQUFULENBQXVCLFFBQXZCLENBQWxCO0FBQ0FrQixlQUFTLENBQUNDLFNBQVYsR0FBc0IsMkNBQXRCO0FBQ0FELGVBQVMsQ0FBQ0QsWUFBVixDQUF1QixhQUF2QixFQUFzQ0QsT0FBdEM7QUFDQUUsZUFBUyxDQUFDRCxZQUFWLENBQXVCLFlBQXZCLEVBQXFDWCxhQUFyQztBQUNBLFVBQU1jLGFBQWEsR0FBRzdCLFFBQVEsQ0FBQzhCLGNBQVQsQ0FBd0JULFVBQVUsQ0FBQ1UsVUFBbkMsQ0FBdEI7QUFDQUosZUFBUyxDQUFDSyxXQUFWLENBQXNCSCxhQUF0QjtBQUNBckIsWUFBTSxDQUFDd0IsV0FBUCxDQUFtQkwsU0FBbkI7O0FBRUEsVUFBSXBCLEtBQUssS0FBSyxDQUFkLEVBQWdCO0FBQ1phLGFBQUssQ0FBQ1EsU0FBTixDQUFnQkssR0FBaEIsQ0FBb0IsUUFBcEI7QUFDQWIsYUFBSyxDQUFDTSxZQUFOLENBQW1CLGFBQW5CLEVBQWtDVixZQUFsQztBQUNBVyxpQkFBUyxDQUFDQyxTQUFWLENBQW9CSyxHQUFwQixDQUF3QixRQUF4QjtBQUNBTixpQkFBUyxDQUFDRCxZQUFWLENBQXVCLFlBQXZCLEVBQXFDWixXQUFyQztBQUNIOztBQUVEYSxlQUFTLENBQUMxQixnQkFBVixDQUEyQixPQUEzQixFQUFvQyxVQUFDaUMsQ0FBRCxFQUFPO0FBRXZDLFlBQU1DLGFBQWEsR0FBR0QsQ0FBQyxDQUFDRSxNQUF4QjtBQUNBLFlBQU1DLGNBQWMsR0FBRzdCLE1BQU0sQ0FBQ0ksVUFBOUI7QUFDQXlCLHNCQUFjLENBQUNoQyxPQUFmLENBQXVCLFVBQUFpQyxNQUFNLEVBQUk7QUFDN0JBLGdCQUFNLENBQUNWLFNBQVAsQ0FBaUJXLE1BQWpCLENBQXdCLFFBQXhCO0FBQ0FELGdCQUFNLENBQUNaLFlBQVAsQ0FBb0IsWUFBcEIsRUFBa0NYLGFBQWxDO0FBQ0gsU0FIRDtBQUlBb0IscUJBQWEsQ0FBQ1AsU0FBZCxDQUF3QkssR0FBeEIsQ0FBNEIsUUFBNUI7QUFDQUUscUJBQWEsQ0FBQ1QsWUFBZCxDQUEyQixZQUEzQixFQUF5Q1osV0FBekM7QUFFQSxZQUFNMEIsVUFBVSxHQUFHTCxhQUFhLENBQUNYLFlBQWQsQ0FBMkIsYUFBM0IsQ0FBbkI7QUFDQU4sY0FBTSxDQUFDYixPQUFQLENBQWUsVUFBQWUsS0FBSyxFQUFJO0FBQ3BCLGNBQUtBLEtBQUssQ0FBQ0ksWUFBTixDQUFtQixhQUFuQixNQUFzQ1IsWUFBM0MsRUFBeUQ7QUFDckRJLGlCQUFLLENBQUNNLFlBQU4sQ0FBbUIsYUFBbkIsRUFBa0NULGNBQWxDO0FBQ0FHLGlCQUFLLENBQUNRLFNBQU4sQ0FBZ0JXLE1BQWhCLENBQXVCekIsV0FBdkI7QUFDSDs7QUFDRCxjQUFLTSxLQUFLLENBQUNJLFlBQU4sQ0FBbUIsYUFBbkIsTUFBc0NnQixVQUEzQyxFQUFzRDtBQUNsRHBCLGlCQUFLLENBQUNNLFlBQU4sQ0FBbUIsYUFBbkIsRUFBa0NWLFlBQWxDO0FBQ0FJLGlCQUFLLENBQUNRLFNBQU4sQ0FBZ0JLLEdBQWhCLENBQW9CbkIsV0FBcEI7QUFDSDtBQUNKLFNBVEQ7QUFVSCxPQXRCRDtBQXVCSCxLQWhERDtBQWlESCxHQWhFRDtBQWlFSCxDQW5FRCxFIiwiZmlsZSI6ImJsb2Nrcy90YWJzL3B1YmxpYy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL3dwLWNvbnRlbnQvcGx1Z2lucy9nZWNrby1mcmFtZXdvcmsvYnVpbGQvXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2Jsb2Nrcy90YWJzL3B1YmxpYy5qc1wiKTtcbiIsImZ1bmN0aW9uIF9hcnJheVdpdGhvdXRIb2xlcyhhcnIpIHtcbiAgaWYgKEFycmF5LmlzQXJyYXkoYXJyKSkge1xuICAgIGZvciAodmFyIGkgPSAwLCBhcnIyID0gbmV3IEFycmF5KGFyci5sZW5ndGgpOyBpIDwgYXJyLmxlbmd0aDsgaSsrKSB7XG4gICAgICBhcnIyW2ldID0gYXJyW2ldO1xuICAgIH1cblxuICAgIHJldHVybiBhcnIyO1xuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2FycmF5V2l0aG91dEhvbGVzOyIsImZ1bmN0aW9uIF9pdGVyYWJsZVRvQXJyYXkoaXRlcikge1xuICBpZiAoU3ltYm9sLml0ZXJhdG9yIGluIE9iamVjdChpdGVyKSB8fCBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoaXRlcikgPT09IFwiW29iamVjdCBBcmd1bWVudHNdXCIpIHJldHVybiBBcnJheS5mcm9tKGl0ZXIpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9pdGVyYWJsZVRvQXJyYXk7IiwiZnVuY3Rpb24gX25vbkl0ZXJhYmxlU3ByZWFkKCkge1xuICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiSW52YWxpZCBhdHRlbXB0IHRvIHNwcmVhZCBub24taXRlcmFibGUgaW5zdGFuY2VcIik7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX25vbkl0ZXJhYmxlU3ByZWFkOyIsInZhciBhcnJheVdpdGhvdXRIb2xlcyA9IHJlcXVpcmUoXCIuL2FycmF5V2l0aG91dEhvbGVzXCIpO1xuXG52YXIgaXRlcmFibGVUb0FycmF5ID0gcmVxdWlyZShcIi4vaXRlcmFibGVUb0FycmF5XCIpO1xuXG52YXIgbm9uSXRlcmFibGVTcHJlYWQgPSByZXF1aXJlKFwiLi9ub25JdGVyYWJsZVNwcmVhZFwiKTtcblxuZnVuY3Rpb24gX3RvQ29uc3VtYWJsZUFycmF5KGFycikge1xuICByZXR1cm4gYXJyYXlXaXRob3V0SG9sZXMoYXJyKSB8fCBpdGVyYWJsZVRvQXJyYXkoYXJyKSB8fCBub25JdGVyYWJsZVNwcmVhZCgpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF90b0NvbnN1bWFibGVBcnJheTsiLCJkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCAoZXZlbnQpID0+IHtcbiAgICBjb25zdCB0YWJCbG9ja3MgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdnZWNrby10YWJzJyk7XG4gICAgWy4uLnRhYkJsb2Nrc10uZm9yRWFjaCgoYmxvY2ssIGluZGV4KSA9PiB7XG4gICAgICAgIC8vIENyZWF0ZSBuYXYgZWxlbWVudCBhbmQgaW5zZXJ0IGludG8gRE9NIGluIGdlY2tvLXRhYnMgZWxlbWVudFxuICAgICAgICBjb25zdCB0YWJOYXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibmF2XCIpO1xuICAgICAgICB0YWJOYXYuY2xhc3NOYW1lID0gXCJnZWNrby10YWJzX19uYXZcIjtcbiAgICAgICAgYmxvY2suaW5zZXJ0QmVmb3JlKHRhYk5hdiwgYmxvY2suY2hpbGROb2Rlc1swXSk7XG5cbiAgICAgICAgLy8gR2V0IGJsb2NrIGluZGV4XG4gICAgICAgIGNvbnN0IGJsb2NrSW5kZXggPSBpbmRleDtcbiAgICAgICAgY29uc3Qgc3R5bGVBY3RpdmUgPSAnYWN0aXZlJztcbiAgICAgICAgY29uc3Qgc3R5bGVJbmFjdGl2ZSA9ICdpbmFjdGl2ZSc7XG4gICAgICAgIGNvbnN0IHN0YXR1c0FjdGl2ZSA9ICd2aXNpYmxlJztcbiAgICAgICAgY29uc3Qgc3RhdHVzSW5hY3RpdmUgPSAnaGlkZGVuJztcblxuICAgICAgICAvLyBHZXQgY2hpbGQgcGFuZWxzXG4gICAgICAgIGNvbnN0IHBhbmVscyA9IGJsb2NrLnF1ZXJ5U2VsZWN0b3JBbGwoJy5nZWNrby10YWJzX19wYW5lbCcpO1xuICAgICAgICBbLi4ucGFuZWxzXS5mb3JFYWNoKChwYW5lbCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIC8vIEdldCBwYW5lbCBkYXRhXG4gICAgICAgICAgICBjb25zdCBhdHRyaWJ1dGVzID0gSlNPTi5wYXJzZShwYW5lbC5nZXRBdHRyaWJ1dGUoJ2RhdGEtcHJvcHMnKSk7XG4gICAgICAgICAgICAvLyBDcmVhdGUgdW5pcXVlIHBhbmVsSWQgdG8gdXNlIGFzIGRhdGEtdGFyZ2V0XG4gICAgICAgICAgICBjb25zdCBwYW5lbElkID0gXCJwYW5lbF9cIiArIGJsb2NrSW5kZXggKyBcIl9cIiArIGluZGV4O1xuICAgICAgICAgICAgLy8gU2V0IHBhbmVsSWQgYXMgZGF0YS10YXJnZXQgb24gcGFuZWxcbiAgICAgICAgICAgIHBhbmVsLnNldEF0dHJpYnV0ZSgnZGF0YS10YXJnZXQnLCBwYW5lbElkKTtcbiAgICAgICAgICAgIHBhbmVsLnNldEF0dHJpYnV0ZSgnZGF0YS1zdGF0dXMnLCAnaGlkZGVuJyk7XG5cbiAgICAgICAgICAgIC8vIGNyZWF0ZSBOYXYgQnV0dG9uOyBhZGQgY2xhc3NlcywgdGV4dCBhbmQgZGF0YS10YXJnZXQuIEluc2VydCB0aGUgbmF2IGJ1dHRvbiBpbnRvIHRoZSBuYXYgRE9NIGVsZW1lbnQuXG4gICAgICAgICAgICBjb25zdCBuYXZCdXR0b24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xuICAgICAgICAgICAgbmF2QnV0dG9uLmNsYXNzTGlzdCA9IFwiZ2Vja28tdGFic19fbmF2X2J1dHRvbiBnZWNrby10YWJzLS1idXR0b25cIjtcbiAgICAgICAgICAgIG5hdkJ1dHRvbi5zZXRBdHRyaWJ1dGUoJ2RhdGEtdGFyZ2V0JywgcGFuZWxJZCk7XG4gICAgICAgICAgICBuYXZCdXR0b24uc2V0QXR0cmlidXRlKCdkYXRhLXN0eWxlJywgc3R5bGVJbmFjdGl2ZSk7XG4gICAgICAgICAgICBjb25zdCBuYXZCdXR0b25UZXh0ID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoYXR0cmlidXRlcy5wYW5lbFRpdGxlKTtcbiAgICAgICAgICAgIG5hdkJ1dHRvbi5hcHBlbmRDaGlsZChuYXZCdXR0b25UZXh0KTtcbiAgICAgICAgICAgIHRhYk5hdi5hcHBlbmRDaGlsZChuYXZCdXR0b24pO1xuXG4gICAgICAgICAgICBpZiAoaW5kZXggPT09IDApe1xuICAgICAgICAgICAgICAgIHBhbmVsLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgIHBhbmVsLnNldEF0dHJpYnV0ZSgnZGF0YS1zdGF0dXMnLCBzdGF0dXNBY3RpdmUpO1xuICAgICAgICAgICAgICAgIG5hdkJ1dHRvbi5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICBuYXZCdXR0b24uc2V0QXR0cmlidXRlKCdkYXRhLXN0eWxlJywgc3R5bGVBY3RpdmUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBuYXZCdXR0b24uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIChlKSA9PiB7XG5cbiAgICAgICAgICAgICAgICBjb25zdCBjbGlja2VkQnV0dG9uID0gZS50YXJnZXQ7XG4gICAgICAgICAgICAgICAgY29uc3QgYnV0dG9uTm9kZUxpc3QgPSB0YWJOYXYuY2hpbGROb2RlcztcbiAgICAgICAgICAgICAgICBidXR0b25Ob2RlTGlzdC5mb3JFYWNoKGJ1dHRvbiA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvbi5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uLnNldEF0dHJpYnV0ZSgnZGF0YS1zdHlsZScsIHN0eWxlSW5hY3RpdmUpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGNsaWNrZWRCdXR0b24uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgY2xpY2tlZEJ1dHRvbi5zZXRBdHRyaWJ1dGUoJ2RhdGEtc3R5bGUnLCBzdHlsZUFjdGl2ZSk7XG5cbiAgICAgICAgICAgICAgICBjb25zdCBkYXRhVGFyZ2V0ID0gY2xpY2tlZEJ1dHRvbi5nZXRBdHRyaWJ1dGUoJ2RhdGEtdGFyZ2V0Jyk7XG4gICAgICAgICAgICAgICAgcGFuZWxzLmZvckVhY2gocGFuZWwgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIHBhbmVsLmdldEF0dHJpYnV0ZSgnZGF0YS1zdGF0dXMnKSA9PT0gc3RhdHVzQWN0aXZlICl7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYW5lbC5zZXRBdHRyaWJ1dGUoJ2RhdGEtc3RhdHVzJywgc3RhdHVzSW5hY3RpdmUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFuZWwuY2xhc3NMaXN0LnJlbW92ZShzdHlsZUFjdGl2ZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKCBwYW5lbC5nZXRBdHRyaWJ1dGUoJ2RhdGEtdGFyZ2V0JykgPT09IGRhdGFUYXJnZXQpe1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFuZWwuc2V0QXR0cmlidXRlKCdkYXRhLXN0YXR1cycsIHN0YXR1c0FjdGl2ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYW5lbC5jbGFzc0xpc3QuYWRkKHN0eWxlQWN0aXZlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xufSk7XG4iXSwic291cmNlUm9vdCI6IiJ9