<?php
/**
 * Grid Image Callback
 */
$render = function ( $attributes, $content ) {
	// Defaults and attributes
	// Setting to false unless needed because all styles do not need to be inlined
	$defaults = array(
		'w' => false,
		'h' => false,
		'newWindow' => false,
		'url' => false,
		'minHeight' => false,
		'className' => false,
		'backgroundColor' => false,
	);
	// Add a filter to hook into the default args
	$defaults = apply_filters( 'gecko/grid-layout-card/defaults', $defaults, $attributes );
	$atts = wp_parse_args( $attributes, $defaults );

	$classNames = array('wp-block-gecko-grid-layout__item');
	$classNames[] = 'gecko-grid-layout-card';
	if($atts['className']) $classNames[] = $atts['className'];
	// Add a filter to hook into classNames
	$classNames = apply_filters( 'gecko/grid-layout-card/class', $classNames, $attributes );

	$styles = array();
	$styles['--grid-column-end'] = ($atts['w'])?'span '.$atts['w']:false;
	$styles['--grid-row-end'] = ($atts['h'])?'span '.$atts['h']:false;
	$styles['-ms-grid-column-span'] = ($atts['w'])? $atts['w']:false;
	// $styles['-ms-grid-row-span'] = ($atts['h'])? $atts['h']:false;
	$styles['min-height'] = ($atts['minHeight'])?$atts['minHeight'].'px':false;
	$styles['background-color'] = ($atts['backgroundColor'])?$atts['backgroundColor']:false;
	// if($atts['alignContent']) $classNames[] = 'align-content-'.$atts['alignContent'];
	// Add a filter to hook into the inine styles $args = ($styles, $atts)
	$styles = apply_filters( 'gecko/grid-layout-card/style', $styles, $attributes );

	$styleString = '';
	foreach ($styles as $key => $value) {
		if($value) $styleString .= $key.':'.$value.';';
	}
	// Caption Template
	$output = sprintf('<div class="gecko-grid-layout-card__content">%s</div>', $content);
	$content = apply_filters( 'gecko/grid-layout-card/output/content', $output, $content, $attributes );
	// Output
	$style = '';
	foreach ($styles as $key => $value) {
		if($value) $style .= $key.':'.$value.';';
	}
	$class = implode(' ', $classNames);
	$template = '<div class="%s" style="%s">%s</div>';
	if($atts['url']){
		$template = '<a class="%s" href="'.$atts['url'].'" style="%s">%s</a>';
		if($atts['newWindow']){
			$template = '<a class="%s" href="'.$atts['url'].'" target="_blank" style="%s">%s</a>';
		}
	}
	$output = sprintf($template, $class, $style, $content);
	return apply_filters( 'gecko/grid-layout-card/output', $output, $class, $style, $content, $attributes );
};