<?php
/**
 * Grid Image Callback
 */
$render = function ( $attributes, $content ) {
	// Defaults and attributes
	// Setting to false unless needed because all styles do not need to be inlined
	$defaults = array(
		'w' => false,
		'h' => false,
		'imgId' => false,
		'imgUrl' => false,
		'minHeight' => false,
		'className' => false,
	);
	// Add a filter to hook into the default args
	$defaults = apply_filters( 'gecko/image/defaults', $defaults, $attributes );
	$atts = wp_parse_args( $attributes, $defaults );

	$classNames = array('wp-block-gecko-image');
	$classNames[] = 'gecko-image';
	if($atts['className']) $classNames[] = $atts['className'];
	// Add a filter to hook into classNames
	$classNames = apply_filters( 'gecko/image/class', $classNames, $attributes );

	$styles = array();
	$styles['--grid-column-end'] = ($atts['w'])?'span '.$atts['w']:false;
	$styles['--grid-row-end'] = ($atts['h'])?'span '.$atts['h']:false;
	$styles['-ms-grid-column-span'] = ($atts['w'])? $atts['w']:false;
	// $styles['-ms-grid-row-span'] = ($atts['h'])? $atts['h']:false;
	$styles['min-height'] = ($atts['minHeight'])?$atts['minHeight'].'px':false;
	// Add a filter to hook into the inine styles $args = ($styles, $atts)
	$styles = apply_filters( 'gecko/image/style', $styles, $attributes );

	$styleString = '';
	foreach ($styles as $key => $value) {
		if($value) $styleString .= $key.':'.$value.';';
	}
	// Image Template
	$preview = wp_get_attachment_image_url($atts['imgId'], 'lazy-load');
	// Check if lazy-load image size exists.
	if($preview === wp_get_attachment_image_url($atts['imgId'], 'full')){
		$preview = wp_get_attachment_image_url($atts['imgId'], 'thumbnail');
	}
	$src = wp_get_attachment_image_url($atts['imgId'], 'large');
	$srcset = wp_get_attachment_image_srcset( $atts['imgId'], array( 400, 200 ) );
	$alt = get_post_meta( $atts['imgId'], '_wp_attachment_image_alt', true );
	$title = get_the_title($atts['imgId']);
	$output = sprintf('<img class="gecko-image__image lazy" src="%s" data-src="%s" data-srcset="%s" alt="%s" title="%s"/>', $preview, $src, $srcset, $alt, $title);
	$image = apply_filters( 'gecko/image/output/image', $output, $preview, $src, $srcset, $alt, $title, $attributes);
	// Caption Template
	$output = sprintf('<figcaption class="gecko-image__caption">%s</figcaption>', $content);
	$caption = apply_filters( 'gecko/image/output/caption', $output, $content, $attributes );
	// Output
	$style = '';
	foreach ($styles as $key => $value) {
		if($value) $style .= $key.':'.$value.';';
	}
	$class = implode(' ', $classNames);
	$template = '<figure class="%s" style="%s">%s%s</figure>';
	$output = sprintf($template, $class, $style, $image, $caption);
	return apply_filters( 'gecko/image/output', $output, $class, $style, $image, $caption, $attributes );
};