const debug = process.env.NODE_ENV !== 'production';
const production = (process.env.NODE_ENV === 'production')? true : false;
const fs = require('fs')
const path = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const Fiber = require('fibers');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
/**
 * WordPress dependencies
 * This will import the external deps and replaces what we had before
 * https://www.npmjs.com/package/@wordpress/dependency-extraction-webpack-plugin
 * @babel/runtime/regenerator regeneratorRuntime wp-polyfill
 * @wordpress/* wp['*'] wp-*
 * jquery jQuery
 * lodash-es lodash
 * lodash lodash
 * moment moment
 * react-dom ReactDOM
 * react React
 */
const DependencyExtractionWebpackPlugin = require('@wordpress/dependency-extraction-webpack-plugin');


/**
 * Recurse through directories and get block entries.
 */
const blockDir = path.resolve(__dirname, 'src/blocks');
const entries = {};
const copyFiles = [];
fs.readdirSync(blockDir).map((item) => {
	const blockPath = path.resolve(blockDir, item);
	if (!fs.lstatSync(blockPath).isDirectory()) return;
	// Editor Scripts
	const editorFile = blockPath + '/index.js';
	if (fs.existsSync(editorFile)) {
		entries[`blocks/${item}/editor`] = editorFile;
	}
	// Public Scripts
	const publicFile = blockPath + '/public.js';
	if (fs.existsSync(publicFile)) {
		entries[`blocks/${item}/public`] = publicFile;
	}
	// Public Styles
	const publicStyle = blockPath + '/style.scss';
	if (fs.existsSync(publicStyle)) {
		entries[`blocks/${item}/style`] = publicStyle;
	}
	// Editor Styles
	const editorStyle = blockPath + '/editor.scss';
	if (fs.existsSync(editorStyle)) {
		entries[`blocks/${item}/style-editor`] = editorStyle;
	}
	// Block.php
	const blockPhp = blockPath + '/block.php';
	if (fs.existsSync(blockPhp)) {
		copyFiles.push(
			{
				from: blockPhp,
				to: `./blocks/${item}/block.php`
			}
		);
	}
	// Deps.json
	const depsJson = blockPath + '/deps.json';
	if (fs.existsSync(depsJson)) {
		copyFiles.push({
			from: depsJson,
			to: `./blocks/${item}/deps.json`
		});
	}
});
const scriptsDir = path.resolve(__dirname, 'src/scripts');
fs.readdirSync(scriptsDir).map((item) => {
	const scriptsPath = path.resolve(scriptsDir, item);
	if (!fs.lstatSync(scriptsPath).isDirectory()) return;
	// Editor Scripts
	const file = scriptsPath + '/index.js';
	if (fs.existsSync(file)) {
		entries[`scripts/${item}`] = file;
	}
});
// console.log(entries);
// console.log(copyFiles);

/**
 * Webpack settings
 */
const settings = {
	context: __dirname,
	devtool: debug ? 'inline-sourcemap' : false,
	mode: debug ? 'development' : 'production',
	target: 'web',
	entry: entries,
	resolve: {
		extensions: ['.scss', '.js', '.jsx'],
	},
	output: {
		path: path.resolve(__dirname, 'build'),
		publicPath: `/wp-content/plugins/${path.basename(__dirname)}/build/`,
		filename: `[name].js`,
	},
	module: {
		rules:[
			{
				test: /\.(css|scss)$/,
				exclude: /node_modules/,
				loader: MiniCssExtractPlugin.loader,
			},
			{
				test: /\.(css|scss)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'css-loader',
						options: {
							sourceMap: (!production) ? true : false,
							url: true,
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							plugins: () => [require('autoprefixer')],
						}
					},
					{
						loader: 'sass-loader',
						options: {
							implementation: require('sass'),
							sassOptions: {
								sourceMap: (!production) ? true : false,
								fiber: Fiber,
							}
						},
					}
				],
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'images/[name].[ext]',
							emitFile: true,
						}
					}
				]
			},
			// Babel loader for es6 support
			{
				test: /\.(js|jsx)?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: [
						'@wordpress/default',
						'@babel/preset-env',
						'@babel/preset-react',
					],
					plugins: [
						'@wordpress/babel-plugin-import-jsx-pragma',
						'@babel/transform-react-jsx',
						'@babel/plugin-transform-runtime',
						'@babel/plugin-syntax-dynamic-import',
						'@babel/plugin-proposal-object-rest-spread',
						'@babel/plugin-proposal-class-properties',
						'babel-plugin-styled-components',
					],
				},
			},
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css',
		}),
		new CopyWebpackPlugin(copyFiles),
		new DependencyExtractionWebpackPlugin({
			injectPolyfill: true
		}),
	],
};

if(production){
	settings.plugins.push(new BundleAnalyzerPlugin());
	settings.optimization = {
		splitChunks: {
			cacheGroups: {
				'react-select': {
					test: /[\\/]node_modules[\\/](react-select)[\\/]/,
					name: 'react-select',
					chunks: 'all',
				},
			},
		},
		minimizer: [
			new TerserPlugin({
				parallel: true,
				extractComments: true,
				terserOptions: {
					mangle: true,
				}
			}),
			new OptimizeCSSAssetsPlugin({})
		],
	};
}

module.exports = settings;