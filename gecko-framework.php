<?php
/**
 * Plugin Name: Gecko Framework
 * Description: Adds all kinds of goodies for gutenberg and theme.
 * Version: 0.0.6
 * Author: Gecko Designs
 * Author URI: https://geckodesigns.com
 * Text Domain: gecko-designs
 * License: GPLv2 or later
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
define( 'GECKO_FRAMEWORK__FILE', __FILE__);
define( 'GECKO_FRAMEWORK__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'GECKO_FRAMEWORK__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once __DIR__ . '/vendor/autoload.php';

/*
 * Class AutoLoader
 * This is the one of the most important features of gecko command
 * It creates the naming convention for gecko plugins
 */
spl_autoload_register(function( $class) {
	$namespaces = explode('\\', $class);
	// Registering Additional Plugins Schema
	if(in_array('GeckoFramework', $namespaces)){
		foreach ($namespaces as $namespace) {
			$file = $namespace;
		}
		array_shift($namespaces);
		$index = count( $namespaces ) - 1;
		$value = $namespaces[$index];
		$namespaces[$index] = 'class-'.$value.'.php';
		$path = strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', implode($namespaces,'/')));
		// $class_file = GECKO_FRAMEWORK__PLUGIN_DIR . 'includes/class-'. strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $file))  . '.php';
		$class_file = GECKO_FRAMEWORK__PLUGIN_DIR . 'includes/'.$path;
		if(file_exists($class_file)){
			require_once $class_file;
		}
	}
});

/*
 * Initiate
 */
function gecko_framework(){return;}
add_action( 'plugins_loaded', function(){
	new GeckoFramework\Init();
} );
