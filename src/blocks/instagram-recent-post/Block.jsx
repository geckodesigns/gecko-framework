import React from 'react';
import apiFetch from '@wordpress/api-fetch';

// Sets a global variable so that you don't load the same post when multiple blocks are present. It will instead load the next in line.
window.geckoInstagramRecentPost = (window.geckoInstagramRecentPost) ? window.geckoInstagramRecentPost : 0;
const addBodyClass = () => document.body.classList.add('asset-modal-open');
const removeBodyClass = () => document.body.classList.remove('asset-modal-open');

export default class Gram extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			gram: null,
		}
	}


	close = (e) => {
		console.log('close');
		// removeBodyClass();
		if(this.state.content){
			removeBodyClass();
		}
		this.setState({
			active: false,
			content: false,
		});
	}


	componentDidMount() {
		// this.isVisible(this.ref, this.get);
		// document.addEventListener('click', this.handleOutsideClick, false);
	}

	componentWillUnmount() {
		// this.isVisible(this.ref, null, true);
		// document.removeEventListener('click', this.handleOutsideClick, false);
	}

	// Right now unselect happen on outside click. I think there is a better way to do this.
	handleOutsideClick = (e) => {
		// if (this.ref.current && !this.ref.current.contains(e.target)) {
		// 	this.close(e);
		// }
	}

	get = () => {
		apiFetch({
			path: `/gecko/v1/instagram`
		}).then(data => {
			const key = window.geckoInstagramRecentPost;
			this.setState({
				gram: data[key],
			});
			window.geckoInstagramRecentPost++;
		}).catch(error => {
			console.error(error);
		});
	}

	componentWillMount(){
		this.get();
	}

	render() {
		const { gram } = this.state;
		if(!gram) return null;
		return(
			<figure className={`gecko-instagram-recent-post`}>
				<div className={`gecko-instagram-recent-post__image`}>
					<img src={(gram.media_type === 'VIDEO') ? gram.thumbnail_url : gram.media_url} />
				</div>
				<figcaption className={`gecko-instagram-recent-post__caption`} dangerouslySetInnerHTML={{ __html: gram.caption }} />
			</figure>
		);
	}
}
