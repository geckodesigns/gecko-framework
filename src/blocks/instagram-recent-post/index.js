/**
 * WordPress dependencies
 */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { __ } from '@wordpress/i18n';
import {
	registerBlockType
} from '@wordpress/blocks';
import {
	PanelBody,
	SelectControl,
} from '@wordpress/components';
import {
	InnerBlocks,
	InspectorControls,
} from '@wordpress/editor';
import Block from './Block';

/**
 * Allowed blocks constant is passed to InnerBlocks precisely as specified here.
 * The contents of the array should never change.
 * The array should contain the name of each block that is allowed.
 * In columns block, the only block we allow is 'dmp/grid-item'.
 *
 * @constant
 * @type {string[]}
*/
const name = 'gecko/instagram-recent-post';
const settings = {
	title: __( 'Instagram Recent Post' ),
	icon: <FontAwesomeIcon icon={faInstagram} />,
	category: 'common',
	description: __( 'Will show the most recent cached post. If two blocks are added it will show the two most recent posts and so on.' ),
	supports: {
		html: false,
	},
	deprecated: [],
	attributes: {},

	edit: ({attributes, setAttributes, insertBlocksAfter, className}) => {
		// const {} = attributes;
		return ([
			<InspectorControls>
				<PanelBody title={__("Settings")}>

				</PanelBody>
			</InspectorControls>,
			<Block/>
		]);
	},

	save: () => {
		return null;
	}
};

registerBlockType(name, settings);