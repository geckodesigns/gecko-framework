<?php
/**
	* If the block is dynamic you would render the template here.
	*/
$render = function ( $attributes, $content ) {
	ob_start();
	if(get_search_query()):
			if (have_posts()):
			echo '<h2>Search Results for: </h2>';
			get_search_form();
			echo '<hr />';
			echo '<div class="gecko-page__loop">';
				$i = 0;
				while (have_posts()): the_post();
					?>
					<article>
						<header class="gecko-loop-single__header">
							<a href="<?php the_permalink();?>">
								<?php the_title('<h2>','</h2>'); ?>
							</a>
						</header>
						<main>
							<?php the_excerpt(); ?>
						</main>
					</article>
					<?php
				endwhile; 
			echo '</div>';
			echo '<div class="gecko-pagination">';
			echo paginate_links(array('prev_text' => 'Previous','next_text' => 'Next'));
			echo '</div>';
			else:
				echo '<h2>Your search did yeild any results:</h2>';
				get_search_form();
				echo '<hr />';
				echo '<p>Suggestions:</p>';
				echo '<ul>
					<li>Make sure all words are spelled correctly.</li>
					<li>Try different keywords.</li>
					<li>Try more general keywords.</li>
				</ul>';
			endif;
		else:
			get_search_form();
			echo '<hr />';
			the_post();
			the_content(); 
	endif;
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
};