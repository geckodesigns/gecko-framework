<?php
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS|JSON_HEX_QUOT);

	/* Accordion Classes */
	$accordionClasses = ["gecko-accordion"];

	if ( $attributes['className']){
		$accordionClasses[] = $attributes['className'];
	}

	if ( $attributes["accordionTitle"] ){
		$title = $attributes["accordionTitle"];
	} else {
		$title = 'No Title Entered';
		$accordionClasses[] = "gecko-accordion--missing-title";
	}

	$accordionClasses = implode(' ', $accordionClasses);
	/* End Accordion Classes */

	/* Closed Icon Classes */
	$closed_icon_classes = [];
	if (isset($attributes['closedIconPrefix'])) {
		$closed_icon_classes[] = $attributes['closedIconPrefix'];
	} else {
		$closed_icon_classes[] = "fas";
	}

	if (isset($attributes['closedIconName'])) {
		$closed_icon_classes[] = "fa-" . $attributes['closedIconName'];
	} else {
		$closed_icon_classes[] = "fa-plus";
	}
	/* End Closed Icon Classes */

	/* Open Icon Classes */
	$open_icon_classes = [];
	if (isset($attributes['openIconPrefix'])) {
		$open_icon_classes[] = $attributes['openIconPrefix'];
	} else {
		$open_icon_classes[] = "fas";
	}

	if (isset($attributes['openIconName'])) {
		$open_icon_classes[] = "fa-" . $attributes['openIconName'];
	} else {
		$open_icon_classes[] = "fa-minus";
	}
	/* End Open Icon Classes */

	/* Closed Image Icon Src */
	$closed_image_src = '';
	if ($attributes['closeImageUrl']){
		$closed_image_src = $attributes['closeImageUrl'];
	}

	/* Open Image Icon Src */
	$open_image_src = '';
	if ($attributes['openImageUrl']){
		$open_image_src = $attributes['openImageUrl'];
	}

	/* Shared Icon Classes */
	if (isset($attributes['iconSize'])) {
		$closed_icon_classes[] = "fa-" . $attributes['iconSize'];
		$open_icon_classes[] = "fa-" . $attributes['iconSize'];
	}

	$closed_icon_classes = implode(' ', $closed_icon_classes);
	$open_icon_classes = implode(' ', $open_icon_classes);

	$icon_color_style = '';
	if (isset($attributes['iconColorValue']) && !empty($attributes['iconColorValue'])) {
		$icon_color_style = 'color: ' . $attributes['iconColorValue'] . ';';
	}

	$start_status = '';
	/* if (isset($attributes['startOpen']) && !empty($attributes['startOpen']) && $attributes['startOpen'] == true){
		$start_status = 'open';
	} */

	$markup = '<details class="%s" data-props="%s" %s>';

	$markup .= '<summary class="gecko-accordion__title">';
	$markup .= '<section class="gecko-accordion__title__wrapper">';

	if ( $attributes['hasIndicator'] === 'icon' ){
		$markup .= '<div class="gecko-accordion__title__icon-wrapper gecko-accordion__title__icon-wrapper--closed">';
		$markup .= sprintf('<i class="%s" style="'.$icon_color_style.'"></i>', $closed_icon_classes);
		$markup .= '</div>';
		$markup .= '<div class="gecko-accordion__title__icon-wrapper gecko-accordion__title__icon-wrapper--open">';
		$markup .= sprintf('<i class="%s" style="'.$icon_color_style.'"></i>', $open_icon_classes);
		$markup .= '</div>';
	}

	if ( $attributes['hasIndicator'] === 'image'){
		$markup .= '<div class="gecko-accordion__title__image-wrapper gecko-accordion__title__image-wrapper--closed">';
		$markup .= sprintf('<img src="%s" >', $closed_image_src);
		$markup .= '</div>';
		$markup .= '<div class="gecko-accordion__title__image-wrapper gecko-accordion__title__image-wrapper--open">';
		$markup .= sprintf('<img src="%s" >', $open_image_src);
		$markup .= '</div>';
	}
	$markup .= '%s</section></summary>';

	$markup .= '<section class="gecko-accordion__content-wrapper">%s</section>';

	$markup .= '</details>';

	return sprintf(
		$markup,
		$accordionClasses,
		htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'),
		$start_status,
		$title,
		$content
	);
};