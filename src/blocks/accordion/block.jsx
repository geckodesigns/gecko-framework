import React from 'react';
import classnames from 'classnames';
import { TextControl } from '@wordpress/components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './style.scss';

export default class Block extends React.PureComponent {
	render() {
		const {
			accordionTitle,
			hasIndicator,
			closedIconPrefix,
			closedIconName,
			openIconPrefix,
			openIconName,
			iconColor,
			iconColorValue,
			iconSize,
			setAttributes,
			openImageUrl,
			closeImageUrl,
		} = this.props;

		const closedPrefix = (closedIconPrefix) ? closedIconPrefix : 'fas';
		const closedName = (closedIconName) ? closedIconName : 'plus';

		const openPrefix = (openIconPrefix) ? openIconPrefix : 'fas';
		const openName = (closedIconName) ? openIconName : 'minus';

		const size = (iconSize) ? iconSize : undefined;

		const classNames = classnames(
			'gecko-accordion',
			{[`${this.props.className}`]: (this.props.className)}
		);

		return (
			<div className={classNames}>
				<section className="gecko-accordion__title">
					{ hasIndicator && hasIndicator === "icon" &&
					<>
						<div className="gecko-accordion__title__icon-wrapper gecko-accordion__title__icon-wrapper--closed">
							{/* <p className="gecko-accordion__title__icon-wrapper--label gecko-accordion__title__icon-wrapper--closed--label">Closed Icon</p> */}
							<FontAwesomeIcon icon={[closedPrefix, closedName]} size={size} color={iconColorValue} />
						</div>
						<div className="gecko-accordion__title__icon-wrapper gecko-accordion__title__icon-wrapper--open">
						{/* <p className="gecko-accordion__title__icon-wrapper--label gecko-accordion__title__icon-wrapper--open--label">Open Icon</p> */}
							<FontAwesomeIcon icon={[openPrefix, openName]} size={size} color={iconColorValue} />
						</div>
					</>
					}
					{ hasIndicator && hasIndicator === "image" &&
					<>
						<div className="gecko-accordion__title__image-wrapper gecko-accordion__title__image-wrapper--closed">
						{/* <p className="gecko-accordion__title__image-wrapper--label gecko-accordion__title__image-wrapper--closed--label">Closed Icon</p> */}
							<img src={closeImageUrl} />
						</div>
						<div className="gecko-accordion__title__image-wrapper gecko-accordion__title__image-wrapper--open">
						{/* <p className="gecko-accordion__title__image-wrapper--label gecko-accordion__title__image-wrapper--open--label">Open Icon</p> */}
							<img src={openImageUrl} />
						</div>
					</>
					}
					{<TextControl
						label="Accordion Title"
						value={accordionTitle}
						onChange={accordionTitle => { setAttributes({accordionTitle}) }}
					/>}
				</section>

				<section className="gecko-accordion__content-wrapper">
				<h5>Accordion Content</h5>
					{this.props.children}
				</section>
			</div>
		);
	}
}