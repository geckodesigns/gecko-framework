import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
// import { far } from '@fortawesome/free-regular-svg-icons';

library.add(fab);
library.add(fas);
// library.add(far);

import Block from './block';
import IconPicker from './components/IconPicker';

import { __ } from '@wordpress/i18n';
import { registerBlockType } from '@wordpress/blocks';

import {
	withColors,
	BlockControls,
	InspectorControls,
	InnerBlocks,
	PanelColorSettings,
	MediaUpload,
	MediaUploadCheck,
	URLPopover,
	URLInput,
} from '@wordpress/editor';

import {
	PanelBody,
	PanelRow,
	Button,
	IconButton,
	Toolbar,
	SelectControl,
	ToggleControl,
} from '@wordpress/components';

const settings = {
	title: 'Accordion',
	icon: <FontAwesomeIcon icon={['fas', 'chevron-down']} />,
	category: 'common',
	description: 'Simple Accordion element',
	attributes: {
		accordionTitle: {
			type: 'string',
		},
		hasIndicator: {
			type: 'string',
			default: 'no',
		},
		startOpen: {
			type: 'boolean',
			default: false,
		},
		closedIconPrefix: {
			type: 'string',
			prefix: 'fas',
		},
		closedIconName: {
			type: 'string',
			default: 'plus',
		},
		openIconPrefix: {
			type: 'string',
			prefix: 'fas',
		},
		openIconName: {
			type: 'string',
			default: 'minus',
		},
		iconSize: {
			type: 'string',
			default: '',
		},
		iconPosition: {
			type: 'string',
			default: 'before',
		},
		iconHorizontalAlign: {
			type: 'string',
			default: 'left',
		},
		iconVerticalAlign: {
			type: 'string',
			default: 'top',
		},
		enableIconLink: {
			type: 'boolean',
			default: false,
		},
		closeImageUrl: {
			type: 'string',
		},
		openImageUrl: {
			type: 'string',
		}
	},

	edit: withColors('iconColor')(props => {
		const {
			accordionTitle,
			hasIndicator,
			closedIconPrefix,
			closedIconName,
			openIconPrefix,
			openIconName,
			iconColor,
			iconColorValue,
			iconSize,
			openImageUrl,
			closeImageUrl,
			startOpen,
		} = props.attributes;

		let allowedBlocks = [
			'core/paragraph',
			'core/heading',
			'core/button',
			'core/image',
			'core/list',
			'core/quote',
			'core/cover',
			'core/columns',
			'gecko/section',
			'gecko/grid-layout',
		];

		const updateClosedIcon = (icon) => {
			props.setAttributes({
				closedIconPrefix: icon.prefix,
				closedIconName: icon.iconName,
			});
		}

		const updateOpenIcon = (icon) => {
			props.setAttributes({
				openIconPrefix: icon.prefix,
				openIconName: icon.iconName,
			});
		}

		/* const updateOpenImage = (image) => {
			props.setAttributes({
				openImageUrl: image.sizes.full.url
			})
		}
		const updateCloseImage = (image) => {
			props.setAttributes({
				closeImageUrl: image.sizes.full.url
			})
		} */

		let colorSettings = [];
		colorSettings.push({
			label: 'Icon Color',
			value: props.iconColor.color,
			onChange: (newIconColor) => {
				props.setAttributes({ iconColorValue: newIconColor });
				props.setIconColor(newIconColor);
			}
		});
		return ([
			<InspectorControls>
				<PanelBody title="Accordion Indicator">
					<SelectControl
						label={__('Show Icon, Image or neither')}
						value={ hasIndicator }
						options={[
							{ value: 'icon', label: 'Icon'},
							{ value: 'image', label: 'Image'},
							{ value: 'no', label: 'No Indicator'}
						]}
						onChange={hasIndicator => { props.setAttributes({ hasIndicator }); }}
						help={(hasIndicator === 'no') ? __('Include two icons or two images next to the title. One when the accordion is open and one when it is closed.') : undefined}
					/>

					{ hasIndicator && hasIndicator === 'icon' &&
						<>
							<p>Closed Accordion Icon</p>
							<IconPicker
								iconPrefix={closedIconPrefix}
								iconName={closedIconName}
								updateSelectedIcon={updateClosedIcon}
							/>
							<br />

							<p>Open Accordion Icon</p>
							<IconPicker
								iconPrefix={openIconPrefix}
								iconName={openIconName}
								updateSelectedIcon={updateOpenIcon}
							/>
							<br />

							<SelectControl
								label={__('Icon Size')}
								value={iconSize}
								onChange={iconSize => props.setAttributes({ iconSize })}
								options={[
									{ label: 'Extra Small', value: 'xs' },
									{ label: 'Small', value: 'sm' },
									{ label: 'Normal', value: '' },
									{ label: 'Large', value: 'lg' },
									{ label: '2x', value: '2x' },
									{ label: '3x', value: '3x' },
									{ label: '4x', value: '4x' },
									{ label: '5x', value: '5x' },
									{ label: '6x', value: '6x' },
									{ label: '7x', value: '7x' },
									{ label: '8x', value: '8x' },
									{ label: '9x', value: '9x' },
									{ label: '10x', value: '10x' },
								]}
							/>
						</>
					}

					{ hasIndicator && hasIndicator === 'image' &&
						<>
							<p>Closed Accordion Image</p>
							<MediaUploadCheck>
								<MediaUpload
									onSelect={image => props.setAttributes({closeImageUrl: image.sizes.full.url}) }
									allowedTypes={["image"]}
									value={closeImageUrl}
									render={ ( { open } ) => (
										<Button onClick={ open }>
											{(props.attributes.closeImageUrl)? <img src={props.attributes.closeImageUrl} /> : 'Open Media Library'}
										</Button>
									)}
								/>
							</MediaUploadCheck>
							<br />
							<p>Open Accordion Image</p>
							<MediaUploadCheck>
								<MediaUpload
									onSelect={image => props.setAttributes({openImageUrl: image.sizes.full.url}) }
									allowedTypes={["image"]}
									value={openImageUrl}
									render={ ( { open } ) => (
										<Button onClick={ open }>
											{(props.attributes.openImageUrl)? <img src={props.attributes.openImageUrl} /> : 'Open Media Library'}
										</Button>
									)}
								/>
							</MediaUploadCheck>
						</>
					}
				</PanelBody>
				<PanelBody title="Accordion Status">
					<ToggleControl
						label="Default Open/Close Status"
						help={startOpen ? 'Will Be Open By Default' : 'Will Be Closed By Default'}
						checked={ startOpen }
						onChange={ ( startOpen ) => props.setAttributes({ startOpen })}
					/>
				</PanelBody>
				{
					hasIndicator && hasIndicator === 'icon' &&
					<PanelColorSettings
						title="Icon Color"
						colorSettings={colorSettings}>
					</PanelColorSettings>
				}
			</InspectorControls>,

			<Block {...props.attributes} setAttributes={props.setAttributes} >
				<InnerBlocks />
			</Block>
		]);
	}),

	save(props) {
		return (
			<div><InnerBlocks.Content /></div>
		);
	}
}

registerBlockType('gecko/accordion', settings);
{/* <TextControl
						label="Accordion Title"
						value={accordionTitle}
						onChange={accordionTitle => { props.setAttributes({accordionTitle}) }}
					/> */}