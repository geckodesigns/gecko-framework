<?php
/**
	* If the block is dynamic you would render the template here.
	*/
$render = function ( $attributes, $content ) {
	// Custom Logo Support
	add_action( 'after_setup_theme', function () {
		$defaults = array(
		'height'      => 100,
		'width'       => 400,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
		);
		add_theme_support( 'custom-logo', $defaults );
	});

	if ( function_exists( 'the_custom_logo' ) ) {
		return get_custom_logo();
	}
};