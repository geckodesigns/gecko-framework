/**
 * WordPress dependencies
 */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { __ } from '@wordpress/i18n';
import {
	registerBlockType
} from '@wordpress/blocks';
import {
	PanelBody,
	SelectControl,
} from '@wordpress/components';
import {
	InnerBlocks,
	InspectorControls,
} from '@wordpress/editor';

/**
 * Allowed blocks constant is passed to InnerBlocks precisely as specified here.
 * The contents of the array should never change.
 * The array should contain the name of each block that is allowed.
 * In columns block, the only block we allow is 'dmp/grid-item'.
 *
 * @constant
 * @type {string[]}
*/
const name = 'gecko/logo';
const settings = {
	title: __( 'Logo' ),
	icon: <FontAwesomeIcon icon={faSignInAlt} />,
	category: 'layout',
	description: __( 'Logo.' ),
	supports: {
		html: false,
	},
	deprecated: [],
	attributes: {
		role: { type: 'string'},
	},

	edit: ({attributes, setAttributes, insertBlocksAfter, className}) => {
		const {size, background, minHeight, align, contrast} = attributes;
		const style = {
			background: background,
			minHeight: minHeight,
		}
		return ([
			<InspectorControls>
				<PanelBody title={__("Settings")}>

				</PanelBody>
			</InspectorControls>,
			<div className="login">
				<p>Custom Logo</p>
			</div>
		]);
	},

	save: () => {
		return null
	}
};

registerBlockType(name, settings);