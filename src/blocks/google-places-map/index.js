/**
 * WordPress dependencies
 */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
	faMap,
} from '@fortawesome/free-solid-svg-icons';

import { __ } from '@wordpress/i18n';
import {
	registerBlockType
} from '@wordpress/blocks';
import {
	PanelBody,
	SelectControl,
} from '@wordpress/components';
import {
	InnerBlocks,
	InspectorControls,
} from '@wordpress/editor';
import Block from './Block';

/**
 * Allowed blocks constant is passed to InnerBlocks precisely as specified here.
 * The contents of the array should never change.
 * The array should contain the name of each block that is allowed.
 * In columns block, the only block we allow is 'dmp/grid-item'.
 *
 * @constant
 * @type {string[]}
*/
const name = 'gecko/google-places-map';
const settings = {
	title: __( 'Google Places Map' ),
	icon: <FontAwesomeIcon icon={faMap} />,
	category: 'common',
	description: __( 'Adds map for google places.' ),
	supports: {
		html: false,
	},
	deprecated: [],
	attributes: {},

	edit: ({attributes, setAttributes, insertBlocksAfter, className}) => {
		// const {} = attributes;
		return ([
			<InspectorControls>
				<PanelBody title={__("Settings")}>

				</PanelBody>
			</InspectorControls>,
			<Block/>
		]);
	},

	save: () => {
		return null;
	}
};

registerBlockType(name, settings);