import React from 'react';
import apiFetch from '@wordpress/api-fetch';

// Sets a global variable so that you don't load the same post when multiple blocks are present. It will instead load the next in line.
const addBodyClass = () => document.body.classList.add('asset-modal-open');
const removeBodyClass = () => document.body.classList.remove('asset-modal-open');

export default class Gram extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			place: null,
		}
	}


	close = (e) => {
		console.log('close');
		// removeBodyClass();
		if(this.state.content){
			removeBodyClass();
		}
		this.setState({
			active: false,
			content: false,
		});
	}


	componentDidMount() {
		// this.isVisible(this.ref, this.get);
		// document.addEventListener('click', this.handleOutsideClick, false);
	}

	componentWillUnmount() {
		// this.isVisible(this.ref, null, true);
		// document.removeEventListener('click', this.handleOutsideClick, false);
	}

	// Right now unselect happen on outside click. I think there is a better way to do this.
	handleOutsideClick = (e) => {
		// if (this.ref.current && !this.ref.current.contains(e.target)) {
		// 	this.close(e);
		// }
	}

	get = () => {
		const {place} = this.props;
		apiFetch({
			path: `/gecko/v1/google/places/${place}`
		}).then(data => {
			this.setState({
				place: data,
			});
		}).catch(error => {
			console.error(error);
		});
	}

	componentWillMount(){
		// this.get();
	}

	render() {
		console.log(this.state);
		const class_name = 'gecko-google-reviews-widget';
		return(
			<div>Gecko Google Reviews Badge</div>
		);
	}
}
