import ReactDom from 'react-dom';
import Block from './Block';
document.addEventListener("DOMContentLoaded", (event) => {
	const elements = document.getElementsByClassName('gecko-instagram-recent-post');
	[...elements].forEach(element => {
		const attributes = JSON.parse(element.getAttribute('data-props'));
		ReactDom.render(<Block {...attributes} />, element);
	});
});