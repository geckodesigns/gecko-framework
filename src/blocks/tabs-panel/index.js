import { Fragment } from 'react';
import classnames from 'classnames';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faExchangeAlt,
	faSquare,
} from '@fortawesome/free-solid-svg-icons';

import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';

import {
	withColors,
	InspectorControls,
	InnerBlocks,
	BlockControls,
	PanelColorSettings,
	MediaUpload,
} from '@wordpress/editor';

import { withState } from '@wordpress/compose'

import {
	createBlock,
} from '@wordpress/blocks';

import {
	PanelBody,
	Toolbar,
	TextControl,
} from '@wordpress/components';

const settings = {
	title: 'Tabs Panel',
	icon: <FontAwesomeIcon icon={faSquare} /> ,
	category: 'layout',
	description: 'Tabs Panel',

	parent: ['gecko/tabs'],

	attributes: {
		panelTitle: {
			type: 'string',
			default: 'Panel Title'
		},
	},

	edit: (props) => {
		const {
			className,
		} = props;

		const {
			panelTitle
		} = props.attributes;


		const classNames = classnames(
			className,
		);

		return ([
			<InspectorControls>
				<PanelBody title="Panel Title">
					<TextControl
						label="Panel Title"
						help="Used in the navigation tab."
						value={(panelTitle)? panelTitle : "Panel Title"}
						onChange={ ( value ) => props.setAttributes({ panelTitle: value }) }
					/>
				</PanelBody>
			</InspectorControls>,

			<div className={classNames}>
				<InnerBlocks />
			</div>
		]);
	},

	save(props) {
		return(<div><InnerBlocks.Content /></div>);
	}
}

registerBlockType('gecko/tabs-panel', settings);