import { Component } from 'react';
import { FixedSizeList as List } from 'react-window';

import {
	Popover
} from '@wordpress/components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

class IconPicker extends Component {
	constructor(props) {
		super(props);

		const iconData = this.updateIconData('');

		this.state = {
			popoverVisible: false,
			searchTerm: '',
			iconData,
		};
	}

	updateIconData = (searchTerm) => {
		const solidIconData = Object.keys(fas).map(key => {
			if (fas[key]) {
				return fas[key];
			}
		});

		const brandsIconData = Object.keys(fab).map(key => {
			if (fab[key]) {
				return fab[key];
			}
		});

		const mergedIconData = solidIconData.concat(brandsIconData);

		return mergedIconData.filter(el => (searchTerm === '' || el.iconName.includes(searchTerm)));
	}

	resetIconData = () => {
		const resetIcons = this.updateIconData('');
		this.setState({
			popoverVisible: false,
			searchTerm: '',
			iconData: resetIcons,
		});
	}

	renderList = () => {
		const rowCount = this.state.iconData.length;

		return (
			<List
				width={260}
				height={400}
				itemSize={50}
				itemCount={rowCount}
			>
				{this.renderListItem}
			</List>
		);
	}

	renderListItem = ({index, style}) => {
		const {
			iconData,
		} = this.state;

		const isSelected = (iconData[index].iconName === this.props.iconName) ? 'true' : undefined;

		return (
			<div
				style={style}
				className="gecko-icon-box__list-item"
				data-is-selected={isSelected}
				onClick={e => {
					this.props.updateSelectedIcon(iconData[index]);
					this.resetIconData();
				}}
			>
				{iconData[index] && iconData[index].icon &&
					<>
						<FontAwesomeIcon
							className="gecko-icon-box__list-item-icon"
							icon={iconData[index]}
						/>
						<div className="gecko-icon-box__list-item-name">{iconData[index].iconName}</div>
					</>
				}
			</div>
		)
	}

	render() {
		const {
			popoverVisible,
			searchTerm,
		} = this.state;

		const {
			iconPrefix,
			iconName,
		} = this.props;

		const prefix = (iconPrefix) ? iconPrefix : 'fab';
		const name = (iconName) ? iconName : 'wordpress';

		return (
			<span>
				<span
					className="gecko-icon-box__choose-button"
					onClick={e => this.setState({ popoverVisible: !popoverVisible })}
				>
					<FontAwesomeIcon icon={[prefix, name]} />
				</span>

				{popoverVisible &&
					<Popover
						position="center"
						onClose={event => {
							this.resetIconData();
						}}
					>
						<input
							type="search"
							className="gecko-icon-box__search-input"
							placeholder="Search icons..."
							value={searchTerm}
							onChange={event => {
								const searchTerm = event.target.value;
								const iconData = this.updateIconData(searchTerm);
								this.setState({
									searchTerm,
									iconData
								});
							}}
						/>

						{this.renderList()}
					</Popover>
				}
			</span>
		);
	}
}

export default IconPicker;