<?php
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS|JSON_HEX_QUOT);

	// Container Classes
	$classes = ["gecko-icon-box"];
	if (isset($attributes['iconPosition'])) {
		$classes[] = "gecko-icon-box--position-" . $attributes['iconPosition'];
	} else {
		$classes[] = "gecko-icon-box--position-before";
	}

	if (isset($attributes['iconVerticalAlign'])) {
		$classes[] = "gecko-icon-box--vertical-align-" . $attributes['iconVerticalAlign'];
	} else {
		$classes[] = "gecko-icon-box--vertical-align-top";
	}

	if (isset($attributes['iconHorizontalAlign'])) {
		$classes[] = "gecko-icon-box--horizontal-align-" . $attributes['iconHorizontalAlign'];
	} else {
		$classes[] = "gecko-icon-box--horizontal-align-left";
	}

	if (isset($attributes['iconFullWidth']) && $attributes['iconFullWidth']) {
		$classes[] = 'gecko-icon-box--full-width';
	}

	$classes = implode(' ', $classes);

	// Icon Classes
	$icon_classes = [];
	if (isset($attributes['iconPrefix'])) {
		$icon_classes[] = $attributes['iconPrefix'];
	} else {
		$icon_classes[] = "fab";
	}

	if (isset($attributes['iconName'])) {
		$icon_classes[] = "fa-" . $attributes['iconName'];
	} else {
		$icon_classes[] = "fa-wordpress";
	}

	if (isset($attributes['iconSize'])) {
		$icon_classes[] = "fa-" . $attributes['iconSize'];
	}
	$icon_classes = implode(' ', $icon_classes);

	$is_link = (isset($attributes['enableIconLink']) && $attributes['enableIconLink']);

	$markup = '<div class="%s" data-props="%s">';

		if ($is_link) {
			$url = (isset($attributes['iconLinkUrl']) && $attributes['iconLinkUrl'] != "") ? $attributes['iconLinkUrl'] : '#';
			$target = (isset($attributes['iconLinkNewWindow']) && $attributes['iconLinkNewWindow']) ? '_blank' : '';
			$markup .= '<a class="gecko-icon-box__icon-wrapper" href="'.$url.'" target="'.$target.'">';
		} else {
			$markup .= '<div class="gecko-icon-box__icon-wrapper">';
		}

			$icon_color_style = '';
			if (isset($attributes['iconColorValue']) && !empty($attributes['iconColorValue'])) {
				$icon_color_style = 'color: ' . $attributes['iconColorValue'] . ';';
			}
			$markup .= '<i class="%s" style="'.$icon_color_style.'"></i>';

		if ($is_link) {
			$markup .= '</a>';
		} else {
			$markup .= 	'</div>';
		}

		if (!isset($attributes['hideContent'])) {
			$markup .= '<div class="gecko-icon-box__content-wrapper">%s</div>';
		}

		$markup .= '</div>';

	return sprintf(
		$markup,
		$classes,
		htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'),
		$icon_classes,
		$content
	);
};