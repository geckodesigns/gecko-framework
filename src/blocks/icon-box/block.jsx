import React from 'react';
import classnames from 'classnames';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './style.scss';

export default class Block extends React.PureComponent {
	render() {
		const {
			iconPrefix,
			iconName,

			iconColor,
			iconColorValue,

			iconFullWidth,
			iconSize,
			iconPosition,
			iconVerticalAlign,
			iconHorizontalAlign,
			hideContent,
		} = this.props;

		const prefix = (iconPrefix) ? iconPrefix : 'fab';
		const name = (iconName) ? iconName : 'wordpress';

		let iconPositionClass = '';
		if (iconPosition) {
			iconPositionClass = 'gecko-icon-box--position-' + iconPosition;
		}

		let iconVerticalAlignClass = '';
		if (iconVerticalAlign) {
			iconVerticalAlignClass = 'gecko-icon-box--vertical-align-' + iconVerticalAlign;
		}

		let iconHorizontalAlignClass = '';
		if (iconHorizontalAlign) {
			iconHorizontalAlignClass = 'gecko-icon-box--horizontal-align-' + iconHorizontalAlign;
		}

		const classNames = classnames(
			'gecko-icon-box',
			{ 'gecko-icon-box--hide-content': hideContent },
			{ 'gecko-icon-box--full-width': iconFullWidth },
			{ [`${iconPositionClass}`]: (iconPosition) },
			{ [`${iconVerticalAlignClass}`]: (iconVerticalAlign) },
			{ [`${iconHorizontalAlignClass}`]: (iconHorizontalAlign) }
		);

		const size = (iconSize) ? iconSize : undefined;

		return (
			<div className={classNames}>
				<div className="gecko-icon-box__icon-wrapper">
					<FontAwesomeIcon icon={[prefix, name]} size={size} color={iconColorValue} />
				</div>

				{!hideContent &&
					<div className="gecko-icon-box__content-wrapper">
						{this.props.children}
					</div>
				}
			</div>
		);
	}
}