import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
// import { far } from '@fortawesome/free-regular-svg-icons';

library.add(fab);
library.add(fas);
// library.add(far);

import Block from './block';
import IconPicker from './components/IconPicker';

import { __ } from '@wordpress/i18n';
import { registerBlockType } from '@wordpress/blocks';

import {
	withColors,
	BlockControls,
	InspectorControls,
	InnerBlocks,
	PanelColorSettings,
	URLPopover,
	URLInput,
} from '@wordpress/editor';

import {
	PanelBody,
	PanelRow,
	Button,
	IconButton,
	Toolbar,
	SelectControl,
	ToggleControl,
} from '@wordpress/components';

const settings = {
	title: 'Icon Box',
	icon: <FontAwesomeIcon icon={['fas', 'icons']} />,
	category: 'common',
	description: 'Place an icon next to or above content',

	attributes: {
		iconPrefix: {
			type: 'string',
			prefix: 'fab',
		},
		iconName: {
			type: 'string',
			default: 'wordpress',
		},

		iconColor: {
			type: 'string',
		},
		iconColorValue: {
			type: 'string',
		},

		iconFullWidth: {
			type: 'boolean',
		},
		iconSize: {
			type: 'string',
			default: '',
		},
		iconPosition: {
			type: 'string',
			default: 'before',
		},
		iconHorizontalAlign: {
			type: 'string',
			default: 'left',
		},
		iconVerticalAlign: {
			type: 'string',
			default: 'top',
		},
		hideContent: {
			type: 'boolean',
			default: false,
		},

		enableIconLink: {
			type: 'boolean',
			default: false,
		},
		iconLinkUrl: {
			type: 'string',
			default: '',
		},
		iconLinkNewWindow: {
			type: 'boolean',
			default: false,
		},
		iconLinkPopoverVisible: {
			type: 'boolean',
			default: false,
		},
	},

	edit: withColors('iconColor')(props => {
		const {
			iconPrefix,
			iconName,
			iconColor,
			iconColorValue,
			iconFullWidth,
			iconSize,
			iconPosition,
			iconHorizontalAlign,
			iconVerticalAlign,
			hideContent,

			enableIconLink,
			iconLinkUrl,
			iconLinkNewWindow,
			iconLinkPopoverVisible,
		} = props.attributes;

		let allowedBlocks = [
			'core/paragraph',
			'core/heading',
			'core/button',
			'core/image',
			'core/list',
			'core/quote',
			'core/cover',
		];

		const PositionToolbar = () => (
			<Toolbar controls={[
				{
					icon: 'align-left',
					title: __('Before'),
					isActive: iconPosition === 'before',
					onClick: () => props.setAttributes({ iconPosition: 'before' }),
				},
				{
					icon: 'align-center',
					title: __('Above'),
					isActive: iconPosition === 'above',
					onClick: () => props.setAttributes({ iconPosition: 'above' }),
				},
				{
					icon: 'align-right',
					title: __('After'),
					isActive: iconPosition === 'after',
					onClick: () => props.setAttributes({ iconPosition: 'after' }),
				},
			]} />
		);

		const HorizontalAlignToolbar = () => (
			<Toolbar controls={[
				{
					icon: 'editor-alignleft',
					title: __('Left'),
					isActive: iconHorizontalAlign === 'left',
					onClick: () => props.setAttributes({ iconHorizontalAlign: 'left' }),
				},
				{
					icon: 'editor-aligncenter',
					title: __('Center'),
					isActive: iconHorizontalAlign === 'center',
					onClick: () => props.setAttributes({ iconHorizontalAlign: 'center' }),
				},
				{
					icon: 'editor-alignright',
					title: __('Right'),
					isActive: iconHorizontalAlign === 'right',
					onClick: () => props.setAttributes({ iconHorizontalAlign: 'right' }),
				},
			]} />
		);

		const VerticalAlignToolbar = () => (
			<Toolbar controls={[
				{
					icon: 'arrow-up-alt',
					title: __('Top'),
					isActive: iconVerticalAlign === 'top',
					onClick: () => props.setAttributes({ iconVerticalAlign: 'top' }),
				},
				{
					icon: 'minus',
					title: __('Center'),
					isActive: iconVerticalAlign === 'center',
					onClick: () => props.setAttributes({ iconVerticalAlign: 'center' }),
				},
				{
					icon: 'arrow-down-alt',
					title: __('Bottom'),
					isActive: iconVerticalAlign === 'bottom',
					onClick: () => props.setAttributes({ iconVerticalAlign: 'bottom' }),
				},
			]} />
		);

		const updateSelectedIcon = (icon) => {
			props.setAttributes({
				iconPrefix: icon.prefix,
				iconName: icon.iconName,
			});
		}

		let colorSettings = [];
		colorSettings.push({
			label: 'Icon Color',
			value: props.iconColor.color,
			onChange: (newIconColor) => {
				props.setAttributes({ iconColorValue: newIconColor });
				props.setIconColor(newIconColor);
				// console.log('B', props.iconColor, props.attributes.iconColor);
				// if (props.iconColor) {
				// 	props.setAttributes({
				// 		iconColorValue: props.iconColor.color,
				// 	});
				// }
			}
		});

		return ([
			<BlockControls>
				{!hideContent &&
					<PositionToolbar />
				}

				{(iconPosition !== 'above' && !hideContent) &&
					<VerticalAlignToolbar />
				}

				{(iconPosition === 'above' || hideContent) &&
					<HorizontalAlignToolbar />
				}
			</BlockControls>,

			<InspectorControls>
				<PanelBody title="Content">
					<ToggleControl
						label={__('Hide Content')}
						checked={hideContent}
						onChange={hideContent => {
							if (hideContent) {
								// Reset position option to 'above' when hiding content
								props.setAttributes({
									iconPosition: 'above',
								});
							}
							props.setAttributes({
								hideContent
							});
						}}
						help={(hideContent) ? __('Hiding content next to the icon') : __('Showing content next to icon')}
					/>
				</PanelBody>

				<PanelBody title="Icon">
					<IconPicker
						iconPrefix={iconPrefix}
						iconName={iconName}
						updateSelectedIcon={updateSelectedIcon}
					/>

					<br />

					<ToggleControl
						label={__('Full Width')}
						checked={iconFullWidth}
						onChange={iconFullWidth => { props.setAttributes({ iconFullWidth }); }}
						help={(iconFullWidth) ? __('Stretch icon-box to fit available space') : undefined}
					/>

					{!hideContent &&
						<>
							<p>{__('Icon Position')}</p>
							<PositionToolbar />
						</>
					}

					{(iconPosition !== 'above' && !hideContent) &&
						<>
							<p>{__('Icon Aligment')}</p>
							<VerticalAlignToolbar />
						</>
					}

					{(iconPosition === 'above' || hideContent) &&
						<>
							<p>{__('Icon Aligment')}</p>
							<HorizontalAlignToolbar />
						</>
					}

					<SelectControl
						label={__('Icon Size')}
						value={iconSize}
						onChange={iconSize => props.setAttributes({ iconSize })}
						options={[
							{ label: 'Extra Small', value: 'xs' },
							{ label: 'Small', value: 'sm' },
							{ label: 'Normal', value: '' },
							{ label: 'Large', value: 'lg' },
							{ label: '2x', value: '2x' },
							{ label: '3x', value: '3x' },
							{ label: '4x', value: '4x' },
							{ label: '5x', value: '5x' },
							{ label: '6x', value: '6x' },
							{ label: '7x', value: '7x' },
							{ label: '8x', value: '8x' },
							{ label: '9x', value: '9x' },
							{ label: '10x', value: '10x' },
						]}
					/>
				</PanelBody>

				<PanelColorSettings
					title="Colors"
					colorSettings={colorSettings}>
				</PanelColorSettings>

				<PanelBody title="Icon Link">

					<ToggleControl
						label={__('Enable Link')}
						checked={enableIconLink}
						onChange={enableIconLink => props.setAttributes({ enableIconLink })}
						help={(enableIconLink) ? __('Icon will be clickable') : __('Icon is not clickable')}
					/>

					{enableIconLink &&
						<>
							<p>Icon Link URL: {iconLinkUrl ? iconLinkUrl : '- no url -'}</p>

							<Button isPrimary onClick={() => props.setAttributes({ iconLinkPopoverVisible: true })}>
								<span>Edit URL</span>

								{iconLinkPopoverVisible &&
									<URLPopover
										position="bottom right"
										onClose={() => {
											props.setAttributes({
												iconLinkPopoverVisible: false
											});
										}}
										renderSettings={() => (
											<ToggleControl
												label="Open in New Window"
												checked={iconLinkNewWindow}
												onChange={(iconLinkNewWindow) => {
													props.setAttributes({ iconLinkNewWindow });
												}}
											/>
										)}
									>

										<form onSubmit={() => {
											props.setAttributes({
												iconLinkPopoverVisible: false,
											});
										}}>
											<PanelRow>
												<URLInput
													value={iconLinkUrl}
													onChange={(iconLinkUrl) => {
														props.setAttributes({
															iconLinkUrl,
														});
													}}
												/>
												<IconButton icon="editor-break" label="Apply" type="submit" />
											</PanelRow>
										</form>
									</URLPopover>
								}
							</Button>

						</>
					}
				</PanelBody>
			</InspectorControls>,

			<Block {...props.attributes}>
				<InnerBlocks allowedBlocks={allowedBlocks} />
			</Block>
		]);
	}),

	save(props) {
		return (
			<div><InnerBlocks.Content /></div>
		);
	}
}

registerBlockType('gecko/icon-box', settings);