/**
 * WordPress dependencies
 */
import './observer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThLarge } from '@fortawesome/free-solid-svg-icons';
import { __ } from '@wordpress/i18n';
import { Fragment } from '@wordpress/element';
import { InnerBlocks } from '@wordpress/editor';
import { select } from '@wordpress/data';
import {createBlock, registerBlockType} from '@wordpress/blocks';
import TemplateSelect from './components/TemplateSelect';

/**
 * Allowed blocks constant is passed to InnerBlocks precisely as specified here.
 * The contents of the array should never change.
 * The array should contain the name of each block that is allowed.
 *
 * @constant
 * @type {string[]}
*/
const ALLOWED_BLOCKS = ['gecko/grid-layout-image', 'gecko/grid-layout-basic'];

const name = 'gecko/grid-layout';

const settings = {
	title: __( 'Grid' ),
	icon: <FontAwesomeIcon icon={faThLarge} />,
	category: 'layout',
	description: __( 'Use CSS Grid to build layouts and, then add whatever content blocks you’d like.' ),
	supports: {
		align: [ 'wide', 'full' ],
		html: false,
	},
	deprecated: [
		{
			migrate() {},
			save() {
				return <InnerBlocks.Content />;
			},
		}
	],
	transforms: {
		from: [{
			type: 'block',
			blocks: ['core/columns'],
			transform: (attributes) => {
				const selected = select('core/editor').getSelectedBlock(); // because innerBlocks does not work.
				const columns = selected.innerBlocks;
				const count = columns.length;
				const w = Math.floor(12/count);
				const innerBlocks = [];
				columns.map((column) => {
					const block = createBlock('gecko/grid-layout-basic', {w:w}, column.innerBlocks);
					innerBlocks.push(block)
				});
				return createBlock('gecko/grid-layout', {}, innerBlocks);
			},
		}, ]
	},
	styles: [
		{ name: 'default', label: __( 'Default'), isDefault: true },
		{ name: 'no-gap', label: __( 'No Gap') },
	],
	edit( { insertBlocksAfter, clientId } ) {
		// Get the block so we can see if it had innerblocks
		const block = select('core/editor').getBlocksByClientId(clientId)[0];
		const hasInnerBlocks = (block && block.innerBlocks) ? block.innerBlocks.length > 0 : false;
		
		return (
			<Fragment>
				<div className="wp-block-gecko-grid-layout-editor">
					{
						typeof insertBlocksAfter === 'function' ?
						<TemplateSelect hasInnnerBlocks={hasInnerBlocks} allowedBlocks={ ALLOWED_BLOCKS }/> :
						<p>{__('Preview Not Available')}</p>
					}
				</div>
			</Fragment>
		);
	},

	save() {
		// Honestly I don't want to do this but conventions are conventions.
		return(<div><InnerBlocks.Content /></div>); 
	},
};
registerBlockType(name, settings);