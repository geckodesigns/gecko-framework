<?php
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS|JSON_HEX_QUOT);

	$classBase = 'gecko-slideshow__slide-video';

	$classes = [
		$classBase,
		'swiper-slide',
	];

	if (isset($attributes['addContrast']) && $attributes['addContrast']) {
		$classes[] = $classBase . '--add-contrast';
	}

	if (isset($attributes['verticalAlign']) && $attributes['verticalAlign'] != "") {
		$classes[] = $classBase . '--vertical-align-' . $attributes['verticalAlign'];
	} else {
		$classes[] = $classBase . '--vertical-align-top';
	}

	if (isset($attributes['rowWidth']) && $attributes['rowWidth'] != "") {
		$classes[] = $classBase . '--row-width-' . $attributes['rowWidth'];
	} else {
		$classes[] = $classBase . '--row-width-full';
	}



	// Background style classes
	if (isset($attributes['bgOverlayOpacity']) && $attributes['bgOverlayOpacity'] != "") {
		$classes[] = $classBase . '--opacity-' . $attributes['bgOverlayOpacity'];
	} else {
		$classes[] = $classBase . '--opacity-0';
	}

	$markup = '<div class="'.implode(' ', $classes). '" data-props="%s">';

	if (isset($attributes['bgOverlayColorValue']) && $attributes['bgOverlayColorValue'] != "") {
		$overlayStyle = 'background-color: ' . $attributes['bgOverlayColorValue'] . ';';
		$markup .= '<div class="' . $classBase . '-overlay" style="' . $overlayStyle . '"></div>';
	}

	if (isset($attributes['bgVideoUrl']) && $attributes['bgVideoUrl'] != "") {
		$markup .= '<video class="'.$classBase.'-video" src="' . $attributes['bgVideoUrl'] . '" autoPlay loop muted></video>';
	}

	$markup .= '%s';

	$markup .= '</div>';


	return sprintf(
		$markup,
		htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'),
		$content
	);
};