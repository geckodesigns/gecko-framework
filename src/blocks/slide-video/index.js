import { Fragment } from 'react';
import classnames from 'classnames';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faFilm,
	faExchangeAlt,
	faSquare,
} from '@fortawesome/free-solid-svg-icons';

import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';

import {
	withColors,
	InspectorControls,
	InnerBlocks,
	BlockControls,
	PanelColorSettings,
	MediaUpload,
} from '@wordpress/editor';

import {
	createBlock,
} from '@wordpress/blocks';

import {
	PanelBody,
	PanelRow,
	Toolbar,
	ToggleControl,
	RangeControl,
	Button,
} from '@wordpress/components';

const settings = {
	title: 'Video Slide',
	icon: <FontAwesomeIcon icon={faFilm} />,
	category: 'layout',
	description: 'Video Slide',

	parent: ['gecko/slideshow'],

	supports: {
		customClassName: false
	},

	transforms: {
		to: [{
				type: 'block',
				blocks: ['gecko/slide-basic'],
				transform: (attributes, innerBlocks) => {
					return createBlock('gecko/slide-basic', attributes, innerBlocks);
				},
			}],
	},

	attributes: {
		verticalAlign: {
			type: 'string',
			default: 'top',
		},
		rowWidth: {
			type: 'string',
			default: 'full',
		},

		addContrast: {
			type: 'boolean',
			default: false,
		},

		bgColor: {
			type: 'string',
		},
		bgColorValue: {
			type: 'string',
		},
		bgOverlayColor: {
			type: 'string'
		},
		bgOverlayColorValue: {
			type: 'string'
		},

		bgOverlayOpacity: {
			type: 'number',
			default: 0
		},

		bgVideoId: {
			type: 'number'
		},
		bgVideoUrl: {
			type: 'string',
		},
	},

	edit: withColors('bgColor', 'bgOverlayColor')((props) => {
		const {
			className,
		} = props;

		const {
			verticalAlign,
			rowWidth,
			addContrast,

			bgColor,
			bgColorValue,

			bgOverlayColor,
			bgOverlayColorValue,

			bgOverlayOpacity,

			bgVideoId,
			bgVideoUrl,
		} = props.attributes;

		let colorSettings = [];
		if (bgVideoId) {
			colorSettings.push({
				label: 'Overlay Color',
				value: props.bgOverlayColor.color,
				onChange: (color) => {
					props.setAttributes({ bgOverlayColorValue: color });
					props.setBgOverlayColor(color);
				},
			});
		} else {
			colorSettings.push({
				label: 'Background Color',
				value: props.bgColor.color,
				onChange: (color) => {
					props.setAttributes({ bgColorValue: color });
					props.setBgColor(color);
				}
			});
		}

		const VerticalAlignToolbar = () => (
			<Toolbar controls={[
				{
					icon: 'arrow-up-alt',
					title: __('Top'),
					isActive: verticalAlign === 'top',
					onClick: () => props.setAttributes({ verticalAlign: 'top' }),
				},
				{
					icon: 'minus',
					title: __('Center'),
					isActive: verticalAlign === 'center',
					onClick: () => props.setAttributes({ verticalAlign: 'center' }),
				},
				{
					icon: 'arrow-down-alt',
					title: __('Bottom'),
					isActive: verticalAlign === 'bottom',
					onClick: () => props.setAttributes({ verticalAlign: 'bottom' }),
				},
			]} />
		)


		const RowWidthToolbar = () => (
			<Toolbar controls={[
				{
					icon: <FontAwesomeIcon icon={faExchangeAlt} />,
					title: __('Full Width'),
					isActive: rowWidth === 'full',
					onClick: () => props.setAttributes({ rowWidth: 'full' }),
				},
				{
					icon: <FontAwesomeIcon icon={faSquare} />,
					title: __('Large'),
					isActive: rowWidth === 'large',
					onClick: () => props.setAttributes({ rowWidth: 'large' }),
				},
				{
					icon: 'align-left',
					title: __('Half (left)'),
					isActive: rowWidth === 'half-left',
					onClick: () => props.setAttributes({ rowWidth: 'half-left' }),
				},
				{
					icon: 'align-center',
					title: __('Half (left)'),
					isActive: rowWidth === 'half-center',
					onClick: () => props.setAttributes({ rowWidth: 'half-center' }),
				},
				{
					icon: 'align-right',
					title: __('Half (right)'),
					isActive: rowWidth === 'half-right',
					onClick: () => props.setAttributes({ rowWidth: 'half-right' }),
				},
			]} />
		)

		const verticalAlignClass = className + '--vertical-align-' + verticalAlign;
		const rowWidthClass = className + '--row-width-' + rowWidth;

		const bgOverlayOpacityClass = className + '--opacity-' + bgOverlayOpacity;

		const classNames = classnames(
			className,
			verticalAlignClass,
			rowWidthClass,
			{ [`${className}--add-contrast`]: (addContrast) },
			{ [`${className}--has-overlay`]: (bgVideoUrl) },
			{ [`${bgOverlayOpacityClass}`]: (bgOverlayOpacity >= 0) }
		);

		let wrapperStyle = {};
		let overlayStyle = {};

		if (bgColorValue) {
			wrapperStyle.backgroundColor = bgColorValue;
		}

		if (bgOverlayColorValue) {
			overlayStyle.backgroundColor = bgOverlayColorValue;
		}

		return ([
			<BlockControls>
				<VerticalAlignToolbar />
				<RowWidthToolbar />
			</BlockControls>,

			<InspectorControls>
				<PanelBody title="Slide Layout">
					<p>Vertical Alignment</p>
					<VerticalAlignToolbar />

					<p>Row Width</p>
					<RowWidthToolbar />
				</PanelBody>

				<PanelBody title="Slide Options">
					<ToggleControl
						label="Add Contrast"
						help={(addContrast) ? 'White text on dark background' : 'Dark text on light background'}
						checked={addContrast}
						onChange={addContrast => { props.setAttributes({ addContrast }) }}
					/>

					<p>Video:<br />{(bgVideoUrl && bgVideoUrl !== "") ? bgVideoUrl : '- no video set -'}</p>

					<PanelRow>
						<MediaUpload
							onSelect={(video) => {
								if (video) {
									props.setAttributes({
										bgVideoId: video.id,
										bgVideoUrl: video.url,
										bgColor: undefined,
										bgColorValue: undefined,
									});
								}
							}}
							allowedTypes={['video']}
							value={bgVideoId}
							title="Background Video"
							render={({ open }) => (
								<div>
									<Button isDefault onClick={open}>
										{(bgVideoId) ? 'Change' : 'Choose'} Video
									</Button>

									{bgVideoId &&
										<Button isDefault onClick={() => {
											props.setAttributes({
												bgVideoId: undefined,
												bgVideoUrl: undefined,
											});
										}}>Remove Video</Button>
									}
								</div>
							)}
						/>
					</PanelRow>

					{bgVideoId &&
						<Fragment>
							<br />

							<RangeControl
								label="Overlay Opacity"
								value={bgOverlayOpacity}
								onChange={(bgOverlayOpacity) => { props.setAttributes({ bgOverlayOpacity }); }}
								beforeIcon={(bgOverlayOpacity > 0) ? "visibility" : "hidden"}
								min={0}
								max={10}
							/>
						</Fragment>
					}
				</PanelBody>

				<PanelColorSettings
					title="Colors"
					colorSettings={colorSettings}>
				</PanelColorSettings>
			</InspectorControls>,

			<div className={classNames} style={wrapperStyle}>
				{bgOverlayColorValue &&
					<div className={className+'__overlay'} style={overlayStyle} />
				}

				{bgVideoUrl &&
					<video
						className={className+'__video'}
						src={bgVideoUrl}
						autoPlay={true}
						loop={true}
						muted={true}
					/>
				}
				<InnerBlocks />
			</div>
		]);
	}),

	save(props) {
		return(<div><InnerBlocks.Content /></div>);
	}
}

registerBlockType('gecko/slide-video', settings);