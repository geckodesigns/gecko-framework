/**
 * WordPress dependencies
 */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImage } from '@fortawesome/free-solid-svg-icons';
import debounce from "lodash/debounce";
import {
	PanelBody,
	RangeControl,
	IconButton,
	Toolbar,
	ResizableBox,
} from '@wordpress/components';
import { __ } from '@wordpress/i18n';
import { Fragment } from '@wordpress/element';
import { createBlock,registerBlockType } from '@wordpress/blocks';
import {
	InspectorControls,
	InnerBlocks,
	MediaUpload,
	MediaPlaceholder,
	BlockControls,
} from '@wordpress/editor';
import {
	select
} from '@wordpress/data';

const ALLOWED_BLOCKS = ['core/paragraph', 'core/heading', 'core/button', 'core/list', 'core/quote', 'gecko/button'];

export const name = 'gecko/image';
export const settings = {
	title: __( 'Advanced Image' ),
	icon: <FontAwesomeIcon icon={faImage} />,
	description: __( 'An image block.' ),
	category: 'common',
	supports: {
		className: true,
	},
	styles: [
		{ name: 'default', label: __( 'Default'), isDefault: true },
		{ name: 'hover', label: __( 'Hover') },
		{ name: 'no-caption', label: __( 'No Caption') },
	],
	attributes: {
		h: { type: 'number', default: 1},
		w: { type: 'number', default: 1},
		minHeight: { type: 'number', default: 200,},
		imgId: { type: 'number'},
		imgUrl: { type: 'string'},
	},

	transforms: {
		from: [
			{
				type: 'block',
				blocks: ['core/image'],
				transform: (attributes, innerBlocks) => {
					const selected = select('core/editor').getSelectedBlock(); // because innerBlocks does not work.
					// It appears that innerBlocks will be added in the future.
					const {h, w, bgMedia, bgMediaUrl, mediaId, mediaUrl, minHeight} = attributes;
					return createBlock(name, {
						h: h,
						w: w,
						imgId: bgMedia || mediaId,
						imgUrl: bgMediaUrl || mediaUrl,
						minHeight: minHeight,
					}, selected.innerBlocks);
				},
			},	
		]
	},

	deprecated: [{
		migrate() {},
		save() {
			return(<InnerBlocks.Content />);
		},
	}],

	edit({ attributes, setAttributes, className, insertBlocksAfter, toggleSelection }) {
		const {
			h,
			w,
			imgId,
			imgUrl,
			minHeight,
		} = attributes;
		const styles = {
			gridColumnEnd: 'span '+ w,
			gridRowEnd: 'span ' + h,
			minHeight: minHeight + 'px',
		};
		const onResizing = (event, direction, elt, delta) => {
			if (direction === 'bottom'){
				const newMinHeight = (elt.clientHeight > 600) ? 600 : elt.clientHeight;
				setAttributes({ minHeight: newMinHeight });
				elt.style.height = "100%";
			};
			if (direction !== 'right') return;
			// console.log(elt);
			const columnWidth = Math.floor(elt.parentNode.offsetWidth / w);
			const currentSpan = Math.floor(elt.parentNode.offsetWidth / columnWidth);
			const elColWidth = Math.floor(elt.clientWidth / w);
			const spans = Math.floor(elt.clientWidth / columnWidth);
			const toUpdate = Math.floor(parseInt(delta.width, 10) / columnWidth);
			const original = w - toUpdate;
			let newWidth = spans;
			if (newWidth > 12) {newWidth = 12}
			if (newWidth < 1) {newWidth = 1}
			setAttributes( {
				w: newWidth,
			} );
			elt.style.width = "100%";
			// }
			return;
		}
		return (
			<Fragment>
				<InspectorControls>
					<PanelBody title="Grid Item">
						<RangeControl
							label={ __( 'Minimum Height' ) }
							value={ minHeight }
							onChange={ ( next ) => {
								setAttributes( {
									minHeight: next,
								} );
							} }
							min = "50"
							max = "600"
							step = "1"
						/>
						<RangeControl
							label={ __( 'Width' ) }
							value={ w }
							onChange={ ( next ) => {
								setAttributes( {
									w: next,
								} );
							} }
							min={ 1 }
							max={ 12 }
						/>
						<RangeControl
							label={ __( 'Span Rows' ) }
							value={ h }
							onChange={ ( next ) => {
								setAttributes( {
									h: next,
								} );
							} }
							min={ 1 }
							max={ 12 }
						/>
					</PanelBody>
				</InspectorControls>
				<BlockControls>
					<Toolbar>
						<MediaUpload
							onSelect={(value) => {
								// console.log(value);
								setAttributes({
									imgId: value.id,
									imgUrl: value.url,
								});
							}}
							type={['image']}
							value={(imgId)? imgId: null }
							render={({open}) => {
								return(
									<IconButton
										className="components-icon-button components-toolbar__control"
										label={ __( 'Edit image' ) }
										onClick={open}
										icon="edit"
									/>
								);
							}}
						/>
					</Toolbar>
				</ BlockControls>
				<div className={`gecko-grid-layout-editor-styles`} style={styles}></div>
				<ResizableBox
						size = {{
							width: '',
							height: '100%',
						}}
						// minHeight = "50"
						// minWidth = "100%"
						className = {
							`wp-block-gecko-grid-layout-editor__wrap gecko-image ${attributes.className}`
						}
						// minHeight={ minHeight }
						// maxHeight={600}
						// lockAspectRatio
						enable={ {
							top: false,
							right: true,
							bottom: true,
							left: false,
						} }
						onResizeStart={ (event, direction, elt, delta) => {
							toggleSelection( false );
						} }
						onResize={ 	debounce(onResizing, 250) }
						onResizeStop={ ( event, direction, elt, delta ) => {
							elt.style.width = "100%";
							if (direction !== 'bottom') return;
							const newMinHeight = (elt.clientHeight > 600)? 600 : elt.clientHeight;
							setAttributes( {
								minHeight: newMinHeight,
							} );
							toggleSelection( true );
						} }
					>
					{
						!imgId &&
						<MediaPlaceholder
							onSelect={(value) => {
									setAttributes({
										imgId: value.id,
										imgUrl: value.url,
									});
								}}
							allowedTypes={['image']}
							accept="image/*"
						/>
					}
					{
						imgUrl &&
						<img className="gecko-image__image" src={imgUrl} />
					}
					{
						imgId &&
						<figcaption className="gecko-image__caption">
							{ typeof insertBlocksAfter === 'function' //This line makes sure styles do not break
								? <InnerBlocks templateLock={ false } allowedBlocks={ALLOWED_BLOCKS}/>
								: <p>Lorem Ipsum</p> // This is what shows as the preview content.
							}
						</figcaption>
					}
				</ResizableBox>
			</Fragment>
		);
	},

	save() {
		// Honestly I don't want to do this but conventions are conventions.
		return(<div><InnerBlocks.Content /></div>); 
	},
};
registerBlockType(name, settings);