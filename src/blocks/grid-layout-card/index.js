/**
 * WordPress dependencies
 */
import debounce from "lodash/debounce";
import {
	PanelBody,
	RangeControl,
	Path,
	SVG,
	TextControl,
	Toolbar,
	ToggleControl,
} from '@wordpress/components';
import { __ } from '@wordpress/i18n';
import { Fragment } from '@wordpress/element';
import { createBlock, registerBlockType } from '@wordpress/blocks';
import {
	InspectorControls,
	InnerBlocks,
	MediaUpload,
	MediaPlaceholder,
	BlockControls,
	PanelColorSettings,
} from '@wordpress/block-editor';
import {
	select
} from '@wordpress/data';

const ALLOWED_BLOCKS = ['core/paragraph', 'core/heading', 'core/button', 'core/list', 'core/quote'];

const name = 'gecko/grid-layout-card';

const settings = {
	title: __( 'Card' ),
	parent: ['gecko/grid-layout'],
	icon: <SVG viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><Path d="M0,0h24v24H0V0z" fill="none" /><Path d="m19 5v14h-14v-14h14m0-2h-14c-1.1 0-2 0.9-2 2v14c0 1.1 0.9 2 2 2h14c1.1 0 2-0.9 2-2v-14c0-1.1-0.9-2-2-2z" /><Path d="m14.14 11.86l-3 3.87-2.14-2.59-3 3.86h12l-3.86-5.14z" /></SVG>,
	description: __( 'Grid block cards.' ),
	category: 'common',
	supports: {
		className: true,
		inserter: true,
		reusable: false,
		html: false,
	},
	styles: [
		{ name: 'default', label: __( 'Default'), isDefault: true },
		{ name: 'hover', label: __( 'Hover') },
		{ name: 'no-caption', label: __( 'No Caption') },
	],
	attributes: {
		h: { type: 'number', default: 1},
		w: { type: 'number', default: 1},
		minHeight: { type: 'number', default: 200,},
		backgroundColor: { type: 'string'},
		alignContent: { type: 'string', default: 'top'},
		url: { type: 'string'},
		newWindow: {type: 'boolean', default: false},
	},

	transforms: {
		from: [
			{
				type: 'block',
				blocks: ['gecko/grid-layout-basic', 'gecko/grid-layout-image'],
				transform: (attributes, innerBlocks) => {
					const selected = select('core/editor').getSelectedBlock(); // because innerBlocks does not work.
					// It appears that innerBlocks will be added in the future.
					const {h, w, minHeight} = attributes;
					return createBlock('gecko/grid-layout-card', {
						h: h,
						w: w,
						minHeight: minHeight,
					}, selected.innerBlocks);
				},
			},	
		]
	},

	deprecated: [{
		migrate() {},
		save() {
			return(<InnerBlocks.Content />);
		},
	}],

	edit({ attributes, setAttributes, className, insertBlocksAfter, toggleSelection }) {
		const {
			h,
			w,
			minHeight,
			backgroundColor,
			alignContent,
			newWindow,
			url
		} = attributes;
		const styles = {
			gridColumnEnd: 'span '+ w,
			gridRowEnd: 'span ' + h,
			minHeight: minHeight + 'px',
			backgroundColor: backgroundColor,
			// alignSelf: (alignContent === 'center') ? 'center' : 'start',
		};
		return (
			<Fragment>
				<InspectorControls>
					<PanelBody title="Grid Item">
						<RangeControl
							label={ __( 'Minimum Height' ) }
							value={ minHeight }
							onChange={ ( next ) => {
								setAttributes( {
									minHeight: next,
								} );
							} }
							min = "50"
							max = "600"
							step = "1"
						/>
						<RangeControl
							label={ __( 'Width' ) }
							value={ w }
							onChange={ ( next ) => {
								setAttributes( {
									w: next,
								} );
							} }
							min={ 1 }
							max={ 12 }
						/>
						<RangeControl
							label={ __( 'Span Rows' ) }
							value={ h }
							onChange={ ( next ) => {
								setAttributes( {
									h: next,
								} );
							} }
							min={ 1 }
							max={ 12 }
						/>
						<TextControl
							label="URL"
							value={ url }
							onChange={(url) => setAttributes({ url })}
						/>
						<ToggleControl
							label="Open in New Window?"
							checked={ newWindow }
							onChange={(newWindow) => setAttributes({ newWindow })}
						/>
						<PanelColorSettings
							title={ __( 'Color Settings' ) }
							colorSettings={ [
								{
									value: backgroundColor,
									onChange: (next) => {
										setAttributes({
											backgroundColor: next,
										})
									},
									label: 'Background Color',
								},
							]}
						/>
					</PanelBody>
				</InspectorControls>
				<BlockControls>
					{/* <Toolbar controls={[
						{
							icon: 'arrow-up-alt',
							title: __('Align Top'),
							isActive: alignContent === 'top',
							onClick: () => setAttributes({
								alignContent: 'top'
							}),
						},
						{
							icon: 'align-center',
							title: __('Align Center'),
							isActive: alignContent === 'center',
							onClick: () => setAttributes({alignContent: 'center'}),
						},
						{
							icon: 'arrow-down-alt',
							title: __('Align Bottom'),
							isActive: alignContent === 'bottom',
							onClick: () => setAttributes({alignContent: 'bottom'}),
						},
					]}>
					</Toolbar> */}
				</ BlockControls>
				<div className={`gecko-grid-layout-editor-styles`} style={styles}></div>
				<div className = {
					`wp-block-gecko-grid-layout-editor__wrap gecko-grid-layout-card ${attributes.className} align-content-${attributes.alignContent}`
				} >
					{ typeof insertBlocksAfter === 'function' //This line makes sure styles do not break
						? <InnerBlocks templateLock={ false }/>
						: <p>Lorem Ipsum</p> // This is what shows as the preview content.
					}
				</div>
			</Fragment>
		);
	},

	save() {
		return(<div><InnerBlocks.Content /></div>); 
	},
};
registerBlockType(name, settings);