import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGripHorizontal } from '@fortawesome/free-solid-svg-icons';

import Block from './block';

import { __ } from '@wordpress/i18n';
import { registerBlockType } from '@wordpress/blocks';

import {
	BlockControls,
	InspectorControls,
	InnerBlocks,
} from '@wordpress/editor';

import {
	PanelBody,
	Toolbar,
} from '@wordpress/components';

const settings = {
	title: 'Button Row',
	icon: <FontAwesomeIcon icon={faGripHorizontal} /> ,
	category: 'common',
	description: 'Manageable group of buttons',

	attributes: {
		horizontalAlign: {
			type: 'string',
			default: 'left',
		},
	},

	edit: (function(props) {
		const {
			horizontalAlign,
		} = props.attributes;

		let allowedBlocks = [
			'core/button',
		];

		const CustomAlignmentToolbar = () => (
			<Toolbar controls={[
				{
					icon: 'editor-alignleft',
					title: __('Left'),
					isActive: horizontalAlign === 'left',
					onClick: () => props.setAttributes({ horizontalAlign: 'left' }),
				},
				{
					icon: 'editor-aligncenter',
					title: __('Center'),
					isActive: horizontalAlign === 'center',
					onClick: () => props.setAttributes({ horizontalAlign: 'center' }),
				},
				{
					icon: 'editor-alignright',
					title: __('Right'),
					isActive: horizontalAlign === 'right',
					onClick: () => props.setAttributes({ horizontalAlign: 'right' }),
				},
			]} />
		)

		return ([
			<BlockControls>
				<CustomAlignmentToolbar />
			</BlockControls>,

			<InspectorControls>
				<PanelBody title="Options">
					<p>{__('Horiztonal Alignment')}</p>
					<CustomAlignmentToolbar />
				</PanelBody>
			</InspectorControls>,

			<Block {...props.attributes}>
				<InnerBlocks allowedBlocks={allowedBlocks} />
			</Block>
		]);
	}),

	save(props) {
		return(<div><InnerBlocks.Content /></div>);
	}
}

registerBlockType('gecko/button-row', settings);