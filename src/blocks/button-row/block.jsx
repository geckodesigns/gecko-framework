import React from 'react';
import classnames from 'classnames';

import './style.scss';

export default class Block extends React.PureComponent {
	render() {
		const {
			horizontalAlign,
		} = this.props;

		let horizontalClass = '';
		if (horizontalAlign) {
			horizontalClass = 'gecko-button-row--horizontal-' + horizontalAlign;
		}

		const classNames = classnames(
			'gecko-button-row',
			{ [`${horizontalClass}`]: (horizontalAlign) }
		);

		return (
			<div className={classNames}>
				{this.props.children}
			</div>
		);
	}
}