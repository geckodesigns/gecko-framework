<?php
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS|JSON_HEX_QUOT);

	$classes = "gecko-button-row";

	if (isset($attributes['horizontalAlign'])) {
		$classes .= " gecko-button-row--horizontal-" . $attributes['horizontalAlign'];
	}

	$classes = trim($classes);

	return sprintf('<div class="%s" data-props="%s">%s</div>',
	$classes, htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'), $content);
};