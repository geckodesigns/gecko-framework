jQuery(document).ready(($) => {
	const widget = $('.gecko-google-reviews-widget[data-behaviour="popup"]')
	const popup = $('.gecko-google-reviews-popup');
	const overlay = popup.find('.gecko-google-reviews-popup__overlay');
	const closeButton = popup.find('.gecko-google-reviews-popup__close-popup');

	widget.on('click', function(e) {
		e.preventDefault();
		showPopup();
	});

	overlay.on('click', function(e) {
		hidePopup();
	});
	closeButton.on('click', function(e) {
		hidePopup();
	});

	function hidePopup() {
		popup.attr('data-status', 'hidden');
	}

	function showPopup() {
		popup.attr('data-status', 'active');
	}
});