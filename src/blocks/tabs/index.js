import classnames from 'classnames';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';

import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { select } from '@wordpress/data';

import {
	InnerBlocks,
} from '@wordpress/editor';

/* import Preview from './components/Preview'; */

const ALLOWED_BLOCKS = [
	'gecko/tabs-panel',
];

const settings = {
	title: 'Tabs',
	icon: <FontAwesomeIcon icon={faEllipsisH} /> ,
	category: 'layout',
	description: 'Tab/Panel Navigation',
	attributes: {
	},
	edit: (props) => {
		const {
			clientId,
			className,
		} = props;

		const wrapperClasses = classnames(
			className
		)

		const parentBlock = select('core/editor').getBlocksByClientId(clientId)[0];
		const childBlocks = parentBlock.innerBlocks;
		return (
			<>
				{ childBlocks &&
					childBlocks.map(block =>
							<button>{block.attributes.panelTitle}</button>
						)
				}
				<div className={wrapperClasses}>
					<InnerBlocks
						allowedBlocks={ALLOWED_BLOCKS}
					/>
				</div>
			</>
		);
	},

	save(props) {
		return(<InnerBlocks.Content />);
	}
}

registerBlockType('gecko/tabs', settings);