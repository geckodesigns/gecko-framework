document.addEventListener("DOMContentLoaded", (event) => {
    const tabBlocks = document.getElementsByClassName('gecko-tabs');
    [...tabBlocks].forEach((block, index) => {
        // Create nav element and insert into DOM in gecko-tabs element
        const tabNav = document.createElement("nav");
        tabNav.className = "gecko-tabs__nav";
        block.insertBefore(tabNav, block.childNodes[0]);

        // Get block index
        const blockIndex = index;
        const styleActive = 'active';
        const styleInactive = 'inactive';
        const statusActive = 'visible';
        const statusInactive = 'hidden';

        // Get child panels
        const panels = block.querySelectorAll('.gecko-tabs__panel');
        [...panels].forEach((panel, index) => {
            // Get panel data
            const attributes = JSON.parse(panel.getAttribute('data-props'));
            // Create unique panelId to use as data-target
            const panelId = "panel_" + blockIndex + "_" + index;
            // Set panelId as data-target on panel
            panel.setAttribute('data-target', panelId);
            panel.setAttribute('data-status', 'hidden');

            // create Nav Button; add classes, text and data-target. Insert the nav button into the nav DOM element.
            const navButton = document.createElement("button");
            navButton.classList = "gecko-tabs__nav_button gecko-tabs--button";
            navButton.setAttribute('data-target', panelId);
            navButton.setAttribute('data-style', styleInactive);
            const navButtonText = document.createTextNode(attributes.panelTitle);
            navButton.appendChild(navButtonText);
            tabNav.appendChild(navButton);

            if (index === 0){
                panel.classList.add('active');
                panel.setAttribute('data-status', statusActive);
                navButton.classList.add('active');
                navButton.setAttribute('data-style', styleActive);
            }

            navButton.addEventListener("click", (e) => {

                const clickedButton = e.target;
                const buttonNodeList = tabNav.childNodes;
                buttonNodeList.forEach(button => {
                    button.classList.remove('active');
                    button.setAttribute('data-style', styleInactive);
                });
                clickedButton.classList.add('active');
                clickedButton.setAttribute('data-style', styleActive);

                const dataTarget = clickedButton.getAttribute('data-target');
                panels.forEach(panel => {
                    if ( panel.getAttribute('data-status') === statusActive ){
                        panel.setAttribute('data-status', statusInactive);
                        panel.classList.remove(styleActive);
                    }
                    if ( panel.getAttribute('data-target') === dataTarget){
                        panel.setAttribute('data-status', statusActive);
                        panel.classList.add(styleActive);
                    }
                });
            });
        });
    });
});
