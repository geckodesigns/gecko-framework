/**
 * WordPress dependencies
 */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { __ } from '@wordpress/i18n';
import {
	registerBlockType
} from '@wordpress/blocks';
import {
	PanelBody,
	SelectControl,
} from '@wordpress/components';
import {
	InnerBlocks,
	InspectorControls,
} from '@wordpress/editor';
import Gallery from './Gallery';

/**
 * Allowed blocks constant is passed to InnerBlocks precisely as specified here.
 * The contents of the array should never change.
 * The array should contain the name of each block that is allowed.
 * In columns block, the only block we allow is 'dmp/grid-item'.
 *
 * @constant
 * @type {string[]}
*/
const name = 'gecko/instagram-gallery';
const settings = {
	title: __( 'Instagram Gallery' ),
	icon: <FontAwesomeIcon icon={faInstagram} />,
	category: 'common',
	description: __( 'Instagram Gallery for Gutenberg' ),
	supports: {
		html: false,
	},
	deprecated: [],
	attributes: {},

	edit: ({attributes, setAttributes, insertBlocksAfter, className}) => {
		// const {} = attributes;
		return ([
			<InspectorControls>
				<PanelBody title={__("Settings")}>

				</PanelBody>
			</InspectorControls>,
			<Gallery show={10}/>
		]);
	},

	save: () => {
		return null;
	}
};

registerBlockType(name, settings);