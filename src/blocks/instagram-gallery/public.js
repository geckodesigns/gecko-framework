import ReactDom from 'react-dom';
import Gallery from './Gallery';
document.addEventListener("DOMContentLoaded", (event) => {
	const elements = document.getElementsByClassName('gecko-instagram-gallery');
	[...elements].forEach(element => {
		const attributes = JSON.parse(element.getAttribute('data-props'));
		ReactDom.render(<Gallery {...attributes} />, element);
	});
});