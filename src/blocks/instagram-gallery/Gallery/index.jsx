import React from 'react';
import apiFetch from '@wordpress/api-fetch';
import Gram from './Gram';

export default class Assets extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			grams: []
		}
	}

	get = () => {
		apiFetch({
			path: `/gecko/v1/instagram`
		}).then(data => {
			this.setState({
				grams: data,
			})
		}).catch(error => {
			console.error(error);
		});
	}

	componentDidMount() {
		this.get();
	}

	render() {
		// console.log(this.props);
		const {grams} = this.state;
		// const {show} = this.props;
		const show = 12;
		let showing = 0;
		return(
			<div className={`gecko-instagram-gallery`} >
				{grams.map((gram, key) => {
					showing++;
					if(show && showing > show) return;
					return (<Gram key={key} gram={gram}/>)
				})}
			</div>
		);
	}
}