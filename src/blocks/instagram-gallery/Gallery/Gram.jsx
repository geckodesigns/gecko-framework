import React from 'react';

const addBodyClass = () => document.body.classList.add('asset-modal-open');
const removeBodyClass = () => document.body.classList.remove('asset-modal-open');

export default class Gram extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			active: false,
		}
		// this.ref = React.createRef();
	}


	close = (e) => {
		console.log('close');
		// removeBodyClass();
		if(this.state.content){
			removeBodyClass();
		}
		this.setState({
			active: false,
			content: false,
		});
	}


	componentDidMount() {
		// this.isVisible(this.ref, this.get);
		// document.addEventListener('click', this.handleOutsideClick, false);
	}

	componentWillUnmount() {
		// this.isVisible(this.ref, null, true);
		// document.removeEventListener('click', this.handleOutsideClick, false);
	}

	// Right now unselect happen on outside click. I think there is a better way to do this.
	handleOutsideClick = (e) => {
		// if (this.ref.current && !this.ref.current.contains(e.target)) {
		// 	this.close(e);
		// }
	}


	render() {
		const { gram } = this.props;
		return(
			<figure className={`gecko-instagram-gallery__gram`}>
				<div className={`gecko-instagram-gallery__image`}>
					<img src={(gram.media_type === 'VIDEO') ? gram.thumbnail_url : gram.media_url} />
				</div>
				<figcaption className={`gecko-instagram-gallery__caption`} dangerouslySetInnerHTML={{ __html: gram.caption }} />
			</figure>
		);
	}
}
