import React from 'react';
import { Fragment } from '@wordpress/element';
import { PluginSidebar, PluginSidebarMoreMenuItem, PluginBlockSettingsMenuItem } from '@wordpress/edit-post';
import { registerPlugin } from '@wordpress/plugins';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFortAwesomeAlt } from '@fortawesome/free-brands-svg-icons';
import SelectIcon from '../../lib/components/select-icon/index.jsx';
import BoxModel from '../../lib/components/box-model/index.jsx';
import {Icon} from './format-type.js';
import {
	Panel,
	PanelBody,
	PanelRow,
	TextControl,
	RangeControl,
	ToggleControl,
	SelectControl,
	FormFileUpload,
	Button,
	Fill,
	Toolbar,
	ToolbarButton,
} from '@wordpress/components';

import {
	registerFormatType,
	getTextContent,
	applyFormat,
	removeFormat,
	insert,
	slice,
	create,
} from '@wordpress/rich-text';

import {
	BlockControls
} from '@wordpress/editor';

import { subscribe } from '@wordpress/data';

registerFormatType('gecko/fontawesome-icon', Icon);
function createIconFormat({icon}) {
	const format = {
		type: 'gecko/fontawesome-icon',
		attributes: {
			icon,
		},
	};
	return format;
}

class Component extends React.Component {
	addIcon(event) {
		const format = createIconFormat({icon: 'home'});
		event.preventDefault();

		const toInsert = applyFormat(create({ text: 'home' }), format, 0);
		insert('home', toInsert);
		console.log('Adding Icon');
	}
	render() {
		console.log(this.props, this.state);
		return (
			<Fragment>
				<PluginBlockSettingsMenuItem
					// allowedBlocks={['core/heading']}
					icon={<FontAwesomeIcon icon={faFortAwesomeAlt} />}
					label={` Add Icon`}
					onClick={(e) => {console.log(e); return;}}
				/>
				<PluginSidebarMoreMenuItem
					target="fontawesome-icon"
					icon={<FontAwesomeIcon icon={faFortAwesomeAlt} />}
				>
					Fontawesome Icon
				</PluginSidebarMoreMenuItem>
				<PluginSidebar
					name="fontawesome-icon"
					title="Fontawesome Icon"
				>	
					<PanelBody title="Icon Settings">
						<SelectIcon
							label="Icon"
							defaultValue={<FontAwesomeIcon icon={faFortAwesomeAlt} />}
						// onChange={(e) => { props.setAttributes({ icon: e.value }) }}
						/>
						<br />
						<BoxModel
							label="Padding"
							value={{top: null, right: null, bottom: null, left:null,}}
							// onChange={(value) => { props.setAttributes({ padding: value }); }}
							min="1"
							max="3"
						/>
						<Button onClick={this.addIcon}>Insert Icon</Button>
					</PanelBody>
					{/* <PanelColorSettings
						title="Color Settings"
						colorSettings={[
							{
								label: 'Color',
								value: color,
								// onChange: (newColor) => { props.setAttributes({ textColor: newColor }) },
							}
						]}>
					</PanelColorSettings> */}
				</PluginSidebar>
			</Fragment>
		)
	}
};

registerPlugin('fontawesome-icon', {
	icon: <FontAwesomeIcon icon={faFortAwesomeAlt} />,
	render: Component,
});