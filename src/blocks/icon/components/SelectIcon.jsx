import React from 'react';
import Select from 'react-select';

export default class SelectIcon extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			defaultValue: null,
			icons: [],
			options: [],
		};
	}

	get = () => {
		wp.apiFetch({
			path: `/gecko/v1/icons/`,
		}).then(data => {
			const options = data.map((icon)=>{
				return {'value': icon.id, 'label': icon.name}
			})
			this.setState({
				options: options,
			})
		}).catch(error => {
			console.error(error);
		});
	}

	componentDidMount(){
		this.get();
	}

	// https://hackernoon.com/replacing-componentwillreceiveprops-with-getderivedstatefromprops-c3956f7ce607
	componentDidUpdate(prevProps, prevState) {
		if (prevState.defaultValue !== this.state.defaultValue) {
			this.setState({
				value: null,
			});
		}
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.defaultValue !== prevState.defaultValue) {
			return { defaultValue: nextProps.defaultValue};
		}
		else return null;
	}

	updateState = (element) => {
		if (typeof this.props.onChange === 'function') {
			this.props.onChange(element.value);
		}
	}

	// Render the component.
	render() {
		const { options, value } = this.state;
		return (
			<div>
				<Select
					placeholder='Select an icon'
					options={options}
					name={this.props.name}
					onChange={this.updateState}
					value={value}
					isClearable={false}
				/>
			</div>
		);
	}
}

