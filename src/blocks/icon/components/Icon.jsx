import React from 'react';


export default class Icon extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			icon: '',
		}
	}

	get = () => {
		wp.apiFetch({
			path: `/gecko/v1/icons/${this.props.icon}/`,
		}).then(data => {
			this.setState({
				icon: data,
			})
		}).catch(error => {
			console.error(error);
		});
	}

	componentDidMount() {
		this.get();
	}

	componentDidUpdate(prevProps, prevState, snapshot){
		if(prevProps.icon != this.props.icon){
			this.get();
		}
	}

	render() {
		const {icon} = this.state;
		if (!this.props.icon) return (<span>Select an Icon</span>);
		if(!icon) return(<span>Loading {this.props.icon}</span>);
		return (
			<div className={`gecko-svg is-align-${this.props.align} is-size-${this.props.size}`} dangerouslySetInnerHTML={{__html: icon.svg}} />
		);
	}
}