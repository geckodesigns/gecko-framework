/**
 * WordPress dependencies
 */
import { Fragment, Component } from '@wordpress/element';
import { RichTextToolbarButton } from '@wordpress/block-editor';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFortAwesomeAlt } from '@fortawesome/free-brands-svg-icons';
// import InlineIconUI from './inline';
import { insertObject } from '@wordpress/rich-text';

const name = 'gecko/icon';

export const IconFormatType = {
	name,
	title: 'FontAwesome Icon',
	keywords: ['icon'],
	// object: true,
	tagName: 'icon',
	className: null,
	attributes: {
		className: 'class',
	},
	edit: class ImageEdit extends Component {
		constructor() {
			super(...arguments);
			this.openModal = this.openModal.bind(this);
			this.closeModal = this.closeModal.bind(this);
			this.state = {
				modal: false,
			};
		}

		openModal() {
			this.setState({ modal: true });
		}

		closeModal() {
			this.setState({ modal: false });
		}

		render() {
			const { value } = this.props;
			const { isActive, activeAttributes, onChange } = this.props;
			return (
				<Fragment>
					<RichTextToolbarButton
						name={name}
						icon={<FontAwesomeIcon icon={faFortAwesomeAlt} />}
						title={'Inline Icon'}
						onClick={() => {
							// onChange(
							// 	insertObject(value, {
							// 		type: name,
							// 		attributes: {
							// 			className: `fab fa-fort-awesome-alt`,
							// 		},
							// 	})
							// );
						}}
					/>
					{/* <div>TEst</div> */}
					{/* <InlineIconUI
						isActive={isActive}
						activeAttributes={activeAttributes}
						value={value}
						onChange={onChange}
					/> */}
				</Fragment>
			);
		}
	},
};
