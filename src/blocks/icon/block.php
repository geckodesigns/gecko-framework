<?php
/**
 * Grid Image Callback
 */
$render = function ( $attributes, $content ) {
	// Defaults and attributes
	// Setting to false unless needed because all styles do not need to be inlined
	$defaults = array(
		'icon' => false,
		'align' => 'none',
		'size' => 'normal',
	);
	// Add a filter to hook into the default args
	// $defaults = apply_filters( 'gecko/svg/defaults', $defaults, $attributes );
	$atts = wp_parse_args( $attributes, $defaults );

	if($atts['icon']){
		$icon = get_post($atts['icon']);
		if($icon){
			$svg = $icon->post_content;
		}
	}
	return '<div class="gecko-svg is-align-'.$atts['align'].' is-size-'.$atts['size'].'">'.$svg.'</div>';
};