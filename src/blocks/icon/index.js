import { IconFormatType } from './format-type';
import { registerFormatType } from '@wordpress/rich-text';
registerFormatType('gecko/icon', IconFormatType);
/**
 * WordPress dependencies
 */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faIcons } from '@fortawesome/free-solid-svg-icons';
import { __ } from '@wordpress/i18n';
import {
	registerBlockType
} from '@wordpress/blocks';
import {
	PanelBody,
	SelectControl,
} from '@wordpress/components';
import {
	InnerBlocks,
	InspectorControls,
	BlockControls,
	BlockAlignmentToolbar,
} from '@wordpress/editor';
import SelectIcon from './components/SelectIcon';
import Icon from './components/Icon';

/**
 * Allowed blocks constant is passed to InnerBlocks precisely as specified here.
 * The contents of the array should never change.
 * The array should contain the name of each block that is allowed.
 * In columns block, the only block we allow is 'dmp/grid-item'.
 *
 * @constant
 * @type {string[]}
*/
const name = 'gecko/icon';
const settings = {
	title: __( 'Icon' ),
	icon: <FontAwesomeIcon icon={faIcons} />,
	category: 'layout',
	description: __( 'Icon Block.' ),
	supports: {
		html: false,
	},
	deprecated: [],
	attributes: {
		icon: { type: 'int'},
		align: {type: 'string'},
		size: {type: 'string'},
		color: {type: 'string'}
	},

	edit: ({attributes, setAttributes, insertBlocksAfter, className}) => {
		const {icon, align, size} = attributes;
		return ([
			<InspectorControls>
				<PanelBody title={__("Settings")}>
					<SelectIcon 
						label="Icon"
						value={ icon }
						onChange = {(v) => { setAttributes({icon: v,})} }
					/>
				</PanelBody>
				<SelectControl
					label={__("Size")}
					value={ size }
					options={ [
						{ value: 'normal', label: 'Normal' },
						{ value: 'sm', label: 'Small' },
						{ value: 'lg', label: 'Large' },
						{ value: 'xl', label: 'X Large' },
					] }
					onChange = {
						(size) => {
							setAttributes({
								size: size,
							})
						}
					}
				/>
			</InspectorControls>,
			<BlockControls>
				<BlockAlignmentToolbar
					value={ align }
					onChange={(v) => { setAttributes({align: v,})} }
				/>
			</BlockControls>,
			<Icon icon={icon}  size={size} align={align}/>
		]);
	},

	save: () => {
		return null
	}
};

registerBlockType(name, settings);