import ReactDom from 'react-dom';
import Events from './Events';
document.addEventListener("DOMContentLoaded", (event) => {
	const elements = document.getElementsByClassName('gecko-facebook-event-cards');
	[...elements].forEach(element => {
		const attributes = JSON.parse(element.getAttribute('data-props'));
		ReactDom.render(<Events {...attributes} />, element);
	});
});