import React from 'react';
import apiFetch from '@wordpress/api-fetch';
import Event from './Event';

export default class Assets extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			events: []
		}
	}

	get = () => {
		apiFetch({
			path: `/gecko/v1/facebook/events`
		}).then(data => {
			this.setState({
				events: data,
			})
		}).catch(error => {
			console.error(error);
		});
	}

	componentDidMount() {
		this.get();
	}

	render() {
		// console.log(this.props);
		const { events} = this.state;
		// const {show} = this.props;
		const show = 12;
		let showing = 0;
		return(
			<div className={`gecko-facebook-events`} >
				{events.map((event, key) => {
					showing++;
					if(show && showing > show) return;
					return (<Event key={key} event={event}/>)
				})}
			</div>
		);
	}
}