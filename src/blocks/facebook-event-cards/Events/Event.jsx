import React from 'react';

export default class Event extends React.Component {

	render() {
		const { event } = this.props;
		return(
			<figure className={`gecko-facebook-events__event`}>
				<div className={`gecko-facebook-events__cover`}>
					<img src={event.cover.source} />
				</div>
				<h3 className={`gecko-facebook-events__name`}>{event.name}</h3>
				<div className={`gecko-facebook-events__date`}>{event.formated_start_date}</div>
				{/* <figcaption className={`gecko-facebook-events__description`} dangerouslySetInnerHTML={{ __html: event.description }} /> */}
				<a className={`gecko-facebook-events__button button`} href={event.ticket_uri} target="_blank">Get Tickets</a>
			</figure>
		);
	}
}
