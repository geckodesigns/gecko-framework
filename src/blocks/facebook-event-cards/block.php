<?php
/**
	* If the block is dynamic you would render the template here.
	*/
$render = function ( $attributes, $content ) {
	$encoded = json_encode($attributes, JSON_HEX_APOS|JSON_HEX_QUOT);
	return sprintf('<div class="gecko-facebook-event-cards" data-props="%s">%s</div>',
	htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'), $content);
};