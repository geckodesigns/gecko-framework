/**
 * WordPress dependencies
 */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { __ } from '@wordpress/i18n';
import { registerBlockType} from '@wordpress/blocks';
import {
	PanelBody,
	SelectControl,
} from '@wordpress/components';
import {
	InnerBlocks,
	InspectorControls,
} from '@wordpress/editor';
import Events from './Events';

/**
 * Allowed blocks constant is passed to InnerBlocks precisely as specified here.
 * The contents of the array should never change.
 * The array should contain the name of each block that is allowed.
 * In columns block, the only block we allow is 'dmp/grid-item'.
 *
 * @constant
 * @type {string[]}
*/
const name = 'gecko/facebook-event-cards';
const settings = {
	title: __( 'Facebook Event Cards' ),
	icon: <FontAwesomeIcon icon={faCalendar} />,
	category: 'common',
	description: __( 'Facebook Event Cards for Gutenberg' ),
	supports: {
		html: false,
	},
	deprecated: [],
	attributes: {},

	edit: ({attributes, setAttributes, insertBlocksAfter, className}) => {
		// const {} = attributes;
		return ([
			<InspectorControls>
				<PanelBody title={__("Settings")}>

				</PanelBody>
			</InspectorControls>,
			<Events show={10}/>
		]);
	},

	save: () => {
		return null;
	}
};

registerBlockType(name, settings);