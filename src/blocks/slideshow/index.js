import { Fragment } from 'react';
import classnames from 'classnames';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImages } from '@fortawesome/free-solid-svg-icons';

import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { select } from '@wordpress/data';

import {
	InspectorControls,
	InnerBlocks,
} from '@wordpress/editor';

import {
	PanelBody,
	ToggleControl,
	SelectControl,
	TextControl,
} from '@wordpress/components';

import Preview from './components/Preview';

const ALLOWED_BLOCKS = [
	'gecko/slide-basic',
	'gecko/slide-video'
];

const settings = {
	title: 'Slideshow',
	icon: <FontAwesomeIcon icon={faImages} /> ,
	category: 'layout',
	description: 'Slideshow',

	supports: {
		customClassName: false
	},

	attributes: {
		// Swiper opts
		enablePagination: {
			type: 'boolean',
			default: false,
		},
		enableLoop: {
			type: 'boolean',
			default: false,
		},
		enableNavArrows: {
			type: 'boolean',
			default: false,
		},
		enableSwiping: {
			type: 'boolean',
			default: false,
		},

		enableAutoplay: {
			type: 'boolean',
			default: false,
		},
		autoplaySeconds: {
			type: 'string',
			default: '6',
		},

		effect: {
			type: 'string',
			default: 'slide',
		},

		fullHeight: {
			type: 'boolean',
			default: false,
		},
	},

	edit: (props) => {
		const {
			clientId,
			className,
		} = props;

		const {
			enableLoop,
			enablePagination,
			enableNavArrows,
			enableSwiping,

			enableAutoplay,
			autoplaySeconds,

			effect,

			fullHeight,
		} = props.attributes;

		const wrapperClasses = classnames(
			className,
			{ [`${className}--full-height`]: fullHeight }
		)

		const parentBlock = select('core/editor').getBlocksByClientId(clientId)[0];
		const childBlocks = parentBlock.innerBlocks;

		return ([
			<InspectorControls>
				<PanelBody title="Slideshow Options">
					<ToggleControl
						label="Loop Slides"
						checked={enableLoop}
						onChange={(enableLoop) => { props.setAttributes({ enableLoop }) }}
					/>

					<ToggleControl
						label="Enable Swiping"
						checked={enableSwiping}
						onChange={(enableSwiping) => { props.setAttributes({ enableSwiping }) }}
					/>

					<ToggleControl
						label="Enable Pagination Dots"
						checked={enablePagination}
						onChange={(enablePagination) => { props.setAttributes({ enablePagination }) }}
					/>

					<ToggleControl
						label="Enable Navigation Arrows"
						checked={enableNavArrows}
						onChange={(enableNavArrows) => { props.setAttributes({ enableNavArrows }) }}
					/>

					<ToggleControl
						label="Enable Autoplay"
						checked={enableAutoplay}
						onChange={(enableAutoplay) => { props.setAttributes({ enableAutoplay }) }}
					/>

					{enableAutoplay &&
						<TextControl
							type="number"
							label="Autoplay Seconds"
							value={autoplaySeconds}
							onChange={autoplaySeconds => { props.setAttributes({autoplaySeconds}) }}
						/>
					}

					<SelectControl
						label="Effect"
						value={effect}
						options={[
							{label: 'Slide', value: 'slide'},
							{label: 'Fade', value: 'fade'},
							{label: 'Cube', value: 'cube'},
							{label: 'Coverflow', value: 'coverflow'},
							{label: 'Flip', value: 'flip'},
						]}
						onChange={effect => { props.setAttributes({effect}) }}
					/>

				</PanelBody>

				<PanelBody title="Layout Settings">
					<ToggleControl
						label="Full Height"
						checked={fullHeight}
						onChange={(fullHeight) => { props.setAttributes({ fullHeight }) }}
					/>
				</PanelBody>
			</InspectorControls>,

			<Fragment>
				{childBlocks &&
					<Preview
						childBlocks={childBlocks}
						enableLoop={enableLoop}
						enableSwiping={enableSwiping}
						enablePagination={enablePagination}
						enableNavArrows={enableNavArrows}
						enableAutoplay={enableAutoplay}
						autoplaySeconds={autoplaySeconds}
						effect={effect}
					/>
				}

				<div className={wrapperClasses}>
					<InnerBlocks
						allowedBlocks={ALLOWED_BLOCKS}
						template={[
							['gecko/slide-basic']
						]}
					/>
				</div>
			</Fragment>
		]);
	},

	save(props) {
		return(<InnerBlocks.Content />);
	}
}

registerBlockType('gecko/slideshow', settings);