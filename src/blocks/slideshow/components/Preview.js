import React from 'react';
import Swiper from 'react-id-swiper';
import classnames from 'classnames';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

export default class Preview extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			selected: null,
		}
	}

	buildPreviewSlides = (childBlocks) => {
		let previewSlides = [];
		childBlocks.forEach(block => {
			if (block.name === 'gecko/slide-basic') {
				if (block.attributes) {
					previewSlides.push({
						bgColor: block.attributes.bgColor,
						bgOverlayColor: block.attributes.bgOverlayColor,
						bgOverlayOpacity: block.attributes.bgOverlayOpacity,
						backgroundImage: block.attributes.bgImageUrl,
						addContrast: block.attributes.addContrast,
						verticalAlign: block.attributes.verticalAlign,
					});
				}
			} else if (block.name === 'gecko/slide-video') {
				if (block.attributes) {
					previewSlides.push({
						bgColor: block.attributes.bgColor,
						bgOverlayColor: block.attributes.bgOverlayColor,
						bgOverlayOpacity: block.attributes.bgOverlayOpacity,
						addContrast: block.attributes.addContrast,
						verticalAlign: block.attributes.verticalAlign,
					});
				}
			}
		});

		return previewSlides;
	}

	render() {
		const {
			childBlocks,

			enableLoop,
			enableSwiping,
			enablePagination,
			enableNavArrows,
			enableAutoplay,
			autoplaySeconds,
			effect,
		} = this.props;

		const previewSlides = this.buildPreviewSlides(childBlocks);

		const swiperParams = {
			rebuildOnUpdate: true,
			shouldSwiperUpdate: true,

			grabCursor: true,
			spaceBetween: 0,
			slidesPerColumn: 1,
			setWrapperSize: false,

			touchRatio: 0.8,
			threshold: 16,

			loop: enableLoop,

			simulateTouch: enableSwiping,
		}

		const paginationClassName = classnames(
			'swiper-pagination',
			{'swiper-pagination--nav-arrows': enableNavArrows}
		);
		if (enablePagination) {
			swiperParams.pagination = {
				el: '.' + paginationClassName.replace(' ', '.'),
				clickable: true,
			}
		}

		if (enableNavArrows) {
			swiperParams.navigation = {
				prevEl: '.swiper-button-prev',
				nextEl: '.swiper-button-next',
			}
			swiperParams.renderPrevButton = () => {
				return <div className="swiper-button-prev"><FontAwesomeIcon icon={faChevronLeft} /></div>
			}
			swiperParams.renderNextButton = () => {
				return <div className="swiper-button-next"><FontAwesomeIcon icon={faChevronRight} /></div>
			}

		}

		if (effect) {
			swiperParams.effect = effect;
		}

		if (enableAutoplay) {
			swiperParams.autoplay = {
				delay: parseInt(autoplaySeconds) * 1000,
			}
		}

		return (
			<div className="gecko-slideshow-preview">
				<header>
					Slideshow Preview:
				</header>
				<Swiper
					{...swiperParams}
				>
					{previewSlides.map(slide => {
						const bgOverlayOpacityClass = 'gecko-slideshow-preview__slide--opacity-' + slide.bgOverlayOpacity;
						const slideClassName = classnames(
							'gecko-slideshow-preview__slide',
							[`has-${slide.bgColor}-bg-color`],
							[`has-${slide.bgOverlayColor}-bg-overlay-color`],
							{ [`${bgOverlayOpacityClass}`]: (slide.bgOverlayOpacity >= 0) },
							{ 'gecko-slideshow-preview__slide--pagination': enablePagination },
							{ 'gecko-slideshow-preview__slide--nav-arrows': enableNavArrows }
						);

						return(
							<div
								className={slideClassName}
								data-vertical-align={slide.verticalAlign}
							>
								<div className="gecko-slideshow-preview__image" style={{ backgroundImage: `url('${slide.backgroundImage}')`, }} />
								<div className="gecko-slideshow-preview__content" data-add-contrast={(slide.addContrast) ? 'true' : ''} />
							</div>
						)
					})}
				</Swiper>
			</div>
		)
	}
}