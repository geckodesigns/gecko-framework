import Swiper from 'swiper';

document.addEventListener("DOMContentLoaded", (event) => {
    const elements = document.getElementsByClassName('gecko-slideshow');
    [...elements].forEach(element => {
        const attributes = JSON.parse(element.getAttribute('data-props'));

        const {
            enableAutoplay,
            autoplaySeconds,

            enableSwiping,
            enableLoop,
            enableNavArrows,
            enablePagination,

            effect,
        } = attributes;

        let swiperParams = {
            grabCursor: true,
            spaceBetween: 0,
            slidesPerColumn: 1,
            setWrapperSize: false,

            touchRatio: 0.8,
            threshold: 16,

            loop: enableLoop,
            simulateTouch: enableSwiping,
        };

        if (enablePagination) {
            swiperParams.pagination = {
                el: '.swiper-pagination',
                clickable: true,
            }
        }

        if (enableNavArrows) {
            swiperParams.navigation = {
                prevEl: '.swiper-button-prev',
                nextEl: '.swiper-button-next',
            }
        }

        if (effect) {
            swiperParams.effect = effect;
        }

        if (enableAutoplay) {
            const delaySeconds = (autoplaySeconds) ? autoplaySeconds : 6;
            swiperParams.autoplay = {
                delay: parseInt(delaySeconds) * 1000,
            }
        }

        const slideshowSwiper = new Swiper(element, swiperParams);
    });
});