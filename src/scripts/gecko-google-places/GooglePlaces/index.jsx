import React from 'react';

export default class GeckoFramework extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			places: [],
		}
	}
	componentDidMount(){
		const { places } = this.props;
		this.setState({
			places
		})
	}


	add = () => {
		const { places } = this.state;
		const newPlaces = JSON.parse(JSON.stringify(places));
		newPlaces.push('');
		this.setState({
			places: newPlaces
		})
	}

	delete = (key) => {
		if (!Number.isInteger(key)) return;
		const { places } = this.state;
		const newPlaces = JSON.parse(JSON.stringify(places));
		newPlaces.splice(key, 1);
		this.setState({
			places: newPlaces
		})
	}
	render(){
		const {places} = this.state;
		return(
			<table>
				<tbody>
					{places.map((place,key)=>{
						return(
							<tr key={key}>
								<td>
									<input name="google_places[]" defaultValue={place} />
								</td>
								<td>
									<p onClick={() => this.delete(key)}>X</p>
								</td>
							</tr>
						)
					})}
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td><p onClick={this.add}>Add Place</p></td>
					</tr>
				</tfoot>
			</table>
		);
	}
}