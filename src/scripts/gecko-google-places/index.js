/**
 * This is the entry point for gecko-framework admin page.
 */
import ReactDom from 'react-dom';
import GooglePlaces from './GooglePlaces';
/**
 * Render elements on load if they are visible
 */
const RenderBlock = (selector, Component) => {
	const elements = document.querySelectorAll(selector);
	[...elements].forEach(element => {
		const props = JSON.parse(element.getAttribute('data-props'));
		return ReactDom.render( <Component {...props}/>, element);
	});
};
/**
 * Render elements on load if they are visible
 */
document.addEventListener("DOMContentLoaded", (event) => {
	RenderBlock('#gecko-google-places', GooglePlaces);
});