/**
 * Constansts are used by actions and reducers
 * @module constants
 */
export const constants = {
	loading: 'LOADED',
	app: 'APP',
	searching: 'SEARCHING',
};
