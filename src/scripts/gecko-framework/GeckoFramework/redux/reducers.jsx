import { constants } from './constants';
import { combineReducers } from 'redux';

function app(state = 'v0.0.1', action) {
	switch (action.type) {
	// Doc Models
	case constants.app:
		return action.data;
	default:
		return state;
	}
}
function searching(state = false, action) {
	switch (action.type) {
	// Doc Models
	case constants.searching:
		return action.data;
	default:
		return state;
	}
}
export function loading(state = true, action) {
	switch (action.type) {
		// On Success
		case constants.loading:
			return action.loading;
		default:
			return state;
	}
}

export const Reducers = combineReducers({
	app,
	loading,
	searching,
});