import { constants } from './constants';

/**
 * Docs Actions.
 * @namespace product
 * @memberof module:actions
 */
export const actions = {
	loading: loading => {return{ type: constants.loading, loading: false}},
	searching: data => {return{ type: constants.searching, data}},
	app: data => {return{ type: constants.app, data}},
}
