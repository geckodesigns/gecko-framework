import React from 'react';
import { Provider } from 'react-redux';
import { store } from './redux/store';


export default class GeckoFramework extends React.Component {
	render(){
		return(
			<Provider store={store}>
				<div>This is the GeckoFramework admin page. There's nothing here yet but it's coming.</div>
			</Provider>
		);
	}
}