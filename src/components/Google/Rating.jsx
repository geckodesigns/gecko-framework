import React from 'react';

export default class Rating extends React.Component {
	render() {
		const { rating } = this.props;
		return (
			<div className={`gecko-google-places-ratings`}>
				{rating}
			</div>
		);
	}
}
