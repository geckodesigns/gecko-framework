import React from 'react';
import ReactQuill from 'react-quill';

export default class Editor extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			html: '',
		}
	}

	handleChange = (content, delta, source, editor) => {
		const {onChange, name} = this.props;
		this.setState({ html: content })
		const e = {
			target: {
				name: name,
				value: (editor.getLength() > 1)? content : '',
			}
		}
		if(typeof onChange === 'function'){
			onChange(e)
		}
	}

	componentDidMount(){
		const { value } = this.props;
		if(value){
			this.setState({
				html: value,
			})
		}
	}

	render() {
		const {html} = this.state;
		const {value, formats, modules} = this.props;
		return (
			<div className="cfg-editor">
				<ReactQuill 
					defaultValue={value}
					formats={formats}
					modules={modules}
					value={html}
					onChange={this.handleChange}
				/>
			</div>
		)
	}
}