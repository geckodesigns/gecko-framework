'use strict';
/**
 * Mobile menu
 */
const toggle = document.querySelectorAll('.header-touch__toggle')[0];
const menu = document.querySelectorAll('.touch-menu')[0];
const blackOut = document.createElement('div');
blackOut.setAttribute('data-blackout', 'hidden');
document.body.prepend(blackOut);
const toggleAction = () => {
	if (toggle.classList.contains('active')) {
		document.body.style.overflow = null;
		blackOut.setAttribute('data-blackout', 'hidden');
		toggle.classList.remove('active');
		menu.classList.remove("touch-menu--show");
	} else {
		document.body.style.overflow = 'hidden';
		blackOut.setAttribute('data-blackout', 'show');
		toggle.classList.add('active');
		menu.classList.add("touch-menu--show");
	}
}
blackOut.onclick = () => toggleAction();
toggle.onclick = (e) => {
	e.preventDefault;
	toggleAction();
}

/**
 * Menu Stuff
 */
const menuItemWithChildren = document.querySelectorAll('.touch-menu .menu > .menu-item-has-children > a');
for (let i = 0; i < menuItemWithChildren.length; ++i) {
	const mobileAction = document.createElement('span');
	mobileAction.setAttribute('data-mobile-action', 'hide')
	mobileAction.onclick = (e) => {
		e.preventDefault();
		const parent = mobileAction.parentNode.parentNode;
		if (parent.classList.contains('show-sub-menu')) {
			parent.classList.remove('show-sub-menu');
			mobileAction.setAttribute('data-mobile-action', 'hide')
		} else {
			parent.classList.add('show-sub-menu');
			mobileAction.setAttribute('data-mobile-action', 'show')
		}
	}
	menuItemWithChildren[i].append(mobileAction);
}
