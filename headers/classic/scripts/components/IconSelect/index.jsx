import React from 'react';


export default class IconSelect extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			editMode: false,
			icon: '',
			icons: [],
			style: '',
			iconId: null,
		}
	}

	save = () => {
		wp.apiFetch({
			path: `/gecko/v1/menu/${this.props.id}`,
			method: 'POST',
			data: {
				'icon': this.state.iconId,
				'style': this.state.style,
			}
		}).then(data => {
			console.log('success', data);
		}).catch(error => {
			console.error(error);
		});
	}

	get = () => {
		wp.apiFetch({
			path: `/gecko/v1/icons/`,
		}).then(data => {
			this.setState({
				icons: data,
			})
		}).catch(error => {
			console.error(error);
		});
	}

	editMode = () => {
		if (this.state.editMode){
			this.save();
		}
		this.setState({
			editMode: (this.state.editMode)? false: true,
		})
	}

	changeStyle = () => {
		this.setState({
			style: (this.state.style === 'outline') ? '' : 'outline',
		}) 
	}
	changeIcon = (e) => {
		const icon = this.state.icons.filter(icon => icon.id === parseInt(e.target.value))[0];
		if(icon){
			this.setState({
				icon: icon.svg,
				iconId: icon.id,
			})
		}else{
			this.setState({
				icon: '',
				iconId: '',
			})
		}
	}

	componentDidMount(){
		console.log(this.props);
		this.setState({
			icon: this.props.icon,
			iconId: this.props.iconId,
			style: this.props.style,
		})
		this.get();
	}

	render(){
		const editStyles = {
				position:'absolute',
				top: 0,
				right: 0,
				padding: '3px',
				fontSize: '12px',
				cursor: 'pointer'
			};
		const styleStyles = {
				position:'absolute',
				top: 0,
				left: 0,
				padding: '3px',
				fontSize: '12px',
				cursor: 'pointer'
			};
		const iconStyles = {
				position:'absolute',
				top: '100%',
				left: 0,
				padding: '3px',
				fontSize: '12px',
				cursor: 'pointer',
				backgroundColor: 'white',
			};
		return(
			<div className={`menu-item__link ${this.state.style}`}>
				<div style={editStyles} onClick={this.editMode}>
					{!this.state.editMode &&
						<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"></path></svg>
					}
					{this.state.editMode &&
						<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z"></path></svg>
					}
				</div>
				{this.state.editMode &&
					<div style={styleStyles} onClick={this.changeStyle}>
						<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><g><path fill="currentColor" d="M512 49.55c0 16.14-6.52 31.64-13.9 46C385.06 306.53 349.06 352 287 352a92 92 0 0 1-22.39-3l-63.82-53.18a92.58 92.58 0 0 1-8.73-38.7c0-53.75 21.27-58 225.68-240.64C428.53 6.71 442.74 0 457.9 0 486 0 512 20.64 512 49.55z" opacity="0.4"></path><path fill="currentColor" d="M255 382.68a86.64 86.64 0 0 1 1 9.13C256 468.23 203.87 512 128 512 37.94 512 0 439.62 0 357.27c9.79 6.68 44.14 34.35 55.25 34.35a15.26 15.26 0 0 0 14.59-10c20.66-54.44 57.07-69.72 97.19-72.3z"></path></g></svg>
					</div>
				}
				<span dangerouslySetInnerHTML={{ __html: this.state.icon}} />
				{this.state.editMode && 
					<div style={iconStyles}>
						<select onChange={this.changeIcon} defaultValue={this.state.iconId}>
							<option value=''>No Icon</option>
							{this.state.icons.map((icon, key) => {
								return (<option key={key} value={icon.id} >{icon.name}</option>)
							})}
						</select>
					</div>
				}
				{this.props.title}
			</div>
		);
	}
}