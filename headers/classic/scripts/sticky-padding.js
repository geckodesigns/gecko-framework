/**
 * Fixed header padding and auto-resize
 */
const stickyPadding = document.querySelectorAll('[data-header-sticky-padding]');
if (stickyPadding.length) {
	const fixedHeaderPadding = document.createElement('div');
	fixedHeaderPadding.id = 'header-sticky-padding';
	document.body.prepend(fixedHeaderPadding);
	const setFixedHeaderHeight = () => {
		const fixedHeader = document.querySelectorAll('[data-header-sticky]');
		for (let i = 0; i < fixedHeader.length; ++i) {
			if (fixedHeader[i].offsetWidth > 0 && fixedHeader[i].offsetHeight > 0) {
				fixedHeaderPadding.style.height = `${fixedHeader[i].offsetHeight}px`;
			}
		}
	}
	document.addEventListener("DOMContentLoaded", setFixedHeaderHeight);
	window.addEventListener("resize", setFixedHeaderHeight);
	window.addEventListener("orientationchange", setFixedHeaderHeight);
}