/**
 * Auto Hide Script
 */
const search = document.querySelectorAll('.header-desktop__search');
if (search.length) {
	const toggleSearch = () => {
		for (let i = 0; i < search.length; ++i) {
			const outsideClickListener = (e) => {
				if (!search[i].contains(e.target)) {
					search[i].classList.remove('reveal');
					document.removeEventListener('click', outsideClickListener);
				}
			}
			const toggle = search[i].querySelector('.header-desktop__search-toggle')
			toggle.onclick = (e) => {
				if (search[i].classList.contains('reveal')) {
					search[i].classList.remove('reveal');
					document.removeEventListener('click', outsideClickListener);
				}else{
					search[i].classList.add('reveal');
					document.addEventListener('click', outsideClickListener);
				}
			}
		}
	}
	document.addEventListener('DOMContentLoaded', toggleSearch);
}