/*
 * Drop down menu
 * Helps make sure drop downs stay on the screen
 */
const menuItems = document.querySelectorAll('.header-desktop__navigation .menu-item.menu-item-has-children');
for (let i = 0; i < menuItems.length; ++i) {
	menuItems[i].onmouseenter = (e) => {
		const item = e.target;
		const submenu = item.querySelectorAll('.sub-menu')[0];
		const box = submenu.getBoundingClientRect();
		var elRight = box.x + submenu.offsetWidth;
		var windowWidth = window.outerWidth;
		var isEntirelyVisible = (elRight <= windowWidth);
		if (!isEntirelyVisible) {
			submenu.classList.add('offscreen');
		}
	}
}
