/**
 * Auto Hide Script
 */
const autoHide = document.querySelectorAll('[data-header-auto-hide]');
if (autoHide.length) {
	let previousPosition = window.pageYOffset || document.documentElement.scrollTop;
	const headerAutoHide = () => {
		const currentPosition = window.pageYOffset || document.documentElement.scrollTop;
		for (let i = 0; i < autoHide.length; ++i) {
			if (previousPosition < currentPosition && currentPosition > autoHide[i].clientHeight) {
				autoHide[i].classList.add('header-auto-hide');
			}else{
				autoHide[i].classList.remove('header-auto-hide');
			}
		}
		previousPosition = currentPosition;
	}
	window.addEventListener("scroll", headerAutoHide);
}