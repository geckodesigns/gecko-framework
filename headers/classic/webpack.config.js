const production = (process.env.NODE_ENV === 'production') ? true : false;

const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const globImporter = require('node-sass-glob-importer');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const Fiber = require('fibers'); // Speeds up Dart Sass

// Wordpress already has these dependencies so if you need them make sure to include theme in the enqueue
const externals = {
  'react': 'React',
  'react-dom': 'ReactDOM',
  'tinymce': 'tinymce',
  'moment': 'moment',
  'jquery': 'jQuery',
  'lodash': 'lodash',
  'lodash-es': 'lodash',
  '@babel/polyfill': '@babel/polyfill',
};

/**
 * Given a string, returns a new string with dash separators converedd to
 * camel-case equivalent. This is not as aggressive as `_.camelCase` in
 * converting to uppercase, where Lodash will convert letters following
 * numbers.
 *
 * @param {string} string Input dash-delimited string.
 *
 * @return {string} Camel-cased string.
 */
function camelCaseDash(string) {
  return string.replace(
    /-([a-z])/g,
    (match, letter) => letter.toUpperCase()
  );
}

const gutenbergPackages = [
  'a11y',
  'api-fetch',
  'autop',
  'blob',
  'blocks',
  'block-library',
  'block-serialization-default-parser',
  'block-serialization-spec-parser',
  'components',
  'compose',
  'core-data',
  'data',
  'date',
  'deprecated',
  'dom',
  'dom-ready',
  'edit-post',
  'editor',
  'element',
  'escape-html',
  'format-library',
  'hooks',
  'html-entities',
  'i18n',
  'is-shallow-equal',
  'keycodes',
  'list-reusable-blocks',
  'nux',
  'plugins',
  'redux-routine',
  'rich-text',
  'shortcode',
  'token-list',
  'url',
  'viewport',
  'wordcount',
];
gutenbergPackages.forEach((name) => {
  externals[`@wordpress/${name}`] = `window.wp.${camelCaseDash(name)}`;
});

// The Webpack Config.
const settings = {
  context: __dirname,
  devtool: (!production) ? 'inline-sourcemap' : false,
  mode: (!production) ? 'development' : 'production',
  target: 'web',
  entry: {
    'scripts': path.resolve(__dirname, `./scripts/index.js`),
    'editor': path.resolve(__dirname, `./scripts/editor.js`),
    'style': path.resolve(__dirname, `./styles/style.scss`),
  },
  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
  },
  externals,
  output: {
    path: path.resolve(__dirname, `./dist`),
    // publicPath: `${theme_uri}`,
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },
  module: {
    rules: [{
        test: /\.(css|scss)$/,
        exclude: /node_modules/,
        use: [{
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: (!production) ? true : false,
              url: true,
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [require('autoprefixer')],
            }
          },
          {
            loader: 'sass-loader',
            options: {
              implementation: require('sass'),
              sassOptions: {
                sourceMap: (!production) ? true : false,
                fiber: Fiber,
                importer: globImporter()
              }
            },
          }
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        exclude: /node_modules/,
        use: [{
          loader: 'file-loader',
          options: {
            // publicPath: `${theme_uri}`,
            name: '[path][name].[ext]',
            emitFile: false,
          }
        }]
      },
      // Babel loader for es6 support
      {
        test: /\.(js|jsx)?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: [
            '@babel/preset-env',
            '@babel/preset-react',
          ],
          plugins: [
            '@wordpress/babel-plugin-import-jsx-pragma',
            '@babel/plugin-transform-runtime',
            '@babel/plugin-syntax-dynamic-import',
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-proposal-class-properties',
          ],
        },
      },
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ]
};

// Production only settings
if (production) {
  settings.plugins.push(new BundleAnalyzerPlugin());
  settings.optimization = {
    minimizer: [
      new TerserPlugin({
        parallel: true,
        extractComments: true,
        terserOptions: {
          mangle: true,
        }
      }),
      new OptimizeCSSAssetsPlugin({})
    ],
  };
}


module.exports = settings;
