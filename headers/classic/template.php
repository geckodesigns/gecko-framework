<?php
/**
 * Defaults
 */
// Get the URL of custom logo if exists
$sticky = get_option( 'gecko_framework_header_sticky', false );
$stickyPadding = get_option( 'gecko_framework_header_sticky_padding', false );
$autoHide = get_option( 'gecko_framework_header_auto_hide', false );
$search = get_option( 'gecko_framework_header_search', false );
// Custom Logo
$custom_logo_id = get_theme_mod( 'custom_logo' );
if($custom_logo_id):
	$custom_logo_id = get_theme_mod( 'custom_logo' ); // Get attachment ID
	$custom_logo_image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	$custom_logo_src = $custom_logo_image[0];	// Image src is the first array key
else:
	$custom_logo_src = GECKO_FRAMEWORK__PLUGIN_URL.'headers/classic/images/logo.png';
endif;
?>
<header 
	class="header-desktop"
	<?php if($sticky) echo 'data-header-sticky'; ?>
	<?php if($stickyPadding) echo 'data-header-sticky-padding'; ?>
	<?php if($autoHide) echo 'data-header-auto-hide'; ?>
	>
	<div class="header-desktop__logo">
		<a href="<?= home_url("/"); ?>" class="logo" title="<?= get_bloginfo( 'name' ) ?>">
			<img src="<?= $custom_logo_src; ?>" alt="<?= get_bloginfo( 'name' ) ?>"/>
		</a>
	</div>
	<div class="header-desktop__main">
		<?php
		if ( has_nav_menu( 'primary' ) ):
			wp_nav_menu([
				"container" => "nav",
				"container_class" => "menu-primary",
				"theme_location" => "primary",
				"depth" => 2,
			]); 
		endif;
		if ( has_nav_menu( 'secondary' ) ):
			wp_nav_menu([
				"container" => "nav",
				"container_class" => "menu-secondary",
				"theme_location" => "secondary",
				"depth" => 1,
				"walker" => new Gecko_Button_Menu_Walker(),
			]); 
		endif;
		?>
		<?php if($search):?>
		<form class="header-desktop__search" action="<?= home_url("/"); ?>">
			<label class="header-desktop__search-toggle" for="s" class="header-desktop__search-toggle" title="Show Search Bar"><svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg></label>
			<div class="header-desktop__search-form">
				<input type="text" id="s" name="s" placeholder="Search...">
			</div>
		</form>
		<?php endif;?>
	</div>
</header>
<?php // Touch Header ?>
<header 
	class="header-touch" 
	<?php if($sticky) echo 'data-header-sticky'; ?>
	<?php if($stickyPadding) echo 'data-header-sticky-padding'; ?>
	<?php if($autoHide) echo 'data-header-auto-hide'; ?>
	>
	<div class="gecko-header-touch__logo">
		<a href="<?= home_url("/"); ?>" class="logo" title="<?= get_bloginfo( 'name' ) ?>">
			<img src="<?= $custom_logo_src; ?>" alt="<?= get_bloginfo( 'name' ) ?>"/>
		</a>
	</div>
	<button class="header-touch__toggle" type="button">
		<span class="header-touch__toggle-box">
			<span class="header-touch__toggle-inner"></span>
		</span>
	</button>
</header>
<?php // Touch Menu ?>
<aside class="touch-menu">
	<?php 
	if ( has_nav_menu( 'primary' ) ):
		wp_nav_menu([
			"container" => "nav",
			"container_class" => "touch-menu__navigation menu-primary",
			"theme_location" => "primary",
		]); 
	endif;
	?>
	<?php if($search):?>
	<?php endif;?>
</aside>