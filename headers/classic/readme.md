Headers are essentially self contained apps.

You need a few files for them to work:

* functions.php - this is loaded on init, so do all your actions and things here.
* template.php - this is the view