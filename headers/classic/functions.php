<?php

/*
 * Theme Support
 * It is generally used to perform basic setup, registration, and init actions for a theme.
 * https://codex.wordpress.org/Plugin_API/Action_Reference/after_setup_theme
 */
add_action('after_setup_theme', function () {
	// Custom Logo in customizer
	add_theme_support( 'custom-logo', [
		'height'      => 200,
		'width'       => 400,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	]);
	// Register Nav Menus
	register_nav_menus([
		'primary' => 'Primary',
		'secondary' => 'Secondary',
	]);
});


/*
 * Header Enqueue Scripts
 * https://developer.wordpress.org/reference/functions/wp_enqueue_script/
 * https://developer.wordpress.org/reference/functions/wp_enqueue_style/
 * But load it before the theme scripts
 */
add_action('wp_enqueue_scripts', function () {
	$header = 'classic';
	wp_register_script(
		'header-'.$header, // Name
		GECKO_FRAMEWORK__PLUGIN_URL.'headers/'.$header.'/dist/scripts.bundle.js', // File
		['wp-polyfill'], // Deps
		filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . 'headers/'.$header.'/dist/scripts.bundle.js' ), // Version
		true // Footer
	);
	wp_register_script(
		'menu-icon-select', // Name
		GECKO_FRAMEWORK__PLUGIN_URL.'headers/'.$header.'/dist/editor.bundle.js', // File
		['react','react-dom','wp-polyfill','wp-api-fetch'], // Deps
		filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . 'headers/'.$header.'/dist/editor.bundle.js' ), // Version
		true // Footer
	);
	wp_enqueue_script( 'header-'.$header);
	wp_register_style(
		'header-'.$header,
		GECKO_FRAMEWORK__PLUGIN_URL.'headers/'.$header.'/dist/style.css',
		[],
		filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . 'headers/'.$header.'/dist/style.css' )
	);
	wp_enqueue_style( 'header-'.$header);
}, 9);


class Gecko_Button_Menu_Walker extends Walker_Nav_Menu {
		
	// Displays start of an element. E.g '<li> Item Name'
	// @see Walker::start_el()
	function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
		$object = $item->object;
		$type = $item->type;
		$title = $item->title;
		$permalink = $item->url;
		$output .= "<li class='" .  implode(" ", $item->classes) . "'>";
		$iconID = get_post_meta( $item->ID, 'icon', true);
		$style = get_post_meta( $item->ID, 'style', true);
		$svg;
		if($iconID){
			$icon = get_post($iconID);
			if($icon){
				$svg = $icon->post_content;
			}
		}
		if(current_user_can('administrator') && is_customize_preview()){
			$props = [
				'id' => $item->ID,
				'icon' => $svg,
				'iconId' => $iconID,
				'title' => $title,
				'style' => $style,
			];
			$encoded = json_encode($props, JSON_HEX_APOS|JSON_HEX_QUOT);
			wp_enqueue_script('menu-icon-select');
			$output .= "<span class='menu-icon-select' data-props='$encoded'>Loading</span>";
		}else{
			if( $permalink && $permalink != '#' ) {
				$output .= '<a class="menu-item__link '.$style.'" href="' . $permalink . '">';
			} else {
				$output .= '<span>';
			}
			$output .= $svg;
			$output .= $title;
			if( $permalink && $permalink != '#' ) {
				$output .= '</a>';
			} else {
				$output .= '</span>';
			}
		}
	}
}



if(current_user_can('administrator')){
	add_action( 'rest_api_init','header_rest_api_init');
}

function header_rest_api_init() {
	register_rest_route( 'gecko/v1', '/menu/(?P<id>\d+)', [
		'methods' => 'POST',
		'callback' => 'header_menu_meta',
	]);
}

function header_menu_meta($request){
	$params = $request->get_params();
	$data = json_decode($request->get_body(), true);
	// $info = [];
	$id = $params['id'];
	$icon = update_post_meta( $id, 'icon', $data['icon'] );
	$style = update_post_meta( $id, 'style', $data['style'] );
	return ['id'=> $id, 'icon' => $icon, 'style' => $style, 'data' => $data];
}
