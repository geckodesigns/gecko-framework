<?php
namespace GeckoFramework;

class Settings {
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'pages' ), 5 );
	}
	
	public function pages(){
		add_menu_page( 
			'Gecko Framework',
			'Gecko Framework',
			'manage_options',
			'gecko-framework',
			function(){
				wp_register_script(
					'gecko-framework',
					GECKO_FRAMEWORK__PLUGIN_URL.'/build/scripts/gecko-framework.js',
					['react','react-dom']
				);
				wp_enqueue_script('gecko-framework');
				//enqueue scripts and use react
				echo '<div id="gecko-framework">Loading</div>';
			},
			GECKO_FRAMEWORK__PLUGIN_URL.'/assets/images/icon.svg',
			60
		);
	}

}