<?php
namespace GeckoFramework;

class Init {
	public function __construct() {
		// new Settings(); // Future Feature
		new Blocks();
		new Customizer();
		new Google\Init(); // Future Feature
		new Facebook\Init();
		new Icons\Init();
		new Api\Login();
		new Api\Instagram();
		new Api\Facebook();
		new Api\Icons();
		new ThemeParts();
		new ThemeHeader();
		add_action("init", [$this, "init"], 0);
		add_action("init", [$this, "register_scripts"], 0);
		add_image_size( 'lazy-load', 100, 100 );
	}

	public function init(){
		/*
		 * Install Updater
		 */
		if(is_admin() && current_user_can('administrator')){
			new Updater(GECKO_FRAMEWORK__FILE, 'geckodesigns', 'gecko-framework');
		}
	}

	public function register_scripts(){
		$scripts = [
			'object-fit-polyfill' => [],
			'lazy-load' => [],
		];
		foreach($scripts as $script => $deps){
			$this->register_script($script, $deps);
		}
	}

	private function register_script($slug, $deps =[]){
		if(file_exists (GECKO_FRAMEWORK__PLUGIN_DIR .'build/scripts/'.$slug.'.js')){
			wp_register_script(
				$slug,
				GECKO_FRAMEWORK__PLUGIN_URL.'build/scripts/'.$slug.'.js',
				$deps,
				filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . 'build/scripts/'.$slug.'.js' )
			);
		}
	}
}