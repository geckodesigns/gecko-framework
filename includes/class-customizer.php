<?php
namespace GeckoFramework;
use RecursiveDirectoryIterator;


class Customizer {
	public $pre = 'gecko_framework_';

	public function __construct() {
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_action( 'customize_register', [$this, 'customizer_register'] );
	}

	public function customizer_register( $wp_customize ) {

		$wp_customize->add_panel( $this->pre.'settings', array(
			'priority' => 21,
			'capability' => 'edit_theme_options',
			'theme_supports' => '',
			'title' => __( 'Framework Settings', 'gecko-theme' ),
			'description' => __( 'Edit framework settings.', 'gecko-theme' ),
		) );

		$wp_customize->add_section( $this->pre.'header', array(
			'priority' => 10,
			'capability' => 'edit_theme_options',
			'theme_supports' => '',
			'title' => __( 'Header', 'gecko-theme' ),
			'description' => __( 'Header settings.' ),
			'panel' => $this->pre.'settings',
		) );

		$wp_customize->add_section( $this->pre.'colors', array(
			'priority' => 10,
			'capability' => 'edit_theme_options',
			'theme_supports' => '',
			'title' => __( 'Colors', 'gecko-theme' ),
			'description' => __( 'Site Colors.' ),
			'panel' => $this->pre.'settings',
		) );

		$wp_customize->add_setting( $this->pre.'header_type', array(
			'default' => '0',
			'type' => 'option',
			'capability' => 'edit_theme_options',
			'transport' => 'refresh',
		) );
		$choices = ['none' => 'None'];
		$headers = new RecursiveDirectoryIterator(GECKO_FRAMEWORK__PLUGIN_DIR.'headers');
		$headers->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
		foreach ($headers as $header) {
			if($header->isDir()){
				$slug = $header->getBasename();
				$name = ucwords(str_replace('-', ' ', $slug));
				$choices[$slug] = $name;
			}
		}
		$wp_customize->add_control( $this->pre.'header_type', array(
			'type' => 'select',
			'priority' => 10,
			'section' => $this->pre.'header',
			'label' => __( 'Header Type', 'gecko-designs' ),
			'description' => 'Select a header type.',
			'choices'  => $choices,
		) );

		$wp_customize->add_setting( $this->pre.'header_sticky', array(
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'sanitize_callback' => [$this, 'sanitize_checkbox'],
		) );
		$wp_customize->add_setting( $this->pre.'header_sticky_padding', array(
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'sanitize_callback' => [$this, 'sanitize_checkbox'],
		) );
		$wp_customize->add_setting( $this->pre.'header_auto_hide', array(
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'sanitize_callback' => [$this, 'sanitize_checkbox'],
		) );
		$wp_customize->add_setting( $this->pre.'header_search', array(
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'sanitize_callback' => [$this, 'sanitize_checkbox'],
		) );

		$wp_customize->add_control( $this->pre.'header_sticky', array(
			'type' => 'checkbox',
			'section' => $this->pre.'header', // Add a default or your own section
			'label' => __( 'Sticky?' ),
			'description' => __( '' ),
		) );
		$wp_customize->add_control( $this->pre.'header_sticky_padding', array(
			'type' => 'checkbox',
			'section' => $this->pre.'header', // Add a default or your own section
			'label' => __( 'Sticky Padding?' ),
			'description' => __( '' ),
		) );
		$wp_customize->add_control( $this->pre.'header_auto_hide', array(
			'type' => 'checkbox',
			'section' => $this->pre.'header', // Add a default or your own section
			'label' => __( 'Auto Hide?' ),
			'description' => __( '' ),
		) );
		$wp_customize->add_control( $this->pre.'header_search', array(
			'type' => 'checkbox',
			'section' => $this->pre.'header', // Add a default or your own section
			'label' => __( 'Include Search?' ),
			'description' => __( '' ),
		) );

	}

	public function sanitize_checkbox( $checked ) {
		// Boolean check.
		return ( ( isset( $checked ) && true == $checked ) ? true : false );
	}

}

