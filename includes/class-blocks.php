<?php
namespace GeckoFramework;
use RecursiveDirectoryIterator;

class Blocks {
	public function __construct() {
		// Custom category registration
		add_action("init", [$this, "register_dependencies"], 10);
		add_action("init", [$this, "register_blocks"], 10);
	}

	public function register_dependencies() {
		/**
		 * If there is a chance an external dependency will be used again in perhaps another plugin you should register it so that that same code is not included over and over and over. Emphasis on over.
		 */
		if(file_exists( GECKO_FRAMEWORK__PLUGIN_DIR . "dist/react-select.js" )){
			wp_register_script("react-select", GECKO_FRAMEWORK__PLUGIN_URL."dist/react-select.js", ["react","react-dom",], filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . "dist/react-select.js" ));
		}

		// Swiper.js - script
		if (file_exists(GECKO_FRAMEWORK__PLUGIN_DIR . "vendor/swiper/swiper.min.js")) {
			wp_register_script("swiper", GECKO_FRAMEWORK__PLUGIN_URL . "vendor/swiper/swiper.min.js", [], filemtime(GECKO_FRAMEWORK__PLUGIN_DIR . "vendor/swiper/swiper.min.js"));
		}
		// Swiper.js - style
		if (file_exists(GECKO_FRAMEWORK__PLUGIN_DIR . "vendor/swiper/swiper.min.css")) {
			wp_register_style("swiper", GECKO_FRAMEWORK__PLUGIN_URL . "vendor/swiper/swiper.min.css", [], filemtime(GECKO_FRAMEWORK__PLUGIN_DIR . "vendor/swiper/swiper.min.css"));
		}

		// FontAwesome icons
		if (file_exists(GECKO_FRAMEWORK__PLUGIN_DIR . "vendor/fontawesome/css/all.min.css")) {
			wp_register_style("font-awesome", GECKO_FRAMEWORK__PLUGIN_URL . "vendor/fontawesome/css/all.min.css", [], filemtime(GECKO_FRAMEWORK__PLUGIN_DIR . "vendor/fontawesome/css/all.min.css"));
		}
	}

	public function register_blocks() {
		// Initialize custom blocks
		// Dynamically import blocks in blocks folder
		$blocks =new RecursiveDirectoryIterator(GECKO_FRAMEWORK__PLUGIN_DIR.'build/blocks');
		$blocks->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
		foreach ($blocks as $block) {
			if($block->isDir()){
				$name = $block->getBasename();
				$this->{str_replace('-', '_', $name)} = new Block($name);
			}
		}
	}

}