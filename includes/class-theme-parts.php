<?php
namespace GeckoFramework;
global $_gecko_theme_parts;
$_gecko_theme_parts = [];

// This is an extendable block.
class ThemeParts {
	public function __construct() {
		add_action( 'init', [$this, 'theme_part'] );
		add_action( 'load-edit.php', [$this, 'update_locations']);
	}

	/**
	 * Get the Theme Part Content
	 */
	static function content($location){
		$post_id = get_site_option( 'theme_part_'.$location); 
		if(!$post_id) return;
		$part = get_post($post_id)->post_content;
		$part = apply_filters('the_content', $part);
		echo $part;
	}

	/**
	 * Register a location in your theme functions
	 */
	static function register($location, $description){
		return self::register_locations( [$location => $description]);
	}

	/**
	 * You could register many at once if you wanted.
	 * [['slug' => 'description']]
	 */
	static function register_locations($locations = []){
		global $_gecko_theme_parts;
		$_gecko_theme_parts = array_merge( (array) $_gecko_theme_parts, $locations );
	}

	/**
	 * Theme Part CPT
	 */
	public function theme_part() {
		$labels = array(
			'name' => __( 'Theme Part', 'gecko-theme' ),
			'singular_name' => __( 'Theme Parts', 'gecko-theme' ),
			'add_new' => __( 'Add New', 'gecko-theme' ),
			'add_new_item' => __( 'Add New Theme Part', 'gecko-theme' ),
			'edit_item' => __( 'Edit Theme Parts', 'gecko-theme' ),
			'new_item' => __( 'New Theme Part', 'gecko-theme' ),
			'view_item' => __( 'View Theme Parts', 'gecko-theme' ),
			'search_items' => __( 'Search Theme Parts', 'gecko-theme' ),
			'not_found' => __( 'No Theme Parts found', 'gecko-theme' ),
			'not_found_in_trash' => __( 'No Theme Parts found in Trash', 'gecko-theme' ),
			'menu_name' => __( 'Theme Parts', 'gecko-theme' ),
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
			'description' => __( 'Theme Parts', 'gecko-theme' ),
			'supports' => array( 'title', 'slug', 'editor' ), // What does the Asset Support
			'public' => false, // Should the cpt have urls
			'show_ui' => true,
			'show_in_rest' => true,
			'show_in_menu' => true,
			'menu_position' => 25,
			'menu_icon' => 'dashicons-welcome-widgets-menus', // https://developer.wordpress.org/resource/dashicons/
			'show_in_nav_menus' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'has_archive' => false, // Slug for the Archive Page
			'query_var' => false,
			'can_export' => false,
			'rewrite' => false,
			'capability_type' => 'post' // If you want hierarchical use page
		);
		register_post_type( 'theme-part', $args );
		add_filter( 'manage_edit-theme-part_columns', [$this, 'theme_part_columns'] ) ;
		add_action( 'manage_theme-part_posts_custom_column', [$this, 'theme_part_manage_columns'], 10, 2 );
	}

	public function theme_part_columns($columns) {
		$columns = array(
			'cb' => $columns['cb'],
			'title' => $columns['title'],
			'theme-location' => 'Location',
		);

		return $columns;
	}

	public function theme_part_manage_columns( $column, $post_id ) {
		global $post;
		global $_gecko_theme_parts;
		switch( $column ) {
			/* If displaying the 'duration' column. */
			case 'theme-location' :
				echo '<form method="POST" action"">';
				echo '<input type="hidden" name="theme-part[post_id]" value="'.$post_id.'"/>';
				foreach($_gecko_theme_parts as $k => $v){
					$checked = '';
					$location = get_site_option( 'theme_part_'.$k);
					if($location == $post_id){
						$checked='checked="checked"';
					}
					echo '<input type="radio" name="theme-part[location]" value="'.$k.'" '.$checked.'>'.$v.'</input><br/>';
				}
				echo '<input type="submit" value="Save"/>';
				echo '</form>';
				break;
			default :
				break;
		}
	}

	public function update_locations(){
		if (isset($_REQUEST['theme-part'])) {
			$theme_part = $_REQUEST['theme-part'];
			$location = update_site_option( 'theme_part_'.$theme_part['location'], $theme_part['post_id']); 
		}
	}
}
