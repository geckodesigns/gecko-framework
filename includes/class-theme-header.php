<?php
namespace GeckoFramework;
use WP_Query;

class ThemeHeader {

	public function __construct() {
		$this->include_functions();
	}

	public function include_functions(){
		$option = get_option( 'gecko_framework_header_type', false);
		if(!$option || $option == 'none') return;
		include(GECKO_FRAMEWORK__PLUGIN_DIR.'headers/'.$option.'/functions.php');
	}

	static function output() {
		$option = get_option( 'gecko_framework_header_type', false);
		if(!$option|| $option == 'none') return;
		include(GECKO_FRAMEWORK__PLUGIN_DIR.'headers/'.$option.'/template.php');
	}
}
