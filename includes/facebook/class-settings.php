<?php
namespace GeckoFramework\Facebook;

class Settings {
	public function __construct() {
		add_action('admin_init', [$this, 'register']);
		add_action( 'admin_menu', array( $this, 'admin_page' ) );
		register_setting( 'gecko-facebook', 'facebook_app_id');
		register_setting( 'gecko-facebook', 'facebook_app_secret');
		register_setting( 'gecko-facebook', 'facebook_page_id');
		register_setting( 'gecko-facebook', 'facebook_cache_updated');
		/**
		 * Refresh feed when updated timestamp changes
		 */
		add_action('update_option_facebook_cache_updated', function( $old_value, $value ) {
			$init = new Facebook();
			$init->update_instagram_feed();
			$init->update_events_feed();
		}, 10, 2);
	}

	public function admin_page(){
		add_options_page( 
			'Facebook',
			'Facebook',
			'manage_options',
			'gecko-facebook',
			function(){
					// Set class property
					$appID = get_option( 'facebook_app_id' );
					$access_token = get_option( 'facebook_access_token' );
					if($appID && !$access_token):
						$init = new Facebook();
					endif;
					?>
					<div class="wrap">
							<h1>Facebook</h1>
							<form method="post" action="options.php">
							<?php
									// This prints out all hidden setting fields
									settings_fields( 'gecko-facebook' );
									do_settings_sections( 'gecko-facebook' );
									submit_button();
							?>
							</form>
					</div>
					<?php
			}
		);
	}


	public function register(){
		add_settings_section( 
			'facebook',
			'Facebook',
			function(){
				$appID = get_option( 'facebook_app_id' );
				if(!$appID){
					echo '<p>You need to set up a facebook app.</p>';
					return;
				}	
				$login = new LoginForm; 
				echo $login->form();
			},
			'gecko-facebook'
		);
		add_settings_field(
			'facebook_appid',
			'App ID',
			function(){
				echo '
					<input name="facebook_app_id" id="facebook_app_id" class="regular-text" type="text" value="' . get_option( 'facebook_app_id' ). '" />
				';
			},
			'gecko-facebook',
			'facebook'
		);
		add_settings_field(
			'facebook_app_secret',
			'Secret',
			function(){
				echo '
					<input name="facebook_app_secret" id="facebook_app_secret" class="regular-text" type="text" value="' . get_option( 'facebook_app_secret' ). '" />
				';
				// echo get_option( 'facebook_access_token' );
			},
			'gecko-facebook',
			'facebook'
		);
		add_settings_field(
			'facebook_cache_updated',
			'Last Updated',
			function(){
				$updated = get_option( 'facebook_cache_updated' );
				$date = date("F j, Y, g:i a", $updated);
				echo '
					<input class="regular-text" readonly="readonly" type="text" value="' . $date. '" />
					<input name="facebook_cache_updated" id="facebook_cache_updated" type="hidden" value="' . time(). '" />
					<p>Clicking "Save Changes" with refresh feed.</p>
				';
			},
			'gecko-facebook',
			'facebook'
		);
		$appID = get_option( 'facebook_app_id' );
		$access_token = get_option( 'facebook_access_token' );
		if($appID && $access_token):
			add_settings_field(
				'facebook_test',
				'Select Page',
				function(){
					$init = new Facebook();
					$fb = $init->fb;
					try {
						$response = $fb->get('/me/accounts');
					} catch(\Facebook\Exceptions\FacebookResponseException $e) {
						// When Graph returns an error
						echo 'Graph returned an error: ' . $e->getMessage();
						return;
					} catch(\Facebook\Exceptions\FacebookSDKException $e) {
						// When validation fails or other local issues
						echo 'Facebook SDK returned an error: ' . $e->getMessage();
						return;
					}
					$pages = $response->getGraphEdge();
					echo '<select name="facebook_page_id" id="facebook_page_id" >';
					foreach ($pages as $page) {
						$selected = (get_option( 'facebook_page_id' ) === $page['id'] )? 'selected="selected"': '';
						echo '<option value="'.$page['id'].'" '.$selected.'>';
						echo $page['name'];
						echo '</option>';
					}
					echo '</select>';

					/**
					 * Get Instagram ID
					 */
					$page_id = get_option( 'facebook_page_id' );
					$instagram_id = get_option( 'facebook_instagram_id' );
					if($page_id && !$instagram_id) {
						$response = $fb->get('/'.$page_id.'?fields=instagram_business_account');
						$instagram = $response->getGraphObject();
						$instagram_id = $instagram->getProperty('instagram_business_account')['id'];
						update_option( 'facebook_instagram_id', $instagram_id);
					}
					echo '<br />';
					echo '<input type="text" value="'.$instagram_id.'" disabled/>';
					echo '<p>Instagram ID</p>';

				},
				'gecko-facebook',
				'facebook'
			);
		endif;
	}
}