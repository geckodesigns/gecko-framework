<?php
namespace GeckoFramework\Facebook;

class LoginForm {
	public $id;
	public $secret;

	public function __construct() {
		$this->id = get_option( 'facebook_app_id' );
		$this->secret = get_option( 'facebook_app_secret' );
	}

	// https://developers.facebook.com/docs/facebook-login/web/login-button
	public function form(){
		return "
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId			: ".$this->id.",
					cookie		: true,
					xfbml			: true,
					version		: 'v3.3'
				});
				FB.AppEvents.logPageView();
				FB.getLoginStatus(function(response) {
					console.log(response);
					FB.api('/me', {fields: 'first_name,last_name,email,picture'}, function(response) {
						console.log(response);
					});
				});
			};

			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = 'https://connect.facebook.net/en_US/sdk.js';
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
			function checkLoginState() {
				FB.getLoginStatus(function(response) {
					console.log(response);
					FB.api('/me', {fields: 'first_name,last_name,email,picture'}, function(response) {
						console.log(response);
					});
				});
			}
		</script>
		<fb:login-button 
			scope='public_profile,email,instagram_basic,manage_pages,pages_show_list,user_events'
			onlogin='checkLoginState();'
			data-use-continue-as='true'
			data-size='large'
			data-button-type='continue_with'
			data-auto-logout-link='true'
			>
		</fb:login-button>
		";
	}
}