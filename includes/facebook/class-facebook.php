<?php
// https://developers.facebook.com/docs/reference/php
// https://developers.facebook.com/tools/explorer/437358993777436
// https://developers.facebook.com/docs/facebook-login/access-tokens/refreshing/
// https://developers.facebook.com/docs/instagram-api/getting-started
namespace GeckoFramework\Facebook;;

class Facebook {
	public $app_id;
	public $app_secret;
	public $fb;

	public function __construct(){
		$this->app_id = get_option( 'facebook_app_id' );
		$this->app_secret = get_option( 'facebook_app_secret' );
		$access_token = get_option( 'facebook_access_token' );
		$this->fb = new \Facebook\Facebook([
			'app_id' => $this->app_id,
			'app_secret' => $this->app_secret,
			'default_graph_version' => 'v4.0',
			'default_access_token' => ($access_token)? $access_token: '', // optional
		]);
		$token = $this->fb->getDefaultAccessToken();
		if($token->isExpired() || !$access_token){
			$helper = $this->fb->getJavaScriptHelper();
			$js_access_token = $helper->getAccessToken();
			if(!$js_access_token) return;
			$access_token = $js_access_token->getValue();
			$this->fb->setDefaultAccessToken($access_token);
		}
		if(!$token->isLongLived()) {
			$oAuth2Client = $this->fb->getOAuth2Client();
			$tokenMetadata = $oAuth2Client->debugToken($access_token);
			$access_token = $oAuth2Client->getLongLivedAccessToken($access_token);
			$this->fb->setDefaultAccessToken($access_token);
		}
		update_option( 'facebook_access_token', $access_token);
	}

	public function update_instagram_feed(){
		if(!$this->fb){return;}	
		$instagram_id = get_option( 'facebook_instagram_id' );
		$instagram_feed = get_option( 'facebook_instagram_feed');
		if(!$instagram_feed){return;}
		try {
			$response = $this->fb->get('/'.$instagram_id.'/media?fields=id,caption,media_type,media_url,thumbnail_url,permalink,shortcode,like_count,comments_count,timestamp&limit=10000');
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			return;
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			return;
		}
		$posts = $response->getGraphEdge();
		foreach ($posts as $post) {
			$feed[] = [
				'id' => $post['id'],
				'caption' => (isset($post['caption']))? base64_encode($post['caption']): '',
				'media_type' => $post['media_type'],
				'media_url' => $post['media_url'],
				'thumbnail_url' => $post['thumbnail_url'], // Specific to videos
				'permalink' => $post['permalink'],
				'shortcode' => $post['shortcode'],
				'like_count' => $post['like_count'],
				'comments_count' => $post['comments_count'],
				'timestamp' => $post['timestamp'],
			];
		}
		update_option('facebook_instagram_feed', $feed );
	}

	public function update_events_feed(){
		if(!$this->fb){return;}	
		$page_id = get_option( 'facebook_page_id' );
		if(!$page_id){return;}
		try {
			$response = $this->fb->get('/'.$page_id.'/events?fields=cover,category,attending_count,id,event_times,end_time,interested_count,name,place,start_time,ticket_uri,description&limit=100');
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			return;
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			return;
		}
		$posts = $response->getGraphEdge();
		foreach ($posts as $post) {
			$feed[] = $post->asArray();
		}
		set_transient( 'facebook_page_events', $feed);
	}
}