<?php
namespace GeckoFramework\Facebook;

class Cron {
	public function __construct() {
		if ( ! wp_next_scheduled( 'gecko_facebook_caching' ) ) {
			wp_schedule_event( time(), 'daily', 'gecko_facebook_caching' );
		}
		add_action("init", [$this, "cron"], 10);
	}
	public function cron(){
		add_action( 'gecko_facebook_caching', [$this, 'update']);
	}
	public function update() {
		$facebook = new Facebook();
		$facebook->update_instagram_feed();
		$facebook->update_events_feed();
	}
}