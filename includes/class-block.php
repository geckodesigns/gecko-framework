<?php
namespace GeckoFramework;

// This is an extendable block.
class Block {
	public $slug;
	public $prefix = 'gecko-';
	public $deps = [];

	public function __construct($slug = 'block') {
			$this->slug = $slug;
			$this->register();
	}

	public function register() {
		$deps_json = file_get_contents(GECKO_FRAMEWORK__PLUGIN_DIR . 'build/blocks/'.$this->slug.'/deps.json');
		$deps = json_decode($deps_json, true);

		$this->deps = $deps['public'];

		/**
		 * Editor File
		 */
		if(file_exists (GECKO_FRAMEWORK__PLUGIN_DIR .'build/blocks/'.$this->slug.'/editor.js')){
			wp_register_script(
				$this->prefix.$this->slug.'-editor',
				GECKO_FRAMEWORK__PLUGIN_URL.'build/blocks/'.$this->slug.'/editor.js',
				$deps['editor'],
				filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . 'build/blocks/'.$this->slug.'/editor.js' )
			);
		}
		/**
		 * Public File
		 */
		if(file_exists (GECKO_FRAMEWORK__PLUGIN_DIR .'build/blocks/'.$this->slug.'/public.js')){
			wp_register_script(
				$this->prefix.$this->slug.'-public',
				GECKO_FRAMEWORK__PLUGIN_URL . 'build/blocks/'.$this->slug.'/public.js',
				$deps['public'],
				filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . 'build/blocks/'.$this->slug.'/public.js' )
			);
		}
		/**
		 * Public Style
		 */
		if(file_exists (GECKO_FRAMEWORK__PLUGIN_DIR .'build/blocks/'.$this->slug.'/style.css')){
			wp_register_style(
				$this->prefix.$this->slug.'-style',
				GECKO_FRAMEWORK__PLUGIN_URL . 'build/blocks/'.$this->slug.'/style.css',
				[],
				filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . 'build/blocks/'.$this->slug.'/style.css' )
			);
		}
		/**
		 * Public Style
		 */
		if(file_exists (GECKO_FRAMEWORK__PLUGIN_DIR .'build/blocks/'.$this->slug.'/style-editor.css')){
			wp_register_style(
				$this->prefix.$this->slug.'-style-editor',
				GECKO_FRAMEWORK__PLUGIN_URL . 'build/blocks/'.$this->slug.'/style-editor.css',
				[],
				filemtime( GECKO_FRAMEWORK__PLUGIN_DIR . 'build/blocks/'.$this->slug.'/style-editor.css' )
			);
		}
		/**
		 * Register the block
		 */
		$args = [];
		$args['editor_script'] = $this->prefix.$this->slug.'-editor';
		$args['editor_style'] = $this->prefix.$this->slug.'-style-editor';
		$dynamic_block = GECKO_FRAMEWORK__PLUGIN_DIR . 'build/blocks/'.$this->slug.'/block.php';
		if(file_exists($dynamic_block )){
			$render = false;
			include_once($dynamic_block);// $render is set here.
			if($render){
				$args['render_callback'] = $render;
			}
		}
		register_block_type('gecko/'.$this->slug, $args);

		/**
		 * Load styles on the backend
		 */
		add_action( 'enqueue_block_editor_assets', function(){
			wp_enqueue_style($this->prefix.$this->slug.'-style');
			// This script already get's included so this is redundent
			// wp_enqueue_script($this->prefix.$this->slug.'-style-editor');
			// This could potentially create bugs. It's better to just include the scripts in the editor compilation
			// wp_enqueue_script($this->prefix.$this->slug.'-public');
		});

		/**
		 * If Post has block then enqueue script
		 */
		add_filter( 'the_content', function($content){
			if(has_block( 'gecko/'.$this->slug, $content )){
				$this->enqueue_block_scripts();
			}
			return $content;
		}, 1);
	}

	public function enqueue_block_scripts(){
		// Make sure the public deps are called if block doesn't have public script
		foreach($this->deps as $dep){
			wp_enqueue_script($dep);
			wp_enqueue_style($dep);
		}
		wp_enqueue_script($this->prefix.$this->slug.'-public');
		wp_enqueue_style($this->prefix.$this->slug.'-style');
	}
}