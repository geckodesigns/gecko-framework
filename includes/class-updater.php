<?php 
/**
 * Main Updater class.
 * Some Documentation:
 * https://css-tricks.com/deploying-bitbucket-wordpress/
 * https://code.tutsplus.com/tutorials/distributing-your-plugins-in-github-with-automatic-updates--wp-34817
 * 
 * @author   Dwayne Parton
 * @category Class
 * @package  Gecko\Command
 * @version  0.0.1
 */

namespace GeckoFramework; 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Updater Class.
 */
class Updater {
	
	private $slug; // plugin slug
	private $pluginData; // plugin data
	private $username; // Bitbucket username
	private $repo; // Bitbucket repo name
	private $pluginFile; // __FILE__ of our plugin
	private $bitbucketAPIResult; // holds data from Bitbucket https://api.bitbucket.org/2.0/repositories/geckodesigns/gecko-command/
	private $accessToken; // Bitbucket private repo token
	
	private $bitbucketDownloadUrl; // Bitbucket Download Link
	//protected $bitbucketSrc; // holds data from https://api.bitbucket.org/2.0/repositories/geckodesigns/gecko-command/src/
	protected $bitbucketLastUpdated; // Bitbucket Download Link
	protected $bitbucketPluginData; // Bitbucket Download Link
	protected $bitbucketPluginFile; // Plugin File on Bitbucket
	protected $bitbucketReadmeFile; // Bitbucket Repo

	/**
	 * Hook in methods.
	 */
	public function __construct($pluginFile, $bitbucketUsername, $bitbucketRepo, $accessToken = '') {
		add_filter( "pre_set_site_transient_update_plugins", array( $this, "setTransitent" ) );
		add_filter( "plugins_api", array( $this, "setPluginInfo" ), 10, 3 );
		add_filter( "upgrader_post_install", array( $this, "postInstall" ), 10, 3 );
		$this->pluginFile = $pluginFile;
		$this->username = $bitbucketUsername;
		$this->repo = $bitbucketRepo;
		$this->accessToken = $accessToken;

		// Include the access token for private Bitbucket repos
		// if ( !empty( $this->accessToken ) ) {
		// 	$bitbucketDownloadUrl = add_query_arg(
		// 		array( "access_token" => $this->accessToken ),
		// 		$bitbucketDownloadUrl
		// 	);
		// }
		$this->bitbucketDownloadUrl = "https://bitbucket.org/{$this->username}/{$this->repo}/get/HEAD.zip";
		//$this->accessToken = "gSWCEVxCQXTFngF8pCEr";
	}

	// Get information regarding our plugin installed on WordPress
	private function initPluginData() {
		$this->slug = plugin_basename( $this->pluginFile );
		$this->pluginData = get_plugin_data( $this->pluginFile );
	}

	// This is a combination of :
	// https://developer.wordpress.org/reference/functions/get_plugin_data/
	// https://developer.wordpress.org/reference/functions/get_file_data/
	// So we can read remote file data
	public function getBitbucketPluginData () {
		if ( empty( $this->bitbucketPluginFile ) ) {
			$this->getRepoReleaseInfo();
		}
		$file = wp_remote_get($this->bitbucketPluginFile);
		$file_data = wp_remote_retrieve_body($file);
		$default_headers = array(
			'Name' => 'Plugin Name',
			'PluginURI' => 'Plugin URI',
			'Version' => 'Version',
			'Description' => 'Description',
			'Author' => 'Author',
			'AuthorURI' => 'Author URI',
			'TextDomain' => 'Text Domain',
			'DomainPath' => 'Domain Path',
			'Network' => 'Network',
		);
		$context = 'plugin';
		// Make sure we catch CR-only line endings.
		$file_data = str_replace( "\r", "\n", $file_data );
	 
		if ( $context && $extra_headers = apply_filters( "extra_{$context}_headers", array() ) ) {
			$extra_headers = array_combine( $extra_headers, $extra_headers ); // keys equal values
			$all_headers = array_merge( $extra_headers, (array) $default_headers );
		} else {
			$all_headers = $default_headers;
		}
	 
		foreach ( $all_headers as $field => $regex ) {
			if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $file_data, $match ) && $match[1] )
				$all_headers[ $field ] = _cleanup_header_comment( $match[1] );
			else
				$all_headers[ $field ] = '';
		}
	 
		$plugin_data = $all_headers;
		$plugin_data['Network'] = ( 'true' == strtolower( $plugin_data['Network'] ) );
		unset( $plugin_data['_sitewide'] );
		$plugin_data['Title']      = $plugin_data['Name'];
		$plugin_data['AuthorName'] = $plugin_data['Author'];
	 	
		return $plugin_data;
		//return get_plugin_data($plugin);
	}
 
	// Get information regarding our plugin from Bitbucket
	private function getRepoReleaseInfo() {
		// Only do this once
		// if ( ! empty( $this->bitbucketPluginData ) ) {
		// 	return;
		// }

		// Query the Bitbucket API and get the Plugin Data.
		// https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/src
		$bitbucketSrc = "https://api.bitbucket.org/2.0/repositories/{$this->username}/{$this->repo}/src/";
		//We need the access token for private repos
		// if ( ! empty( $this->accessToken ) ) {
		// 	$bitbucketSrc = add_query_arg( array( "access_token" => $this->accessToken ), $bitbucketSrc );
		// }
		// Get the results
		$bitbucketSrc = wp_remote_retrieve_body( wp_remote_get( $bitbucketSrc ) );
		if ( ! empty( $bitbucketSrc ) ) {
			$bitbucketSrc = @json_decode( $bitbucketSrc );
		}
		foreach ($bitbucketSrc->values as $src) {
			if($src->path == 'README.md'){
				$this->bitbucketReadmeFile = $src->links->self->href;
			}
			if($src->path == $this->repo.'.php'){
				$this->bitbucketPluginFile = $src->links->self->href;
			}
		}
		$this->bitbucketPluginData = $this->getBitbucketPluginData();

		// Query the Bitbucket API and get the last updated on.
		// https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D
		$bitbucketRepo = "https://api.bitbucket.org/2.0/repositories/{$this->username}/{$this->repo}/";
		 
		//We need the access token for private repos
		// if ( ! empty( $this->accessToken ) ) {
		// 	$bitbucketRepo = add_query_arg( array( "access_token" => $this->accessToken ), $bitbucketRepo );
		// }
		// Get the results
		$bitbucketRepoResult = wp_remote_retrieve_body( wp_remote_get( $bitbucketRepo ) );
		if ( ! empty( $bitbucketRepoResult ) ) {
			$bitbucketRepoResult = @json_decode( $bitbucketRepoResult );
		}
		$this->bitbucketLastUpdated = $bitbucketRepoResult->updated_on;
	}
 
	// Push in plugin version information to get the update notification
	public function setTransitent( $transient ) {
		// If we have checked the plugin data before, don't re-check
		// if ( empty( $transient->checked ) ) {
		// 	return $transient;
		// }
		// Get plugin & Bitbucket release information
		$this->initPluginData();
		$this->getRepoReleaseInfo();
		// Check the versions if we need to do an update
		$doUpdate = ($this->pluginData["Version"] < $this->bitbucketPluginData["Version"])? true : false ;
		// Update the transient to include our updated plugin data
		if ( $doUpdate ) {
			$obj = (object)[];
			$obj->slug = $this->slug;
			$obj->new_version = $this->bitbucketPluginData["Version"];
			$obj->url = $this->pluginData["PluginURI"];
			$obj->package = $this->bitbucketDownloadUrl;
			$transient->response[$this->slug] = $obj;
		}
		return $transient;
	}
 
	// Push in plugin version information to display in the details lightbox, this shows if there is an update
	// https://developer.wordpress.org/reference/functions/plugins_api/
	// https://developer.wordpress.org/reference/hooks/plugins_api/
	public function setPluginInfo( $false, $action, $response ) {
		// Get plugin & Bitbucket release information
		$this->initPluginData();
		$this->getRepoReleaseInfo();
		// If nothing is found, do nothing
		if ( empty( $response->slug ) || $response->slug != $this->slug ) {
			return false;
		}
		// Add our plugin information
		$response->slug          = $this->slug;
		$response->plugin_name   = $this->bitbucketPluginData["Name"];
		$response->name          = $this->bitbucketPluginData["Name"];
		$response->author        = $this->bitbucketPluginData["AuthorName"];
		$response->homepage      = $this->bitbucketPluginData["PluginURI"];
		//$response->donate_link   = $plugin->donate_link;
		$response->version       = $this->bitbucketPluginData["Version"];
		//$response->requires      = $plugin->requires;
		//$response->tested        = $plugin->tested;
		//$response->downloaded    = $plugin->downloaded;
		$response->last_updated  = $this->bitbucketLastUpdated;
		$response->download_link = $this->bitbucketDownloadUrl;
		$response->banners       = array(
										'high' => GECKO_FRAMEWORK__PLUGIN_URL.'/assets/images/banner-772x250.png',
										'low' => GECKO_FRAMEWORK__PLUGIN_URL.'/assets/images/banner-1544x500.png',
									);
		$response->icons         = array(
										'svg' => GECKO_FRAMEWORK__PLUGIN_URL.'/assets/images/icon.svg',
										'1x' => GECKO_FRAMEWORK__PLUGIN_URL.'/assets/images/icon-128x128.png',
										'2x' => GECKO_FRAMEWORK__PLUGIN_URL.'/assets/images/icon-256x256.png',
									);
		//foreach ( (array) $plugin->contributors as $contributor ) {
		// 	$contributors[ $contributor ] = '//profiles.wordpress.org/' . $contributor;
		//}
		$response->contributors = array(
										'dwayneparton' => '//profiles.wordpress.org/dwayneparton',
									);
		// We're going to parse the Bitbucket markdown release notes, include the parser
		$Parsedown = new \Parsedown();
		// Create tabs in the lightbox
		$readme = wp_remote_retrieve_body(wp_remote_get($this->bitbucketReadmeFile));
		$readme = $Parsedown->text($readme);
		$debug = "<h3>Bitbucket Info</h3>
				<ul>
					<li>Download Link: ".$this->bitbucketDownloadUrl."</li>
					<li>Plugin File: ".$this->bitbucketLastUpdated."</li>
					<li>Plugin File: ".$this->bitbucketPluginFile."</li>
					<li>Repo: ".$this->bitbucketReadmeFile."</li>
				</ul>";
		$response->sections = array(
			'description' => $this->bitbucketPluginData["Description"],
			'readme' => $readme,
			'debugging' => $debug,
		);
		return $response;
	}
 
	// Perform additional actions to successfully install our plugin
	public function postInstall( $true, $hook_extra, $result ) {
		// Get plugin information
		$this->initPluginData();
		// Remember if our plugin was previously activated
		$wasActivated = is_plugin_active( $this->slug );
		// Since we are hosted in Bitbucket, our plugin folder would have a dirname of
		// reponame-tagname change it to our original one:
		global $wp_filesystem;
		$pluginFolder = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . dirname( $this->slug );
		$wp_filesystem->move( $result['destination'], $pluginFolder );
		$result['destination'] = $pluginFolder;
		// Re-activate plugin if needed
		if ( $wasActivated ) {
			// Activate Plugin
			$activated = activate_plugin( $this->slug );
			// If activation do action
			if($activated){
				// Place to hook in functions that run when the plugin is updated.
				do_action('gecko_framework_plugin_updated');
			}
		}
		 
		return $result;
	}


}