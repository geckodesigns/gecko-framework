<?php
namespace GeckoFramework\Google;

class Cron {
	public function __construct() {
		if ( ! wp_next_scheduled( 'gecko_google_places' ) ) {
			wp_schedule_event( time(), 'daily', 'gecko_google_places' );
		}
		add_action("init", [$this, "cron"], 10);
	}
	public function cron(){
		add_action('update_option_google_api_key', [$this, 'update']);
		add_action('update_option_google_places', [$this, 'update']);
		add_action( 'gecko_google_places', [$this, 'update']);
	}
	public function update() {
		$api_key = get_option( 'google_api_key' );
		if(!$api_key) return;
		$places = get_option( 'google_places' );
		if(!$places) return;
		foreach($places as $place){
			$url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$place.'&key='.$api_key;
			$response = wp_remote_get( $url );
			$body = json_decode($response['body'], true);
			if($body['status'] === 'OK'){
				update_site_option( 'google_places_'.$place, wp_json_encode($body['result']));
			}
		}
	}
}