<?php
namespace GeckoFramework\Google;

class Init {
	public function __construct() {
		new Api();
		new Cron();
		new Settings();
	}
}