<?php
namespace GeckoFramework\Google;

class Settings {
	public function __construct() {
		add_action("admin_init", [$this, "register"]);
		add_action('admin_init', [$this, 'register']);
		add_action( 'admin_menu', array( $this, 'admin_page' ) );
		add_site_option( 'google_places', [] );
		register_setting( 'gecko-google', 'google_api_key');
		register_setting( 'gecko-google', 'google_places');
	}

	public function admin_page(){
		add_options_page( 
			'Google',
			'Google',
			'manage_options',
			'gecko-google',
			function(){
					// Set class property
					?>
					<div class="wrap">
							<h1>Google</h1>
							<form method="post" action="options.php">
							<?php
									// This prints out all hidden setting fields
									settings_fields( 'gecko-google' );
									do_settings_sections( 'gecko-google' );
									submit_button();
							?>
							</form>
					</div>
					<?php
			}
		);
	}
	
	public function register(){
		add_settings_section( 
			'google-api',
			'Google API',
			function(){
				echo '<p>Certain parameters are required to initiate a search request. As is standard in URLs, all parameters are separated using the ampersand (&) character. Below is a list of the parameters and their possible values.</p>';
			},
			'gecko-google'
		);
		add_settings_field(
			'google_api_key',
			'API key',
			function(){
				echo '
					<input name="google_api_key" id="google_api_key" class="regular-text" type="text" value="' . get_option( 'google_api_key' ). '" />
					<p>This key identifies your application for purposes of quota management. See <a href="https://developers.google.com/places/web-service/get-api-key">Get a key</a> for more information.</p>
				';
			},
			'gecko-google',
			'google-api'
		);
		add_settings_section( 
			'google-places',
			'Places',
			function(){
				echo '<p>This should allow for an array of places to be added and cached.</p>';
			},
			'gecko-google',
			'google-api'
		);
		add_settings_field(
			'google_places',
			'Place ID',
			function(){
				wp_register_script(
					'gecko-google-places',
					GECKO_FRAMEWORK__PLUGIN_URL.'/build/scripts/gecko-google-places.js',
					['react','react-dom']
				);
				wp_enqueue_script('gecko-google-places');
				$places = ['places' => get_option( 'google_places' )];
				$encoded = json_encode($places, JSON_HEX_APOS|JSON_HEX_QUOT);
				echo sprintf('<div id="gecko-google-places" data-props="%s"></div>', htmlspecialchars($encoded, ENT_QUOTES, 'UTF-8'));
				echo '<p>A textual identifier that uniquely identifies a place, returned from a <a href="https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder">Place Search</a>. This just saves an api call at the moment.';
			},
			'gecko-google',
			'google-places'
		);
	}

}