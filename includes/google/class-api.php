<?php
namespace GeckoFramework\Google;
use WP_Query;

class Api extends \GeckoFramework\Api{

	public function rest_api_init() {
		register_rest_route( $this->namespace, '/google/places', [
			'methods' => 'GET',
			'callback' => [$this, 'get'],
		]);
		register_rest_route( $this->namespace, '/google/places/(?P<place_id>[a-zA-Z0-9-]+)/', [
			'methods' => 'GET',
			'callback' => [$this, 'get_place'],
		]);
	}

	public function get(){
		return get_option( 'google_places', [] );
	}
	public function get_place($request){
		$params = $request->get_params();
		return json_decode(get_option( 'google_places_'.$params['place_id'], [] ));
	}

}
