<?php
namespace GeckoFramework\Icons;
global $_gecko_icons;
$_gecko_icons = [];

// This is an extendable block.
class Init {
	public function __construct() {
		add_action( 'init', [$this, 'icon'] );
	}

	/**
	 * Icon CPT
	 */
	public function icon() {
		$labels = array(
			'name' => __( 'Icon', 'gecko-theme' ),
			'singular_name' => __( 'Icons', 'gecko-theme' ),
			'add_new' => __( 'Add New', 'gecko-theme' ),
			'add_new_item' => __( 'Add New Icon', 'gecko-theme' ),
			'edit_item' => __( 'Edit Icons', 'gecko-theme' ),
			'new_item' => __( 'New Icon', 'gecko-theme' ),
			'view_item' => __( 'View Icons', 'gecko-theme' ),
			'search_items' => __( 'Search Icons', 'gecko-theme' ),
			'not_found' => __( 'No Icons found', 'gecko-theme' ),
			'not_found_in_trash' => __( 'No Icons found in Trash', 'gecko-theme' ),
			'menu_name' => __( 'Icons', 'gecko-theme' ),
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
			'description' => __( 'Icons', 'gecko-theme' ),
			'supports' => array( 'title', 'slug', 'editor' ), // What does the Asset Support
			'public' => false, // Should the cpt have urls
			'show_ui' => true,
			'show_in_rest' => false,
			'show_in_menu' => true,
			'menu_position' => 100,
			'menu_icon' => 'dashicons-welcome-widgets-menus', // https://developer.wordpress.org/resource/dashicons/
			'show_in_nav_menus' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'has_archive' => false, // Slug for the Archive Page
			'query_var' => false,
			'can_export' => false,
			'rewrite' => false,
			'capability_type' => 'post' // If you want hierarchical use page
		);
		register_post_type( 'icon', $args );

		// Ideally use code editor instead maybe:
		// https://gist.github.com/igorbenic/f63f1d21823871833d937e06c05ee8a0
		add_filter('wp_editor_settings', function(  $settings, $editor_id ) {
			if ( $editor_id === 'content' && get_current_screen()->post_type === 'icon' ) {
					$settings['tinymce']   = false;
					$settings['quicktags'] = false;
					$settings['media_buttons'] = false;
			}
			return $settings;
		}, 10, 2);
	}

}
