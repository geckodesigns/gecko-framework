<?php
namespace GeckoFramework\Api;
use WP_Query;

class Login extends \GeckoFramework\Api {

	public function __construct() {
		add_action( 'rest_api_init',[$this, 'rest_api_init']);
	}

	public function rest_api_init() {
		register_rest_route( $this->namespace, '/login', [
			'methods' => 'POST',
			'callback' => [$this, 'login'],
		]);
		register_rest_route( $this->namespace, '/login/facebook', [
			'methods' => 'POST',
			'callback' => [$this, 'facebook'],
		]);
		register_rest_route( $this->namespace, '/logout', [
			'methods' => 'GET',
			'callback' => [$this, 'logout'],
		]);
		register_rest_route( $this->namespace, '/register', [
			'methods' => 'POST',
			'callback' => [$this, 'register'],
		]);
	}

	public function login($request){
		$params = $request->get_params();
		$info = array();
		$info['user_login'] = $params['user'];
		$info['user_password'] = $params['password'];
		$info['remember'] = true;
		$user_signon = wp_signon( $info, false );
		 if ( is_wp_error($user_signon) ){
			return ['loggedin'=>false, 'message'=> 'Wrong username or password.'];
		} else {
			return ['loggedin'=>true, 'message'=> 'Login successful, redirecting...'];
		}
	}

	/**
	 * Process facebook login
	 */
	public function facebook($request){
		$params = $request->get_params();
		$info = array();
		$info['user_login'] = $params['user'];
		$info['user_password'] = $params['password'];
		$info['remember'] = true;
		$user_signon = wp_signon( $info, false );
		 if ( is_wp_error($user_signon) ){
			return ['loggedin'=>false, 'message'=> 'Wrong username or password.'];
		} else {
			return ['loggedin'=>true, 'message'=> 'Login successful, redirecting...'];
		}
	}

	public function logout(){
		return wp_logout();
	}

	public function register(){
		// https://codex.wordpress.org/Function_Reference/register_new_user
		return wp_logout();
	}

}
