<?php
namespace GeckoFramework\Api;
use WP_Query;

class Instagram extends \GeckoFramework\Api {

	public function __construct() {
		add_action( 'rest_api_init',[$this, 'rest_api_init']);
	}

	public function rest_api_init() {
		register_rest_route( $this->namespace, '/instagram', [
			'methods' => 'GET',
			'callback' => [$this, 'get'],
		]);
	}

	public function get($request){
		$instagram_feed = get_option( 'facebook_instagram_feed');
		$return = [];
		if(!$instagram_feed)return $return;
		foreach($instagram_feed as $post){
			$return[] = [
				'id' => $post['id'],
				'caption' => (isset($post['caption'])) ? apply_filters('the_content', base64_decode($post['caption'])) : '',
				'media_type' => $post['media_type'],
				'media_url' => $post['media_url'],
				'thumbnail_url' => $post['thumbnail_url'], // Specific to videos
				'permalink' => $post['permalink'],
				'shortcode' => $post['shortcode'],
				'like_count' => $post['like_count'],
				'comments_count' => $post['comments_count'],
				'timestamp' => $post['timestamp'],
			];
		}
		return $return;
	}

}
