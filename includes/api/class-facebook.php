<?php
namespace GeckoFramework\Api;
use WP_Query;
use DateTime;

class Facebook extends \GeckoFramework\Api {

	public function __construct() {
		add_action( 'rest_api_init',[$this, 'rest_api_init']);
	}

	public function rest_api_init() {
		register_rest_route( $this->namespace, '/facebook/events', [
			'methods' => 'GET',
			'callback' => [$this, 'get'],
		]);
	}

	public function get($request){
		$events = get_transient( 'facebook_page_events');
		$return = [];
		if(!$events)return $return;
		foreach($events as $event){
			$date_now = strtotime("now");
			$end_date = strtotime($event['end_time']->date);
			$start_date = strtotime($event['start_time']->date);
			if ($date_now > $end_date) continue;
			$update = $event;
			$update['description'] = (isset($event['description'])) ? apply_filters('the_content', $event['description']) : '';
			$update['sort'] = $end_date;
			$update['formated_start_date'] = date("F j, Y @ g:i a", $start_date);
			$update['formated_end_date'] = date("F j, Y @ g:i a", $end_date);
			$return[] = $update;
		}
		usort($return, function ($a, $b) {
			return $a['sort'] - $b['sort'];
		});
		return $return;
	}

}
