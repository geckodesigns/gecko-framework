<?php
namespace GeckoFramework\Api;
use WP_Query;

class Icons extends \GeckoFramework\Api {

	public function __construct() {
		if(current_user_can('administrator')){
			add_action( 'rest_api_init',[$this, 'rest_api_init']);
		}
	}

	public function rest_api_init() {
		register_rest_route( $this->namespace, '/icons/', [
			'methods' => 'GET',
			'callback' => [$this, 'icons'],
		]);
		register_rest_route( $this->namespace, '/icons/(?P<id>\d+)/', [
			'methods' => 'GET',
			'callback' => [$this, 'icon'],
		]);
	}

	public function icon($request){
		$params = $request->get_params();
		$id = $params['id'];
		$icon = get_post($id);
		return [
			'id' => $icon->ID,
			'name' => $icon->post_title,
			'slug' => $icon->post_name,
			'svg' => $icon->post_content,
		];
	}

	public function icons($request){
		$posts_per_page = (isset($params['posts_per_page']))? $params['posts_per_page'] : -1;
		$args = [
			'post_type' => 'icon',
			'post_status' => array('publish'),
			'posts_per_page' => $posts_per_page,
		];
		// add_filter( 'posts_search', 'gecko_autocomplete_search_by_title', 10, 2 );
		$query = new WP_Query( $args );
		$return = [];
		if ( $query->have_posts() ){
			foreach($query->get_posts() as $p){
				$return[] = [
					'id' => $p->ID,
					'name' => $p->post_title,
					'slug' => $p->post_name,
					'svg' => $p->post_content,
				];
			}
		}
		return $return;
	}

}
